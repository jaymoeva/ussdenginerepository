$(document).ready(function() {
	var tour = new Tour({
            steps: [{

                    element: "#step1",
                    title: "Visit our website",
                    content: "Go to www.ussdengine.co.ke and click the login/register link at the top right of your screen. You will be redirected to the login/register page",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }

                },
                {
                    element: "#step2",
                    title: "Create an account",
                    content: "Enter your personal details and pick a strong password to allow you to log in to the system. You will be required to provide your full names, personal or business email, and a strong password with a matching confirmation.",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#step3",
                    title: "Email verification",
                    content: "Check your mailbox and click the button titled verify email address. Please note that this is a one time link that expires upon clicking the button. This link will take you to a page whereby you can add your company.",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#step4",
                    title: "Add a company",
                    content: "Add your company to the system. You will be required to provide the full name of your company, its physical or postal address and a phone number. Please note that you wont be able to login until this step is completed.",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#step5",
                    title: "Log in",
                    content: "Congratulations! Upon adding your company, yopu will be automatically logged in. Access your dashboard and add more users to your company if need be.",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                }
            ]});

        // Initialize the tour
        tour.init();

        $('.startTour').click(function(){
            tour.restart();

            // Start the tour
            // tour.start();
        })
});