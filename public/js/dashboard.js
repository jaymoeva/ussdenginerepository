$(document).ready(function(){

	// IChecks for fancy checkboxes and radio buttons
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
    });

    // Jquery steps
    $("#wizard").steps();
    $("#form").steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex)
            {
                return true;
            }

            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("next");
            }

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);

            // Submit form input
            form.submit();
        }
    }).validate({
                errorPlacement: function (error, element)
                {
                    element.before(error);
                },
                rules: {
                    confirm: {
                        equalTo: "#password"
                    }
                }
            });

    // Dynamically add applications
    $(".add-application").click(function(e){
        e.preventDefault();
        var appName = $("#app-name").val();
        var appUrl = $("#app-url").val();
        var markup = '';
        console.log(appUrl);
        console.log(appName);
        $.ajax({
            type: "POST",
            url:'/ussdengine/public/createproject',
			headers: {
			  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {appname: appName, appurl: appUrl,type : 1 },
            success: function( res ) {
                console.log(res);

                for (var i = 0; i <= res.length; i++) {
                    console.log(res[i]);
                 //   if(res[i] !== "undefined") markup = markup + "<tr><td><input type='checkbox' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                    if (typeof res[i]=== "undefined") {
                       console.log('here us undefined');
                    }else{
                        markup = markup + "<tr><td><input onclick='getdetails("+res[i]['project_id']+")' type='radio' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                    }
                }
               // console.log(markup);
                $("#applications").empty();
                $("#applications").append(markup);
            },
			error: function(error){
				console.log(error);
			} 

        });
        console.log(markup);
        //var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + appName + "</td><td>" + appUrl + "</td></tr>";

    });


    

    // Show password when creating user
    $(".toggle-password").click(function() {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
    //Assigned

});