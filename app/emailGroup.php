<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class emailGroup extends Model
{
    protected $fillable = ['name', 'description', 'company_id'];

    /**
     * One to Many relationship with emailGroup::class
     *
     */

    public function contacts()
    {
    	return $this->hasMany('App\emailContact');
    }
}
