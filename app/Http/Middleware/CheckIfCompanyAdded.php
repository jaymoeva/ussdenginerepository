<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckIfCompanyAdded
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (Auth::user()->hasRole('executive')) {
           if (is_null(AUth::user->company_id)) {
               return redirect()->route('registration.complete');
           }
       }
        return $next($request);
    }
}
