<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Permission;
use App\User;
use App\Role;
use Mail;
use Auth;
use App\Mail\UserActivatedEmail;
use App\Mail\UserDeactivatedEmail;
use App\Mail\RoleChangedEmail;
use App\Mail\UserUpdatedEmail;
use App\Mail\UserCreatedEmail;

class UserController extends Controller
{

    // ============ WET CODE WILL BE REFACTORED LATER ==============
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|executive');

        // methods only accessible to the support@edgetech
        $this->middleware('role:executive')->only('create', 'store', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response        

     */
    public function index()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $users = User::orderBy('id', 'asc')->paginate(10);
        } else {
            $users = User::executive()->orderBy('id', 'asc')->paginate(10);
        }

        return view('backend/modules/user/index', compact('users'));
    }

    /**
     * Display a listing of verified users.
     *
     * @return \Illuminate\Http\Response
     */
    public function verified()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $verified = User::verified()->orderBy('id', 'asc')->paginate(10);
        } else {
            $verified = User::verified()->executive()->orderBy('id', 'asc')->paginate(10);
        }
        
        return view('backend/modules/user/verified', compact('verified'));
    }

    /**
     * Display a listing of unverified users.
     *
     * @return \Illuminate\Http\Response
     */
    public function unverified()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $unverified = User::unverified()->orderBy('id', 'asc')->paginate(10);
        } else {
            $unverified = User::unverified()->executive()->orderBy('id', 'asc')->paginate(10);
        }

        $unverified = User::where('verified', false)->orderBy('id', 'asc')->paginate(10);
        return view('backend/modules/user/unverified', compact('unverified'));
    }

    /**
     * Display a listing of active users.
     *
     * @return \Illuminate\Http\Response
     */
    public function active()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $active = User::active()->orderBy('id', 'asc')->paginate(10);
        } else {
            $active = User::active()->executive()->orderBy('id', 'asc')->paginate(10);
        }

        return view('backend/modules/user/active', compact('active'));
    }

    /**
     * Display a listing of inactive users.
     *
     * @return \Illuminate\Http\Response
     */
    public function inactive()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $inactive = User::inactive()->orderBy('id', 'asc')->paginate(10);
        } else {
            $inactive = User::inactive()->executive()->orderBy('id', 'asc')->paginate(10);
        }

        
        return view('backend/modules/user/inactive', compact('inactive'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('name', 'clerk')->orWhere('name', 'executive')->get();
        $myCompany = DB::table('companies')->where('owner_id', Auth::user()->id)->first();


        if (!empty($myCompany)) {
            if ($myCompany->status) {
                return view('backend/modules/user/create', compact('roles'));
            } else {
                Session::flash('unapproved', 'You can only create users after your company is approved');
                return redirect()->back();
            }
        } else {
            Session::flash('company-first', 'You can only perform this operation after adding your company');
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $myCompany = DB::table('companies')->where('owner_id', Auth::user()->id)->first();
        if (!empty($myCompany)) {
            if ($myCompany->status) {
                $request->validate([
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6',
                ]);

                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->added_by = Auth::user()->id;
                $user->company_id = $myCompany->id;

                $user->save();

                if ($request->roles) {
                   $user->attachRoles($request->roles);
                }
                else {
                    $user->attachRole(Role::where('name','clerk')->first());
                }

                //Send Email
                Mail::to($user->email)->send(new UserCreatedEmail($user));

                Session::flash('create-user', 'New user has been created successfully');
                return redirect()->route('users.show', $user->id);
            } else {
                Session::flash('unapproved', 'You can only create users after your company is approved');
                return redirect()->back();
            }
            
        } else {
            Session::flash('company-first', 'You can only perform this operation after adding your company');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if ($user->added_by === Auth::user()->id) {
            return view('backend/modules/user/show', compact('user'));
        } elseif ($user->id === Auth::user()->id) {
            Session::flash('not-permitted', 'You can not view yourself. To do this, go to your profile.');
            return redirect()->back();
        } else {
            Session::flash('not-permitted', 'You can only view your own users.');
            return redirect()->back();
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->with('roles')->first();

        if ($user->added_by === Auth::user()->id) {
            $roles = Role::where('name', 'clerk')->orWhere('name','executive')->get();
            $user_roles = $user->roles()->pluck('id', 'id')->toArray();
            return view('backend/modules/user/edit', compact('user', 'roles', 'user_roles'));
        } elseif ($user->id === Auth::user()->id)  {
            Session::flash('not-permitted', 'You can not edit yourself. To do this, got to your profile');
            return redirect()->back();
        } else {
            Session::flash('not-permitted', 'You can only edit your own users');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $user = User::find($id);

         if ($user->added_by === Auth::user()->id) {
             // Validate the request...
            $this->validate($request, [
                    'name' => 'required|string|max:255',
                    'email' => [
                        'required', 'string', 'email', 'max:255', 
                        Rule::unique('users')->ignore($user->id),
                    ],
                    'phone' => 'required|numeric|max:255',
                    'password' => 'required|string|min:6',
                ]);
            
            // update into DB
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);

            //save into DB
            $user->save();

            DB::table('role_user')->where('user_id',$id)->delete();
            $user->attachRoles($request->roles);

            //Send Email
            Mail::to($user->email)->send(new UserUpdatedEmail($user));
            
            //Flash Message
            Session::flash('update-user', 'This user was updated successfully!');

            // Redirect to view the post with saved changes
            return redirect()->route('users.show', $user->id);
         } else {
             Session::flash('not-permitted', 'You can only edit your own users');
             return redirect()->back();
         }

    }

      /**
     * Show the form for changing the role of a 
     * specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getChangeRole($id)
    {
        $user = User::where('id', '=', $id)->first();

        if (Auth::user()->hasRole('admin')) {
            $roles = Role::all();
        } else {
            if ($user->added_by === Auth::user()->id) {
                $roles = Role::where('name', 'clerk')->orWhere('name', 'executive')->get();
            } elseif ($user->id === Auth::user()->id) {
                Session::flash('not-permitted', 'You can not modify your own role');
                return redirect()->back();
            } else {
                Session::flash('not-permitted', 'You can only modify the roles of your users');
                return redirect()->back();
            }
            
        }
        $user_roles = $user->roles()->pluck('id', 'id')->toArray();
        return view('backend/modules/user/change-role', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Change the role of a specified resource
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postChangeRole(Request $request, $id)
    {
        $user = User::find($id);

        if ($user->added_by === Auth::user()->id) {
             DB::table('role_user')->where('user_id',$id)->delete();
            $user->attachRoles($request->roles);

            //Send Email
            Mail::to($user->email)->send(new RoleChangedEmail($user));
            
            //Flash Message
            Session::flash('change-role', 'The role was changed successfully!');

            // Redirect to view the post with saved changes
            return redirect()->route('users.show', $user->id);
        } else {
            Session::flash('not-permitted', 'You can only modify the roles of your users');
            return redirect()->back();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user->added_by === Auth::user()->id) {
            $user->delete();

            Session::flash('delete-user', 'User has been deleted successfully');
            return redirect()->route('users.index');
        } elseif ($user->id === Auth::user()->id) {
            Session::flash('not-permitted', 'You can not delete yourself');
            return redirect()->back();
        } else {
            Session::flash('not-permitted', 'You can only delete your own users');
            return redirect()->back();
        }
        

        
    }

    /**
     * Deactivate the specified user to prevent login.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deactivate($id)
    {
        $user = User::findOrFail($id);

        if (Auth::user()->hasRole('admin')) {
            if ($user->hasRole('guest') || $user->hasRole('executive')) {
                DB::table('users')->where('id', $user->id)->update(['active' => false]);

                //Send Email
                Mail::to($user->email)->send(new UserDeactivatedEmail($user));

                Session::flash('deactivate-user', 'User has been deactivated successfully');
                return redirect()->route('users.index');
            }
        } else {
            if ($user->added_by === Auth::user()->id) {
                DB::table('users')->where('id', $user->id)->update(['active' => false]);

                //Send Email
                Mail::to($user->email)->send(new UserDeactivatedEmail($user));

                Session::flash('deactivate-user', 'User has been deactivated successfully');
                return redirect()->route('users.index');
            } elseif ($user->id === Auth::user()->id) {
                Session::flash('not-permitted', 'You can not deactivate yourself');
                return redirect()->back();
            } else {
                Session::flash('not-permitted', 'You can only deactivate your own users');
                return redirect()->back();
            }
            
        }
    }

    /**
     * Activate the specified user to prevent login.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $user = User::findOrFail($id);

        if (Auth::user()->hasRole('admin')) {
            if ($user->hasRole('guest') || $user->hasRole('executive')) {
                DB::table('users')->where('id', $user->id)->update(['active' => true]);

                //Send Email
                Mail::to($user->email)->send(new UserActivatedEmail($user));

                Session::flash('activate-user', 'User has been activated successfully');
                return redirect()->route('users.index');
            }
        } else {
            if ($user->added_by === Auth::user()->id) {
                DB::table('users')->where('id', $user->id)->update(['active' => true]);

                //Send Email
                Mail::to($user->email)->send(new UserActivatedEmail($user));

                Session::flash('aactivate-user', 'User has been activated successfully');
                return redirect()->route('users.index');
            } else {
                Session::flash('not-permitted', 'You can only activate your own users');
                return redirect()->back();
            }
            
        }
    }
}
