<?php

namespace App\Http\Controllers\Backend;

use App\Role;
use App\Permission;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::orderBy('id', 'asc')->paginate(10);
        return view('backend/modules/role/index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();
        return view('backend/modules/role/create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:15|unique:roles,name',
            'display_name' => 'required|min:5|max:30',
            'description' => 'required|min:5|max:255'
        ]);

        $role = new Role;
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();

       if ($request->permissions) {
           $role->attachPermissions($request->permissions);
       }

       Session::flash('new-role', 'You have successfully created the '. $role->name . ' role');
       return redirect()->route('roles.show', $role->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::findOrFail($id);
        $users = $role->users()->paginate(10);
        return view('backend/modules/role/show', compact('role', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::where('id', $id)->with('permissions')->first();
        $permissions = Permission::all();
        $role_permissions = $role->permissions()->pluck('id','id')->toArray();
        return view('backend/modules/role/edit', compact('role', 'permissions', 'role_permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        
        $this->validate($request, [
            'name' => [
                'required', 'max:15', 
                Rule::unique('roles')->ignore($role->id),
            ],
            'display_name' => 'required|min:5|max:30',
            'description' => 'required|min:5|max:255'
        ]);

        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();

        DB::table('permission_role')->where('role_id',$id)->delete(); // Delete the permission relationships first and then save

        if ($request->permissions) {
           $role->attachPermissions($request->permissions);
        }
        
        Session::flash('update-role', 'You have successfully updated the '. $role->name . ' role');
        return redirect()->route('roles.show', $id);
    }

    /**
     * Display the form for attaching permissions.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getChangePermission($id)
    {
        $role = Role::where('id', '=', $id)->first();
        $permissions = Permission::all();
        $role_permissions = $role->permissions()->pluck('id', 'id')->toArray();
        return view('backend/modules/role/attach-permission', compact('role', 'permissions', 'role_permissions'));
    }

    /**
     * Attach permissions to the retrieved role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postChangePermission(Request $request, $id)
    {
        $role = Role::find($id);
        DB::table('permission_role')->where('role_id', $id)->delete();

        if($request->permissions) {
            $role->attachPermissions($request->permissions);
        }

        Session::flash('role-permission', 'Permissions belonging to ' . $role->name . ' have been successfully updated');
        return redirect()->route('roles.show', $role->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id); // Pull back a given role
        // Regular Delete
        $role->delete(); // This will work no matter what

        Session::flash('delete-role', 'The role has been deleted successfully');
        return redirect()->route('roles.index');
    }
}
