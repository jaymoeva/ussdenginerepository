<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use DB;
use Request as Newrequest;

class GamesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role:admin');
    }
    public function addtrivia(){
        //add the trivia
        $data = Newrequest::all();
        //dd($data);
        $company_id = Auth::user()->company_id;
        //dd($company_id);
        $name = $data['trivianame'];
        $type = $data['triviatype'];
        $description = $data['triviadesc'];

        //generate a url for the project
        $endpoint_internal = url("/project/".preg_replace('/\s+/', '',str_replace(' ','',$name)));
        //save the data to the DB
        $id =  DB::table('trivia')->insertGetID(array('company_id'=>$company_id,'trivia_name'=>$name, 'trivia_type'=>$type,'description'=>$description,'url_endpoint'=>$endpoint_internal));
        log::debug(json_encode($data));
        //view trivia of a specific id

        return redirect()->action('GamesManagementController@viewtrivia', ['id'=>$id]);

    }
    public function addtriviaquestions(){
        $data = Newrequest::all();
        //dd($data);
        $question = $data['question'];
        $answer_a = $data['answer_a'];
        $answer_b = $data['answer_b'];
        $answer_c = $data['answer_c'];
        $answer = $data['answer'];
        $trivia_id = $data['trivia_id'];
        //insert to DB
        DB::table('trivia_questions')->insertGetId(array('trivia_id'=>$trivia_id,"question"=>$question,"choice_a"=>$answer_a,"choice_b"=>$answer_b,"choice_c"=>$answer_c ,"answer"=>$answer));
        return back()->withInput();
    }
    public function deletetrivia(){
        $data = Newrequest::all();
        $id = $data['id'];
        DB::table('trivia')->where(array('id'=>$id))->delete();
        return back()->withInput();
    }
}
