<?php

namespace App\Http\Controllers\Backend;

use App\User;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::orderBy('id', 'asc')->paginate(10);
        return view('backend/modules/permission/index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/modules/permission/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|alpha_dash|unique:permissions,name',
            'display_name' => 'required|min:5|max:100',
            'description' => 'required|min:5|max:255'
        ]);

        $permission = new Permission;
        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();

        Session::flash('new-permission', 'You have successfully created the '. $permission->name . ' permission');
        return redirect()->route('permissions.show', $permission->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::findOrFail($id);
        $users = User::wherePermissionIs($permission->name)->paginate(10);
        return view('backend/modules/permission/show', compact('permission', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view('backend/modules/permission/edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::find($id);

        $this->validate($request, [
            'name' => [
                'required', 'alpha_dash', 
                Rule::unique('permissions')->ignore($permission->id),
            ],
            'display_name' => 'required|min:5|max:100',
            'description' => 'required|min:5|max:255'
        ]);

        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();
        
        Session::flash('update-permission', 'You have successfully updated the '. $permission->name . ' permission');
        return redirect()->route('permissions.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id); // Pull back a given permission
        // Regular Delete
        $permission->delete(); // This will work no matter what

        Session::flash('delete-permission', 'The permission has been deleted successfully');
        return redirect()->route('permissions.index');
    }
}
