<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class ReportController extends Controller
{
    public function getGraphs()
    {
    	$user = Auth::user();
    	return view('backend/modules/analytics/graphs', compact('user'));
    }

    public function getReports()
    {
    	if (Auth::user()->hasRole('admin')) {
    		$users = User::orderBy('id', 'asc')->paginate(10);
    		$verified = User::verified()->orderBy('id', 'asc')->paginate(10);
	    	$unverified = User::unverified()->orderBy('id', 'asc')->paginate(10);
	    	$active = User::active()->orderBy('id', 'asc')->paginate(10);
	    	$inactive = User::inactive()->orderBy('id', 'asc')->paginate(10);
    	} else {
    		$users = User::executive()->orderBy('id', 'asc')->paginate(10);
    		$verified = User::verified()->executive()->orderBy('id', 'asc')->paginate(10);
	    	$unverified = User::unverified()->executive()->orderBy('id', 'asc')->paginate(10);
	    	$active = User::active()->executive()->orderBy('id', 'asc')->paginate(10);
	    	$inactive = User::inactive()->executive()->orderBy('id', 'asc')->paginate(10);
    	}
    	
    	return view('backend/modules/analytics/reports', compact('users', 'verified', 'unverified', 'active', 'inactive'));
    }
}
