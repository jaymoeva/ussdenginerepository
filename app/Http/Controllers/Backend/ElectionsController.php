<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use DB;
use Request as Newrequest;
use Carbon\Carbon;
class ElectionsController extends Controller
{
	public function __construct()
    {
         $this->middleware('auth');
        // $this->middleware('role:admin');
    }
    //
	public function create(){
		//add new polls
		$starts =  Carbon::now();
		$endate = $starts->addDays(2);
		$name = "Election Polls";
		$question = "do you support william rutos stand on constitutional chnage?";
		$response = json_encode(array(1=>"Yes",2=>"No",3=>"May be"));
		$desc = "Further Descriptions on the way";
		$company_id = Auth::user()->company_id;
		//save to DB
		$id = DB::table("polls")->insertGetId(array("name"=>$name,"question"=>$question,"responses"=>$response,"desc"=>$desc,"company_id"=>$company_id, "starts"=>$starts,"ends"=>$endate));
		//save the endpoints
		 $endpoint_internal = url("api/polls/".$id."/".preg_replace('/\s+/', '',str_replace(' ','',$name)));
		 DB::table("polls_endpoints")->insert(array("polls_id"=>$id,"company_id"=>$company_id,"endpoint"=>$endpoint_internal));
		 //redirect to the individual polls ID
		//return view('gamesmanagement/poll-details');
		return redirect()->route('polls.details', ["id"=>$id]);
	}
	public function delete_polls(){
		//delete exsting polls
	}
	public function edit(){
		//edit existing polls
	}
	public function view(){
		//existing polls
		$company_id = Auth::user()->company_id;
		$polls  = DB::table('polls')->where(array("company_id"=>$company_id))->paginate(10);
		//dd($polls);
		return view('gamesmanagement/all-polls', compact('polls'));
	}
	public function getinvidualpoll(){
		//pass the ID 
		$data = Newrequest::all();
		$id = $data['id'];
		$polls  = DB::table('polls')
				->join('polls_endpoints','polls_endpoints.polls_id','polls.id')
				->where(array("polls.id"=>$id))
				->get();
		//dd($polls); 
		foreach($polls as $poll_details){}
		$participants = DB::table('polls_participants')->where(array('polls_id'=>$id))->count();
		$responses = json_decode($poll_details->responses);
		$particpantslist = DB::table('polls_participants')->limit(1)->get();
		return view('gamesmanagement/poll-details',compact('participants','poll_details','responses','particpantslist'));
	}
	public function getparticipants(){
		//get the particpants
		$data = Newrequest::all();
		log::debug($data);
		$id = $data['id'];
		$particpantslist = DB::table('polls_participants')
							->where('id','>',$id)
							->limit(20)
							->get();
		return $particpantslist;
	}
}
