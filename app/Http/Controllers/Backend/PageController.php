<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;
use Image;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use DB;
use Request as Newrequest;
use App\Company;

class PageController extends Controller
{
	/**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
       // $this->middleware('role:admin')->only('getDashboard');

    }

    public function getDashboard() 
    {
        $user = Auth::user();

        $myCompany = DB::table('companies')->where('owner_id', $user->id)->first();
      
        if ($user->hasRole('admin')) {
           $users = User::all();
           $latest_users = User::orderBy('id', 'desc')->take(5)->get();
           $companies = Company::all();
           $latest_companies = Company::orderBy('id','desc')->take(5)->get();

           foreach ($latest_companies as $company) {
               $owner = User::where('id', '=', $company->owner_id)->first();
               $company_users = User::where('company_id', '=', $company->id)->get();
           } 
        } elseif ($user->hasRole('executive')) {
            if (!empty($myCompany)) {
                $latest_users = User::where('company_id', '=', $myCompany->id)->orderBy('id', 'desc')->take(5)->get();
                $users = User::where('company_id', '=', $myCompany->id)->paginate(10);
            }
        }

    	return view('backend/pages/dashboard', compact('users', 'user', 'companies', 'users', 'myCompany', 'latest_users', 'latest_companies', 'company_users', 'owner'));
    }

    public function getStaticUSSD() 
    {
        $user = Auth::user();

    	return view('backend/modules/services/ussd/static', compact('user'));
    }

    public function getDynamicUSSD() 
    {
        $user = Auth::user();

    	return view('backend/modules/services/ussd/dynamic', compact('user'));
    }

    public function getStaticSMS()
    {
        $user = Auth::user();
        return view('backend/modules/services/sms/static', compact('user'));
    }

    public function getDynamicSMS()
    {
        $user = Auth::user();
        return view('backend/modules/services/sms/dynamic', compact('user'));
    }

    // We can pass the id argument to this function and inject it into the URI.. It's blank for now for dev purposes
    public function getShortcodeDetails(Request $request)
    {
        $user = Auth::user();
		log::debug($request);
        $id = $request['id'];
		
        //get the project details
        $projects = DB::table('project')
            ->join('project_status','project_status.id_status','project.status')
            ->join('project_endpoints','project_endpoints.project_id','project.id')
            ->join('sms_type','sms_type.id','project.sms_type')
            ->select('project.name as project_name','project_status.*','sms_type.name as sms_typename','project.id as project_id','project_endpoints.*')
            ->where(array('project.id'=>$id))//$user->company_id))
            ->get();
		$apidetails = DB::table('sms_api')->where(array('project_id'=>$id))->get();
       //dd($projects);
        return view('backend/modules/services/sms.shortcode-details', compact('user','projects','apidetails'));
    }

    public function getProfile()
    {
        $user = Auth::user();
        return view('backend/pages/profile', compact('user'));
    }

    /**
     * Change User Image
     * @return \Illuminate\Http\Response
     */
    public function postChangeAvatar (Request $request) 
    {
         // Validate the request...
        $this->validate($request, [
            'avatar' => 'required|image',
            ]);
            
        if($request->hasFile('avatar'))
        {
        
            $image = $request->file('avatar');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(160, 160)->save(public_path('/img/avatars/' . $filename));

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        Session::flash('avatar-change', 'Your avatar was changed successfully!');
        return redirect()->back();
    }

    /**
     * Change User Details
     * @return \Illuminate\Http\Response
     */
    public function postChangeDetails(Request $request)
    {
        $user = $request->user();

        // Validate the request...
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => "required|string|email|max:255|unique:users,email,$user->id",
            'password' => 'required|string|min:6|confirmed',
            ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->save();

        Session::flash('profile-updated', 'Your profile has been updated successfully');
        return redirect()->back();
    }

    /*
     * Show the form for adding company
     *
     */
    public function getAddCompany()
    {
        $user = Auth::user();
        return view('backend/pages/account-complete', compact('user'));
    }

    /*
     * Post new company into the database
     *
     */
    public function postAddCompany(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255|unique:companies,name',
            'address' => 'required|string|max:255',
            'telephone' => ['required','regex:/(07)[0-9]{8}/'],
        ]);

        $company = new Company;
        $company->name = $request->name;
        $company->address = $request->address;
        $company->telephone = $request->telephone;
        $company->user_id = $request->user();

        $company->save();

        Session::flash('company-added', 'Company ' . $company->name . ' was added successfully');
    }

}
