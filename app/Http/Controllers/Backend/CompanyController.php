<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Auth;
use DB;
use App\User;
use App\Role;

class CompanyController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|executive');
        $this->middleware('role:admin')->only('index', 'activateCompany', 'deactivateCompany', 'show');
        $this->middleware('role:executive')->only('create', 'store', 'edit', 'update', 'destroy', 'manageCompany');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $companies = Company::orderBy('id', 'asc')->paginate(10);    
        return view('backend/modules/company/index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $myCompany = DB::table('companies')->where('owner_id', $user->id)->first();

        if (empty($myCompany)) {
            return view('backend/modules/company/create', compact('user'));
        } else {
            Session::flash('company-exists', 'You already own a company! You can only edit and update but not add a new one');
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $myCompany = DB::table('companies')->where('owner_id', $user->id)->first();

        if (empty($myCompany)) {
             $request->validate([
                'name' => 'required|string|max:255|unique:companies,name',
                'address' => 'required|string|max:255',
                'telephone' => ['required','regex:/(07)[0-9]{8}/'],
            ]);

            $company = new Company;
            $company->name = $request->name;
            $company->address = $request->address;
            $company->telephone = $request->telephone;
            $company->owner_id = $user->id;

            $company->save();

            $user->company_id = $company->id;
            $user->active = true;
            $user->save();


            // Delete the default guest role
            DB::table('role_user')->where('user_id',$user->id)->delete();

            // Attach guest role
            $user->attachRole(Role::where('name','executive')->first());

            Session::flash('company-added', 'Company was added successfully. You are now an executive but your company is due activation');
            return redirect()->route('dashboard');
        } else {
            Session::flash('company-exists', 'You already own a company! You can only edit and update it but not add a new one');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $company = Company::findOrFail($id);

        $users = User::where('company_id', '=', $company->id)->get();

        return view('backend/modules/company/show', compact('company', 'user', 'users'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$company = Company::find($id);
        $user = Auth::user();
        $company = Company::where('owner_id', $user->id)->first();

        if ($company->status) {
           if (Gate::allows('is-my-company', $company)) {
                // Validate the request...
                $this->validate($request, [
                     'name' => [
                            'required', 'string', 'max:255', 
                            Rule::unique('companies')->ignore($company->id),
                        ],
                    'address' => 'required|string|max:255',
                    'telephone' => ['required','regex:/(07)[0-9]{8}/'],
                ]);
                
                $company->name = $request->name;
                $company->address = $request->address;
                $company->telephone = $request->telephone;
                $company->owner_id = Auth::user()->id;

                //save into DB
                $company->save();
                
                //Flash Message
                Session::flash('update-company', 'This company has been updated successfully!');

                // Redirect to view the company with saved changes
                return redirect()->route('company.manage', compact('company'));
            } else {
                Session::flash('not-permitted', 'You are not allowed to perform this operation');
                return redirect()->back();
            }
        } else {
            Session::flash('unapproved', 'You can only perform this operation after your company is approved');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        if ($company->status) {
            if (Gate::allows('is-my-company', $company)) {
                $company->delete();
                Session::flash('company-deleted', 'You have successfully deleted this company');
                return redirect()->back();
            } else {
                Session::flash('not-permitted', 'You are not allowed to perform this operation');
                return redirect()->back();
            }
        } else {
            Session::flash('unapproved', 'You can only perform this operation after your company is approved');
            return redirect()->back();
        }
    }

    /**
     * Activate company 
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function activateCompany($id)
    {
        $company = Company::findOrFail($id);

        DB::table('companies')->where('id', $company->id)->update(['status' => true]);

        //Send Company Owner Email
        

        Session::flash('company-activated', 'Company has been activated successfully');
        return redirect()->route('companies.index');
    }

    /**
     * Deactivate company 
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function deactivateCompany($id)
    {
        $company = Company::findOrFail($id);

        DB::table('companies')->where('id', $company->id)->update(['status' => false]);

        //Send Company Owner Email
        

        Session::flash('company-deactivated', 'Company has been deactivated successfully');
        return redirect()->route('companies.index');
    }

    /**
     *
     * Show view for managing one's company
     *
     */
    public function manageCompany()
    {
        $user = Auth::user();
        $company = DB::table('companies')->where('owner_id', $user->id)->first();
        if (!empty($company)) {
            $users = User::where('company_id', '=', $company->id)->paginate(10);
        } else {
            Session::flash('company-first', 'You can only access this page after adding your company');
            return redirect()->back();
        }

        return view('backend/modules/company/manage', compact('company', 'users'));
    }
}
