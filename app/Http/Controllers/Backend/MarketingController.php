<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
use App\Http\Controllers\Controller;
use Auth;
use App\emailGroup;
use App\emailContact;

class MarketingController extends Controller
{
    public function emailMarketing(Request $request)
    {
        $user = $request->user();
        $company_id = $user->company_id;
        $email_groups = emailGroup::where('company_id', '=', $company_id)->get();
        return view('gamesmanagement.send-email', compact('email_groups'));
    }

    public function postSendEmail(Request $request)
    {
        $request->validate([
            'subject' => 'required|max:100',
            'message' => 'required',
            'group_id' => 'required|numeric'
        ]);

        $subject = $request->subject;
        $message = $request->message;
        $group = $request->group;
        $time = \Carbon\Carbon::now();

        DB::table('emails')->insert(
            [
                'subject' => $subject,
                'message' => $message,
                'group_id' => $group,
                'created_at' => $time,
                'updated_at' =>$time
            ]
        );

        $contacts = emailContact::where('group_id', $group->id)-get();

        $noOfContacts = count($contacts);

        Session::flash('success', 'Bulk email sent successfully to ' . $noOfContacts . ' contacts');
        return redirect()->back();


    }

    public function emailGroups(Request $request)
    {
    	$user = $request->user();
    	$company_id = $user->company_id;
    	$email_groups = emailGroup::where('company_id', '=', $company_id)->paginate(10);
        return view('gamesmanagement.email-groups', compact('email_groups'));
    }

    public function addEmailGroup(Request $request)
    {
    	$request->validate([
    		'name' => 'required|string',
    		'description' => 'required',
    	]);

    	$user = $request->user();

        $group = new emailGroup;
        $group->name = $request->name;
        $group->description = $request->description;
        $group->company_id = $user->company_id;
        $group->save();

    	Session::flash('success', 'Email group has been added successfully');
    	return redirect()->back();
    }

    public function showEmailGroup($id)
    {
      $group = emailGroup::findOrFail($id);
      $contacts = emailContact::where('group_id', $group->id)->paginate(10);
      return view('gamesmanagement.show-email-group', compact('group', 'contacts'));
    }

    public function addEmailContact(Request $request, $id)
    {
        $group = emailGroup::findOrFail($id);

        $request->validate([
            'email' => 'required|email',
        ]);

        $contact = new emaiContact;
        $contact->email = $request->email;
        $contact->group_id = $group->id;
        $contact->save();

        Session::flash('success', 'Contact has been added successfully');
        return redirect()->back();
    }

    public function showEmailContact($id)
    {
      $contact = DB::table('email_contacts')->where('id', '=', $id)->first();
      return view('gamesmanagement.email-contact', compact('contact'));
    }

    public function editEmailGroup($id)
    {
      $group = emailGroup::findOrFail($id);
      return view('gamesmanagement.edit-email-group', compact('group'));
    }

    public function updateEmailGroup($id)
    {
       $group = emailGroup::findOrFail($id); 

       $request->validate([
            'name' => 'required|string',
            'description' => 'required',
        ]);

        $user = $request->user();

        $group = new emailGroup;
        $group->name = $request->name;
        $group->description = $request->description;
        $group->company_id = $user->company_id;
        $group->save();

        Session::flash('success', 'Group updated successfully');
        return redirect()->route('marketingemail.showgroup', $group->id);
    }

    public function smsMarketing()
    {
        return view('gamesmanagement.send-sms');
    }

    public function smsGroups(Request $request)
    {
        return view('gamesmanagement.sms-groups');
    }

    public function showSMSGroup()
    {
      return view('gamesmanagement.show-sms-group');
    }

    public function editSMSGroup()
    {
        return view('gamesmanagement.edit-sms-group');
    }

    public function showSMSContact()
    {
        return view('gamesmanagement.sms-contact');
    }
}
