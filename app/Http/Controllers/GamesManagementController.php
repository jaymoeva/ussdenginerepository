<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use DB;
use Request as Newrequest;
use Illuminate\Support\Facades\Storage;

class GamesManagementController extends Controller
{
      /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         //$this->middleware('auth');
        // $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //trivia functions

    public function trivia(){

    	return view('gamesmanagement.trivia');
    }

    public function viewtrivia(Request $request){
        //view trivia of a specific id
        $data = Newrequest::all();
        $id = $data['id'];
        $trivia_details = DB::table('trivia')->where(array('id'=>$id))->get();
        $questions = DB::table('trivia_questions')->where(array('trivia_id'=>$id))->get();
       //var_dump($trivia_details);
    	return view('gamesmanagement.viewtrivia',compact('trivia_details','id','questions'));
    }

    public function listtrivia(){
        $company_id = Auth::user()->company_id;
        $trivia_details = DB::table('trivia')->where(array('company_id'=>$company_id))->get();
    	return view('gamesmanagement.listtrivia',compact('trivia_details'));
    }

    public function getPlayers()
    {
        return view('gamesmanagement/player-management');
    }

    public function getPlayerDetails(/*$id*/)
    {
        return view('gamesmanagement/player-details');
    }

    public function polls(){

        return view('gamesmanagement.electionandpolls');
    }

    public function getAllPolls()
    {
        return view('gamesmanagement/all-polls');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getPollDetails(/*$id*/)
    {
        return view('gamesmanagement/poll-details');
    }

    public function editPoll() 
    {
      return view('gamesmanagement/edit-poll');
    }

    public function lottery(){

        return view('gamesmanagement.lotteries');
    }

    public function lifestyle(){

        return view('gamesmanagement.lifestyle');
    }

    public function createLifestyle()
    {
        return view('gamesmanagement.create-lifestyle');
    }

    public function getLifestyleTypes()
    {
        return view('gamesmanagement/lifestyle-types');
    }

    public function editLifeStyleType()
    {
        return view('gamesmanagement/edit-lifestyle');
    }

    public function lifestyleGamesPresent()
    {
      return view('gamesmanagement/games-present');
    }

    public function createLifestyleGame()
    {
      return view('gamesmanagement/create-lifestyle-game');
    }

    public function editLifestyleGame()
    {
      return view('gamesmanagement/edit-lifestyle-game');
    }

    public function showLifestyleGame()
    {
      return view('gamesmanagement/show-lifestyle-game');
    }

}
