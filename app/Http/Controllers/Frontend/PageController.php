<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function getWelcome() 
    {
    	return view('frontend/pages/welcome');
    }
	
	public function getDocs()
	{
		return view('frontend/pages/docs/docs');
	}

	public function getHowTo()
	{
		return view('frontend/pages/how-to');
	}

	public function getCompleteRegistration() 
	{
		return view('auth/complete-registration');
	}
}
