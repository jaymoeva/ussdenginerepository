<?php

namespace App\Http\Controllers\API;
use Request as Newrequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Http\Controllers\API\SmsController;
class UssdController extends Controller
{
    public $successStatus = 200;

    //
    public function getshortcodes()
    {
        //returns the available shortcodes
        $user = Auth::user();
        if ($user) {
            $role_name = DB::table('roles')->where(array('role_no' => $user->role_id))->value('name');
            if ($role_name == "superadmin") {
                $codes = DB::table('ussd_codes')
                    ->join('ussd_codes_type', 'ussd_codes_type.id', 'ussd_codes.type')
                    //->where(array('company_id'=>$user->company_id))
                    ->get();
            } else {
                $codes = DB::table('ussd_codes')
                    ->join('ussd_codes_type', 'ussd_codes_type.id', 'ussd_codes.type')
                    ->where(array('company_id' => $user->company_id))
                    ->get();
            }
            return response()->json(['success' => $codes], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function getshortcodetype()
    {
        //specific type
        $user = Auth::user();
        if ($user) {
            $codes = DB::table('ussd_codes_type')->get();
            return response()->json(['success' => $codes], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function updatecode()
    {
        //update the short code
    }

    public function deletecode()
    {
        //delete the specific code

    }

    public function createshortcode()
    {
        //get the shortcode details
    }

    public function assignshortcode()
    {
        //assign a code to certain user
    }

    public function generatemenu()
    {
        //accepts the shortcode frontend menu
    }

    public function getsessions()
    {
        //per shortcode
        $data = Newrequest::all();
		log::debug($data);
		DB::enableQueryLog();
        //Log::debug($data);
        $id = $data['id'];
        $sessions = DB::table('ussd_session')
            ->join('project', 'project.id', 'ussd_session.project_id')
            ->select('project.name as project_name', 'ussd_session.*')
            ->where('ussd_session.project_id', $id)
            ->limit(20)
            ->get();
		Log::debug(DB::getQueryLog());
		log::debug(json_encode($sessions));
        return $sessions;
    }

    public function getstatistics()
    {
        $data = Newrequest::all();
        //get the ID and the statistics
        $id = $data['id'];

        $sucess = DB::table('ussd_session')->where(array('project_id' => $id, 'status' => 1))->count();
        $failure = DB::table('ussd_session')->where(array('project_id' => $id, 'status' => 0))->count();

        return array('success' => $sucess, 'failed' => $failure);

    }

    public function ussdlink(Request $request, $id,$type,$name)
    {
        $data = Newrequest::all();
		log::debug($id);
		log::debug($name);
		log::debug($type);
		if($type === "ussd"){
			return $this->process($data,$id);
		}elseif($type === "sms"){ 
			$smscontroller = new SmsController(); 
			$response = $smscontroller->incoming_message($data,$id);
			log::debug("incoming sms ".json_encode($response)); 
		}
		//log::debug();
    //    $project_id = DB::table('routes')->where(array('route_name'=>$id))->pluck('project_id');
        //Ssave the project traffic and sessions to database
        log::debug(json_encode($data));
    }
    public function process($data,$id){
        $data = Newrequest::all();
        $phone = $data['phoneNumber'];
        $session_id = $data['sessionId'];
        $service_code = $data['serviceCode'];
        //$id = 1;
        $ussd_string = $data['text'];
        //use the shortcode to get the call back urldecode
      //  $getcallbackurls = DB::table('d4a_ussd_shortcode')->where('service_code',$service_code)->where('code_status',1)->get();
      //  foreach($getcallbackurls as $getcallbackurl){}
        //dd($getcallbackurl->company_id);
          //get the session status and update the various tables
            $session_status = DB::table('ussd_session')->where('session',$session_id)->count();
            $hopscount = 0;
            $hopscount ++;

            if($session_status == 0){
                $reg_session_id = DB::table('ussd_session')->insertGetId(array(
                    'hops_count'=> $hopscount,
                    'msisdn'=> $phone,
                    'code'=> $service_code,
                    'session'=> $session_id,
                   'project_id'=>$id,
                   // 'company_id'=> $getcallbackurl->company_id,
                    'status'=>1
                ));

                DB::table('ussd_hops')->insert(array(
                    'session_id'=> $reg_session_id,
                    'text'=>$ussd_string,
                    'message'=>1
                ));
            }else{
                $reg_session_id = DB::table('ussd_session')->where('session',$session_id)->value('id');

                DB::table('ussd_session')
                    ->where('session', $session_id)
                    ->increment('hops_count', 1);
                DB::table('ussd_hops')->insert(array(
                    'session_id'=> $reg_session_id,
                    'text'=>$ussd_string,
                    'message'=>1
                ));
            }
			header('Content-type: text/plain');
            return "END Welcome to USSD ENGINE";

//            $response = Curl::to($getcallbackurl->callback_url)
//                ->withData($data)
//                ->returnResponseObject()
//                ->post();
//            Log::debug(json_encode($response));
//            $crawler = new Crawler($response->content);
//            if($response->status == 200){
//                $code = $response->status;
//                $message = ltrim($response->content);
//                $result = substr($message, 0, 3);
//                Log::debug("result".$result);
//                //dd($result);
//                $response = array('message'=>$message,'code'=>$code);
//
//                if($result === "END"){
//                    DB::table('d4a_ussd_hops')->where('session_id',$reg_session_id)
//                        ->where('message',1)
//                        ->update(array('error_code'=>$code,'message'=>$message,'hop_status'=>1));
//                    DB::table('d4a_session')
//                        ->where('session', $session_id)
//                        ->update(array('session_status'=> 2));
//                    return $message;
//                }else if($result === "CON"){
//                    DB::table('d4a_ussd_hops')->where('session_id',$reg_session_id)
//                        ->where('message',1)
//                        ->update(array('error_code'=>$code,'message'=>$message,'hop_status'=>1));
//                    return $message;
//                }else{
//                    $message = "Invalid Network Response, Expecting CON or END from the network";
//                    $response = array('message'=>$message,'code'=>$code);
//                    DB::table('d4a_ussd_hops')->where('session_id',$reg_session_id)->where('message',1)->update(array(
//                        'error_code'=>$code,'message'=>$message));
//                    return $message;
//                }
//                //$response = array('message'=>$res->getBody()->getContents(),'code'=>$res->getStatusCode('reasonPhrase'));
//                Log::debug(json_encode($response));
//
//            }else{
//                $response_message = $crawler->filterXPath('//h1')->text();
//                $code = $response->status;
//                DB::table('d4a_ussd_hops')->where('session_id',$reg_session_id)->where('message',1)->update(array(
//                    'error_code'=>$code,'message'=>$response_message
//                ));
//                $response = array('message'=>$response_message,'code'=>$code);
//                Log::debug(json_encode($response));
//                return $response_message;
//            }

}
}
