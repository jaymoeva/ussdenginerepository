<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use DB;
use Request as Newrequest;
use Ixudra\Curl\Facades\Curl;
class SmsController extends Controller
{
    //
    public function getincomingsms(){
        //get the incoming sms
        $data = Newrequest::all();
		log::debug($data);
        $id = $data['id'];
        $messages = DB::table('sms_incoming')
            ->join('project','project.id','sms_incoming.project_id')
            ->select('sms_incoming.*','project.name as project_name')
            ->where(array('project_id'=>$id))
            ->limit(20)
            ->orderby('id','desc')
            //->first(20);
        //dd($messages);
          ->get();

        return json_encode($messages); 
    }
    public function getoutgoing(){
        //get the outgoing sms
        $data = Newrequest::all();
        $id = $data['id'];
        $messages = DB::table('sms_outgoing')
            ->join('project','project.id','sms_outgoing.project_id')
            ->select('sms_outgoing.*','project.name as project_name')
            ->where(array('sms_outgoing.project_id'=>$id))
            ->limit(20)
            ->orderby('sms_outgoing.id','desc')
            ->get();
        return json_encode($messages);
    }
    public function getstatistics(){
        //get the incoming sms
        $data = Newrequest::all();
        $id = $data['id'];
        $incoming = DB::table('sms_incoming')
            ->where(array('project_id'=>$id))
            ->count();
        $outgoing = DB::table('sms_outgoing')
            ->where(array('project_id'=>$id))
            ->count();
        $statistics = array('incoming'=>$incoming,'outgoing'=>$outgoing);
        return $statistics;
    }
	public function incoming_message($data,$id){
		//
		log::debug('here is the'.json_encode($data));  
		//get the project endpoints 
		$endpoints = DB::table('project_endpoints')->where(array('project_id'=>$id))->pluck('endpoint_external');
		//make curl request to the thirdparty
		$url = json_decode($endpoints)[0];
		$response = Curl::to($url)
        ->withData( $data )
        ->asJson( true )
        ->post();
		log::debug(json_encode($response)); 
		return $response;
	}
	public function getsmsapidetails(){
		//
		$data = Newrequest::all();
		$id = $data['id'];
		$apidetails = DB::table('sms_api')->where(array('project_id'=>$id))->get();
		return $apidetails;
		
	}
	public function smsservice(Request $request,$id,$type,$name){
		$data = Newrequest::all();
		log::debug($id);
		log::debug($name);
		log::debug($type);
		log::debug($data);
		$apikey = @$data['apikey'];
		$linkid = @$data['linkid'];
		$shortcode = @$data['shortcode'];
		$text = @$data['text'];
		$msisdn = @$data['msisdn'];
		$network = @$data['network'];
		
		//get the project details
		$project_details = DB::table('sms_api')->where(array('project_id'=>@$id))->get();
		if(count($project_details) > 0){
			foreach($project_details as $project_detail){}
			if($project_detail->api_key === $apikey){ 
				if($linkid AND $shortcode AND $text AND $msisdn){
					//save to the outgoing databases
					$queue_id = DB::table('sms_outgoing')->insertGetId(array('project_id'=>$id,'msisdn'=>$msisdn,'message'=>$text,'uniqueid'=>$linkid));
					$response = array("status"=>200,"message"=>"Message qeued successfully, ID is".$queue_id);
				}else{
					$response = array("status"=>300,"message"=>"Invalid request parameters");
				}
			}else{
				$response = array("status"=>300,"message"=>"Unauthorized access, Invalid API key");
			}
		}else{
			$response = array("status"=>300,"message"=>"Unauthorized url");
		}
		return $response;
		
	}
}
