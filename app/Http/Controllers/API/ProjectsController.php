<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use DB;
use Request as Newrequest;
use Illuminate\Support\Facades\Storage;

class ProjectsController extends Controller
{
    public $successStatus = 200;
    //create project
	 public function __construct()
    {
        //$this->middleware('auth');
        // $this->middleware('role:admin');
    }
    public function createproject(){
        //new project
        $user = Auth::user();
        $data = Newrequest::all();
        log::debug($data);
        if($data){
        $projectname = $data['appname'];
        $projecttype = $data['type'];
        $sms_type = @$data['smstype'];
        $company_id = $user->company_id;
           // $endpoint_internal = route('project.show', ['project' => $projectname]);;
          
            $url = $data['appurl'];
        //save
        $id = DB::table('project')->insertGetId(array('company_id'=>$company_id, 'name'=>$projectname,'type'=>$projecttype,'status'=>1));
          if($projecttype == 1){
		  $endpoint_internal = url("api/service/".$id."/ussd/".preg_replace('/\s+/', '',str_replace(' ','',$projectname)));//route('project.show', ['project' => $projectname]);;
		  }else if($projecttype == 2){
		 $endpoint_internal = url("api/service/".$id."/sms/".preg_replace('/\s+/', '',str_replace(' ','',$projectname)));//route('project.show', ['project' => $projectname]);;	  
		 $endpoint_sendmessage = url("api/smsservice/".$id."/sendsms/".preg_replace('/\s+/', '',str_replace(' ','',$projectname)));//route('project.show', ['project' => $projectname]);;	  
		 $rand_string = str_random(65);
		 DB::table('sms_api')->insert(array('project_id'=>$id,'url_endpoint'=>$endpoint_internal,'api_key'=>$rand_string,'company_id'=>$company_id,'status'=>1));
        //$APIkey = $rand_string;
		 }
            DB::table('project_endpoints')->insert(array('project_id'=>$id,'company_id'=>$company_id,'endpoint_external'=>$url,'endpoint_internal'=>$endpoint_internal));
        //create folder
      //  $path = public_path().'/projects/' .$projectname;
      //  Storage::makeDirectory($path);
      //  Storage::makeDirectory($projectname);
       // File::makeDirectory($path, $mode = 0777, true, true);
      //  log::debug($path);

        //create Json file
            $projects = DB::table('project')
                ->join('project_status','project_status.id_status','project.status')
                ->select('project.name as project_name','project_status.*','project.id as project_id')
                ->where(array('company_id'=>$company_id,'project.type'=>$projecttype))//$user->company_id))  
                ->get();
            return response()->json($projects);
         }else{
             return response()->json(['error'=>'Empty request'], 300);
        }

    }
    public function getprojects(){
        //existing projects
        $user = Auth::user();
        $data = Newrequest::all();
        log::debug($user); 
       // if($user){
            $projects = DB::table('project')
                ->join('project_status','project_status.id_status','project.status')
                ->select('project.name as project_name','project_status.*','project.id as project_id')
                ->where(array('company_id'=>$user->company_id,'project.type'=>$data['type']))//$user->company_id))
                ->get();
            return response()->json($projects);
            //return response()->json(['success' => $projects], $this-> successStatus);
       //}else{
       //     return response()->json(['error'=>'Unauthorised'], 401);
        //}
    }
    public function getprojectstypes(){
        //update current project
        $types = DB::table('project_type')->get();
        if(count($types) > 0){
            return response()->json(['success' => $types], $this-> successStatus);
        }else{
            return response()->json(['error'=>'No data'], 300);
        }
    }
    public function updateproject(){
        //update current project
    }
    public function urlendpoint(){
        //get the projectname

    }
    public function getprojectsdetails(){ 
        //get the project detauls
        $data = Newrequest::all();
        log::debug($data);
        $id = $data['id'];
        $projects = DB::table('project')
            ->join('project_status','project_status.id_status','project.status')
            ->join('project_endpoints','project_endpoints.project_id','project.id')
            ->select('project.name as project_name','project_status.*','project.id as project_id','project_endpoints.*')
            ->where(array('project.id'=>$id))//$user->company_id))
            ->get();
       // $sucess = DB::table('ussd_session')->where(array('project_id'=>$id,'status'=>1))->count();
      //  $failure = DB::table('ussd_session')->where(array('project_id'=>$id,'status'=>0))->count();
      //  $sessions = array('success'=>$sucess,'failed'=>$failure);
       // return array('project'=>$projects,'sessions'=>$sessions);
        return response()->json($projects);
    }
    public function getsmstype(){
        //open to public
        $types = DB::table('sms_type')->where(array('status'=>1))->get();
        return $types;
    }
}
