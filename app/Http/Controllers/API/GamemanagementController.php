<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use DB;
use Request as Newrequest;
use Illuminate\Support\Facades\Storage;
class GamemanagementController extends Controller
{
    //
    public function create(){
        //create a trivia
        $data = Newrequest::all();
        $name = $data['trivia_name'];
        $type = $data['type'];
        $description = $data['desc'];
        //generate a url for the project
        $endpoint_internal = url("/trivia/".preg_replace('/\s+/', '',str_replace(' ','',$name)));
        //save the data to the DB
       $id =  DB::table('trivia')->insertGetID(array('trivia_name'=>$name, 'trivia_type'=>$type,'description'=>$description,'url_endpoint'=>$endpoint_internal));

    }
    public function listtrivia(){
        $data = DB::table('trivia')->where(array('status'=>1))->get();
        return $data;
    }
    public function gettrivia(){
        //get the trivia
        $data = Newrequest::all();
        $id = $data['id'];
        $question = DB::table('trivia')->where(array('trivia'=>$id))->get();
        return $question;
    }
    public function update_trivia(){
        $data = Newrequest::all();
        $name = $data['trivia_name'];
        $type = $data['type'];
        $id = $data['id'];
        $description = $data['desc'];
        //update

        DB::table('trivia')->where(array('id'=>$id))->update(array('trivia_name'=>$name, 'trivia_type'=>$type,'description'=>$description));

    }
    public function addquestions(){
        //add the trivia questions
        $data = Newrequest::all();
        $question = $data['question'];
        $answer_a = $data['answer_a'];
        $answer_b = $data['answer_b'];
        $answer_c = $data['answer_c'];
        $answer = $data['answer'];
        $trivia_id = $data['trivia_id'];
        //insert to DB
        DB::table('trivia_questions')->insertGetId(array('trivia_id'=>$trivia_id,"question"=>$question,"choice_a"=>$answer_a,"choice_b"=>$answer_b,"choice_c"=>$answer_c ,"answer"=>$answer));

    }
    public function retrieve_questions(){
         //retrieve questions as per the
        $data = Newrequest::all();
        $id = $data['id'];
        $questions = DB::table('trivia_questions')->where(array('trivia_id'=>$id))->get();
        return $questions;
    }
    public function delete_question(){
        $data = Newrequest::all();
        $id = $data['id'];
        DB::table('trivia_questions')->where(array('trivia_id'=>$id))->delete();
    }
    public function delete_trivia(){
        $data = Newrequest::all();
        $id = $data['id'];
        DB::table('trivia')->where(array('id'=>$id))->delete();
    }
    public function incoming_trivia()
    {
        //sms from customers
        $data = Request::all();
        log::debug($data);
        //get the incoming message endpoint then get the triviaID
        //from the triviaID
        $recipient = $data['from'];
        $shortCode = $data['to'];
        $message = $data['text'];
        $keyword = "premiumKeyword"; // $keyword = null;
        $bulkSMSMode = 0;
        $linkId = $data['linkId'];
        $options = array(
            'keyword' => $keyword,
            'linkId' => $linkId,
            'retryDurationInHours' => "3"
        );
        //check table users if exists
        if (@json_decode($message)->time_sent) {
            log::debug("here in postmessagesSMS");
            $this->postMessageSMS(json_decode($message));
        } else {
            //$user_exists = DB::table('sw_whitelist')->where(array('msisdn'=>$recipient))->get();
            //if(count($user_exists) > 0){
           $trivia_id = 2;//DB::table()->where(array('url_'))
            if (strlen($message) > 0) {
                DB::table('sms_triviaincoming')->insertGetId(array("project_id"=>$trivia_id,"msisdn" => $recipient, "shortcode" => $shortCode, "link_id" => $linkId, "message" => $message));
            }
        }
    }
    public function postMessageSMS(){
        //delivery reports
    }
    public function processTrivia()
    {
        //process the trivia
        $incomings = DB::table('sms_triviaincoming')->where(array('status' => 0))->get();
        if (count($incomings) > 0) {
        foreach($incomings as $incoming) {
            //get the trivia question asked
            $active_trivia = DB::table('trivia_participants')->where(array('msisdn' => $incoming->msisdn, 'trivia_id' => $incoming->project_id, 'status' => 0))->get();
            log::debug("active".json_encode($active_trivia));
			if (count($active_trivia) == 0) {
                //the trivia is a new trivia

                //respond to the user
                $questions = DB::table('trivia_questions')->where(array('trivia_id' => $incoming->project_id))->limit(1)->get();
				if(count($questions) > 0){
				foreach ($questions as $question) {}
                $trivia_id = DB::table('trivia_participants')->insertGetId(array('trivia_id' => $incoming->project_id, 'msisdn' => $incoming->msisdn, 'question_id' => $question->id));
                $message = $question->question;
				}else{
				$message =	DB::table('system_messages')->where(array("type"=>"invalid_trivia"))->value('message');
				} 
				//update the message as processed
				DB::table('sms_triviaincoming')->where(array('id'=>$incoming->id))->update(array('status'=>1));
				//log::debug($message);
            } else {
				$triviaid = $incoming->project_id;
				$msisdn  =  $incoming->msisdn;
            //check if the answer is correct 
			foreach($active_trivia as $active){}
			//get the question
			$question_details = DB::table('trivia_questions')->where(array('trivia_id'=>$active->trivia_id,'id'=>$active->question_id))->get();
			//compare the answers
			if(count($question_details) > 0){
				foreach($question_details as $question_detail){}
				$answer = $question_detail->answer;
				$choice = "choice_".strtolower($answer);
				$choice_selected = $question_detail->$choice;
				//get the answer, and the choice
				 similar_text(strtoupper($choice_selected), strtoupper($incoming->message), $similarity_pst);
				 if(number_format($similarity_pst, 0) > 50 OR strtoupper($answer) === strtoupper($incoming->message)){
					 //correct answer
					 $draft = DB::table("system_messages")->where(array('type'=>'correct_answer'))->value('message');
					 $draft_message = str_replace('c_placeholder',strtoupper($answer),$draft);
					 DB::table('trivia_participants')->where(array('question_id'=>$active->question_id,'msisdn'=>$msisdn))
							->update(array("answer"=>$choice_selected,"answer_id"=>$answer,"response"=>$incoming->message,"status"=>1));
					 //$message = str_replace('c_placeholder',strtoupper($answer),$draft);
				 }else{
					 //wrong answerra
					 $draft = DB::table("system_messages")->where(array('type'=>'wrong_answer'))->value('message');
					 $draft_message = str_replace('c_placeholder',strtoupper($answer),$draft);
					 DB::table('trivia_participants')->where(array('question_id'=>$active->question_id,'msisdn'=>$msisdn))
												->update(array("answer"=>$choice_selected,"answer_id"=>$answer,"response"=>$incoming->message,"status"=>2));
				 }
				 DB::enableQueryLog();
				$result = DB::table('trivia_questions')
                ->where('trivia_id',$incoming->project_id)
                    ->whereNotIn('id', function ($q)use ($triviaid,$msisdn) { 
                   // $data = Request::all();
                    //$user_id = $data['user_id'];
                    $q->select('question_id')->where(array('trivia_id'=>$triviaid,"msisdn"=>$msisdn))->from('trivia_participants');
                })
                ->limit(1) 
                ->get();
				log::debug(DB::getQueryLog());
				log::debug(json_encode($result));
					if(count($result) > 0){
						foreach($result as $result_detail){}
						//more trivia
					$trivia_id = DB::table('trivia_participants')->insertGetId(array('trivia_id' => $result_detail->trivia_id, 'msisdn' => $incoming->msisdn, 'question_id' => $result_detail->id));
					$draft = DB::table("system_messages")->where(array('type'=>'next_trivia'))->value('message');
					 $message = $draft_message." ".$draft.". ".$result_detail->question;
					
					}else{
						//no more trivia
						$draft = DB::table("system_messages")->where(array('type'=>'no_trivia'))->value('message');
					 $message = $draft_message." ".$draft.". ";
					}
			}else{
				$message =	DB::table('system_messages')->where(array("type"=>"invalid_trivia"))->value('message');
			}
            //give a responding message} 
            DB::table('sms_triviaincoming')->where(array('id'=>$incoming->id))->update(array('status'=>1));
        }
		//save the trivua to outgoing 
		DB::table('sms_triviaoutgoing')->insertGetId(array('project_id'=>$incoming->project_id,'msisdn'=>$incoming->msisdn, 'message'=>$message));
        }
    }
    }
}
