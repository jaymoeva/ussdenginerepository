<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use DB;
use Request as Newrequest;
use Illuminate\Support\Facades\Storage;

class MarketingController extends Controller
{



   public function postemails(Request $request){
   	$datas = $request->all();

   	$emaildata = $datas['emaildata'];
   	$message = $datas['message'];
   	$singleemail = $datas['singleemail'];

   	if($singleemail){

   		        DB::table('emails')->insert(array(
                'sender'=>Auth::user()->id,
                'recipient'=>$singleemail,
                'recipientgroup'=>"Individual",
                'message'=> $message,
                
            ));

   	}


   	 if(!$emaildata){

            return redirect()->back();
        }

        else{

   	 try {

    	$emaildata = $request->file('emaildata');
        $filename=$emaildata->getClientOriginalName();
        $emaildata->move('importfile',$filename);
        //$project['chemicalimage']=$chemimageName;
        $productfile = $request->file($emaildata);

        
   // dd($filename);

     if (($handle = fopen ( public_path () . '\importfile/'.$filename, 'r' )) !== FALSE) {


        while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {
            // dd($data);
            $mailer = NewRequest::All();
            $mailmessage = $mailer['message'];

            try {
            DB::table('emails')->insert(array(
                'sender'=>Auth::user()->id,
                'recipient'=>$data[0],
                'recipientgroup'=>$data[1],
                'message'=> $mailmessage,
                
            ));
             
            } catch (Exception $e) {
                 fclose ( $handle );
                
            }
        }
       
    }


      } catch (Exception $e) {

        // return $data;
    }

    return redirect()->back();
  		  }
  		}
 	  



    public function postsms(Request $request){
   	$datas = $request->all();

   	$smsdata = $datas['smsdata'];
   	$message = $datas['message'];
   	$singlsms = $datas['singlesms'];

   	if($singlsms){

   		        DB::table('sms')->insert(array(
                'sender'=>Auth::user()->id,
                'recipient'=>$singlsms,
                'recipientgroup'=>"Individual",
                'message'=> $message,
                
            ));

   	}

   	 if(!$smsdata){

            return redirect()->back();
        }

        else{

   	 try {

    	$smsdata = $request->file('smsdata');
        $filename=$smsdata->getClientOriginalName();
        $smsdata->move('importfile',$filename);
        //$project['chemicalimage']=$chemimageName;
        $productfile = $request->file($smsdata);


     if (($handle = fopen ( public_path () . '\importfile/'.$filename, 'r' )) !== FALSE) {


        while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {
            // dd($data);
            $messenger = NewRequest::All();
            $smsmessage = $messenger['message'];

            try {
            DB::table('sms')->insert(array(
                'sender'=>Auth::user()->id,
                'recipient'=>$data[0],
                'recipientgroup'=>$data[1],
                'message'=> $smsmessage,
                
            ));
             
            } catch (Exception $e) {
                 fclose ( $handle );
                
            }
        }
       
    }


      } catch (Exception $e) {

        // return $data;
    }

    return redirect()->back();
    }
   }
}
