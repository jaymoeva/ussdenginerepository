<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use App\Company;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\UserRegisteredEmail;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

   // private $user;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $user->attachRole(Role::where('name','guest')->first());
        return $user;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //Send Email
        Mail::to($user->email)->send(new UserRegisteredEmail($user));

        Session::flash('verify-email', 'Your account has been created. Please verify your email');

        return redirect()->route('login');
    }

    public function verifyEmail($token)
    {
       // $userretrieved = User::whereToken($token)->firstOrFail();
        User::whereToken($token)->firstOrFail()->hasVerified();
       
        Session::flash('confirmed', 'Your email has been verified. Complete your registration by adding your company below');

        return redirect()->route('registration.complete');
    }

    /**
     * Handle the post-basic-registration.
     *
     * @return \App\User
     */
    public function completeRegistration(Request $request) 
    {
            $user = User::orderBy('id', 'desc')->first();

            $request->validate([
                'name' => 'required|string|max:255|unique:companies,name',
                'address' => 'required|string|max:255',
                'telephone' => ['required','regex:/(07)[0-9]{8}/'],
            ]);

            $company = new Company;
            $company->name = $request->name;
            $company->address = $request->address;
            $company->telephone = $request->telephone;

            $company->owner_id = $user->id;
            $company->status = true;

            //dd($company);

            $company->save();

            $user->company_id = $company->id;
            $user->active = true;
            //$user->added_by = $user->id;
            $user->save();

            // Delete the default guest role
            DB::table('role_user')->where('user_id',$user->id)->delete();

            // Attach guest role
            $user->attachRole(Role::where('name','executive')->first());

            Session::flash('registration-complete', 'You have successfully completed the registarion');

            /**
             * Auth::login($user); ==> this can be used too. The second option is better 
             * coz you only have to change the redirect path once and it becomes
             * available to every other view in our application
             *
             **/
            $this->guard()->login($user);

            Session::flash('registration-complete', 'Congratulations. Your are now an executive manager of ' . $company->name);
            return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());

    }
}
