<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class emailContact extends Model
{
    protected $fillable = ['email', 'subscribed', 'group_id'];

    /**
     * One to many relationship with emailGroup::class
     *
     */
    public function group()
    {
    	return $this->belongsTo('App\emailGroup');
    }
}
