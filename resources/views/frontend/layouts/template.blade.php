<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="USSD Engine, Engine, USSD">
        <meta name="author" content="Edgetech Consults Ltd">

        <title>{{ config('app.name') }} | @yield('title')</title>

        <!-- Core Css -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}">

        <!-- Icon Font -->
        <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/flaticon.css') }}">

        <!-- Icheck -->
        <link rel="stylesheet" href="{{ asset('plugins/iCheck/blue.css') }}">

        @stack('custom-styles')

        <!-- Theme Css -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

        <!-- Override Css -->
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        
    </head>
    <body id="page-top" class="landing-page no-skin-config">
        <!-- Navbar goes here -->
        @include('frontend/partials/_header')

        @yield('content')

        @include('frontend/partials/_footer')

        <!-- Core scripts -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('plugins/metis-menu/jquery.metisMenu.js') }}"></script>
        <script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

        <!-- Plugin scripts for better user experience -->
        <script src="{{ asset('js/inspinia.js') }}"></script>
        <script src="{{ asset('plugins/pace/pace.min.js') }}"></script>
        <script src="{{ asset('plugins/wow/wow.min.js') }}"></script>

        <!-- Custom scripts -->
        <script src="{{ asset('js/custom.js') }}"></script>
        <!-- iCheck -->
        <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                });
            });
        </script>
        @stack('scripts')
    </body>
</html>
