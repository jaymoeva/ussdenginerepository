<section id="contact" class="gray-section contact">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Contact Us</h1>
                <p>We return E-Mails and Chats promptly. We dont mind a call anytime of the day and "night". <br>
                We transform imaginations to ideas to solve problems for all.</p>
            </div>
        </div>
        <div class="row m-b-lg">
            <div class="col-lg-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">Edgetech Consults Ltd.</span></strong><br/>
                    Sports Road, Westlands<br/>
                    PROCMURA Suites<br/>
                    <abbr title="Phone">P:</abbr> +254-720-529868
                </address>
            </div>
            <div class="col-lg-4">
                <p class="text-color">
                    Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis, totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis, totam corporis ea,
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="mailto:support@edgetech.co.ke" class="btn btn-primary">Mail Us</a>
                <p class="m-t-sm">
                    Or call us on skype
                </p>
                <ul class="list-inline social-icon">
                    <li><a href="#"><i class="fa fa-skype"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; {{ date('Y') }} Edgetech Consults Ltd</strong><br/> Built with class!!</p>
            </div>
        </div>
    </div>
</section>