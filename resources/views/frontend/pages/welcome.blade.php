@extends('frontend/layouts/template')

@section('title')
	Welcome
@endsection

@section('content')
	<!-- Slider with hero images -->
	<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
	    <ol class="carousel-indicators">
	        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
	        <li data-target="#inSlider" data-slide-to="1"></li>
	    </ol>
	    <div class="carousel-inner" role="listbox">
	        <div class="item active">
	            <div class="container">
	                <div class="carousel-caption blank">
	                    <h1>It's all about <br/> promoting your business</h1>
	                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
	                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
	                </div>
	            </div>
	            <!-- Set background for slide in css -->
	            <div class="header-back one"></div>

	        </div>
	        <div class="item">
	            <div class="container">
	                <div class="carousel-caption blank">
	                    <h1>We create meaningful <br/> interfaces that inspire.</h1>
	                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
	                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
	                </div>
	            </div>
	            <!-- Set background for slide in css -->
	            <div class="header-back two"></div>
	        </div>
	    </div>
	    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
	        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
	    </a>
	    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
	        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	        <span class="sr-only">Next</span>
	    </a>
	</div>

	<!-- // End Slider // -->
	<section class="team">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Api Features</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <i class="fa fa-user fa-5x is-green"></i>
                    <h2><span class="navy">Easy to Use</span></h2>
                    <p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus. </p>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="team-member">
                    <i class="fa fa-laptop fa-5x is-green"></i>
                    <h2><span class="navy">Awesome Design</span></h2>
                    <p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus. </p>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="team-member">
                    <i class="fa fa-cogs fa-5x is-green"></i>
                    <h2><span class="navy">Easy to Customize</span></h2>
                    <p>Lorem ipsum dolor sit amet, illum fastidii dissentias quo ne. Sea ne sint animal iisque, nam an soluta sensibus. </p>
                </div>
            </div>

        </div>
        
    </div>
</section>
@endsection