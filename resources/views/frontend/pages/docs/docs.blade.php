<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="USSD Engine, Engine, USSD Engine Docs, Docs, USSD Docs, API Docs, USSD Engine Documentation, USSD Documentation">
    <meta name="author" content="Edgetech Consults Ltd">
    <title>{{ config('app.name') }} - Docs</title>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/prism/prism.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/elegant_font/css/style.css') }}">
    
    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ asset('css/docs.css') }}">
    <link rel="stylesheet" href="{{ asset('css/docs-override.css') }}">
</head>
<body class="body-green landing-page">

	<div class="page-wrapper">
		<!-- Inlcude Navbar -->
		@include('frontend/partials/_header')
		<div class="doc-wrapper">
			<div class="container">
				<div id="doc-header" class="doc-header text-center" style="padding-top: 30px; padding-bottom: 5px;">
                    <h1 class="doc-title" style="padding-top: 30px;"><i class="icon fa fa-send"></i>API Quick Start</h1>
                    <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Jan 25th, 2016</div>
                </div><!--//doc-header-->

                <div class="doc-body">
                	<div class="doc-content">
                		<div class="content-inner">
                			<section id="download-section" class="doc-section">
                                <h2 class="section-title">Download</h2>
                                <div class="section-block">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec imperdiet turpis. Curabitur aliquet pulvinar ultrices. Etiam at posuere leo. Proin ultrices ex et dapibus feugiat <a href="#">link example</a> aenean purus leo, faucibus at elit vel, aliquet scelerisque dui. Etiam quis elit euismod, imperdiet augue sit amet, imperdiet odio. Aenean sem erat, hendrerit  eu gravida id, dignissim ut ante. Nam consequat porttitor libero euismod congue. 
                                    </p>
                                    <a href="http://themes.3rdwavemedia.com/" class="btn btn-green" target="_blank"><i class="fa fa-download"></i> Download PrettyDocs</a>
                                </div>
                            </section><!--//doc-section-->
                            <section id="installation-section" class="doc-section">
                                <h2 class="section-title">Installation</h2>
                                <div id="step1"  class="section-block">
                                    <h3 class="block-title">Step One</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis.
                                    </p>
                                    <div class="code-block">
                                        <h6>Default code example:</h6>
                                        <p><code>bower install &lt;package&gt;</code></p>
                                        <p><code>npm install &lt;package&gt;</code></p>
                                    </div><!--//code-block-->
                                </div><!--//section-block-->
                                <div id="step2"  class="section-block">
                                    <h3 class="block-title">Step Two</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <h6>Un-ordered list example</h6>
                                            <ul class="list">
        										<li>Lorem ipsum dolor sit amet.</li>
        										<li>Aliquam tincidunt mauris.</li>
        										<li>Ultricies eget vel aliquam libero.
        											<ul>
        												<li>Turpis pulvinar</li>
        												<li>Feugiat scelerisque</li>
        												<li>Ut tincidunt</li>
        											</ul>
        										</li>
        										<li>Pellentesque habitant morbi.</li>
        										<li>Praesent dapibus, neque id.</li>
        									</ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <h6>Ordered list example</h6>
                                            <ol class="list">
        										<li>Lorem ipsum dolor sit amet.</li>
        										<li>Aliquam tincidunt mauris.</li>
        										<li>Ultricies eget vel aliquam libero.
        											<ul>
        												<li>Turpis pulvinar</li>
        												<li>Feugiat scelerisque</li>
        												<li>Ut tincidunt</li>
        											</ul>
        										</li>
        										<li>Pellentesque habitant morbi.</li>
        										<li>Praesent dapibus, neque id.</li>
        									</ol>
                                        </div>
                                    </div><!--//row-->
                                </div><!--//section-block-->
                                <div id="step3"  class="section-block">
                                    <h3 class="block-title">Step Three</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis.
                                    </p>
                                </div><!--//section-block-->
                            </section><!--//doc-section-->
                            <section id="code-section" class="doc-section">
                            	<h2 class="section-title">Code</h2>
                                <div class="section-block">
                                    <p>
                                        <a href="http://prismjs.com/" target="_blank">PrismJS</a> is used as the syntax highlighter here. You can <a href="http://prismjs.com/download.html" target="_blank">build your own version</a> via their website should you need to.
                                    </p>
                                </div><!--//section-block-->
                                <div id="html" class="section-block">
                                	<div class="callout-block callout-success">
                                        <div class="icon-holder">
                                            <i class="fa fa-thumbs-up"></i>
                                        </div><!--//icon-holder-->
                                        <div class="content">
                                            <h4 class="callout-title">Useful Tip:</h4>
                                            <p><span style="color: white;">You can use this online</span> <a href="https://mothereff.in/html-entities" target="_blank">HTML entity encoder/decoder</a> <span style="color: white;">to generate your code examples.</span> </p>
                                        </div><!--//content-->
                                    </div>
                                    <div class="code-block">
                                        <h6>HTML Code Example</h6>
                                        <pre><code class="language-markup">&lt;!DOCTYPE html&gt; 
&lt;html lang=&quot;en&quot;&gt; 
    ...
    &lt;div class=&quot;jumbotron&quot;&gt; 
        &lt;h1&gt;Hello, world!&lt;/h1&gt; 
        &lt;p&gt;...&lt;/p&gt; 
        &lt;p&gt;&lt;a class=&quot;btn btn-primary btn-lg&quot; href=&quot;#&quot; role=&quot;button&quot;&gt;Learn more&lt;/a&gt;&lt;/p&gt; 
    &lt;/div&gt;
    &lt;div class=&quot;jumbotron&quot;&gt; 
        &lt;h1&gt;Hello, world!&lt;/h1&gt; 
        &lt;p&gt;...&lt;/p&gt; 
        &lt;p&gt;&lt;a class=&quot;btn btn-primary btn-lg&quot; href=&quot;#&quot; role=&quot;button&quot;&gt;Learn more&lt;/a&gt;&lt;/p&gt; 
    &lt;/div&gt;
    ...
&lt;/html&gt;</code></pre>
                                    </div><!--//code-block-->
                                </div><!--Section block -->
                            </section><!--//doc-section-->
                            <section id="tables-section" class="doc-section">
                                <h2 class="section-title">Tables</h2>
                                <div class="section-block">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis.
                                    </p>
                                </div>
                                <div class="section-block">
                                    <h6>Basic Table</h6>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Username</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!--//table-responsive-->
                                    <h6>Bordered Table</h6>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Username</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!--//table-responsive-->
                                    <h6>Striped Table</h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Username</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div><!--//table-responsive-->
                                </div><!--//section-block-->
                            </section><!--//doc-section-->
                            <section id="buttons-section" class="doc-section">
                                <h2 class="section-title">Buttons</h2>
                                <div class="section-block">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec imperdiet turpis. Curabitur aliquet pulvinar ultrices. Etiam at posuere leo. Proin ultrices ex et dapibus feugiat <a href="#">link example</a> aenean purus leo, faucibus at elit vel, aliquet scelerisque dui. Etiam quis elit euismod, imperdiet augue sit amet, imperdiet odio. Aenean sem erat, hendrerit  eu gravida id, dignissim ut ante. Nam consequat porttitor libero euismod congue. 
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <h6>Basic Buttons</h6>
                                            <ul class="list list-unstyled">
                                                <li><a href="#" class="btn btn-primary">Primary Button</a></li>
                                                <li><a href="#" class="btn btn-green">Green Button</a></li>
                                                <li><a href="#" class="btn btn-blue">Blue Button</a></li>
                                                <li><a href="#" class="btn btn-orange">Orange Button</a></li>
                                                <li><a href="#" class="btn btn-red">Red Button</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <h6>CTA Buttons</h6>
                                            <ul class="list list-unstyled">
                                                <li><a href="#" class="btn btn-primary btn-cta"><i class="fa fa-download"></i> Download Now</a></li>
                                                <li><a href="#" class="btn btn-green btn-cta"><i class="fa fa-code-fork"></i> Fork Now</a></li>
                                                <li><a href="#" class="btn btn-blue btn-cta"><i class="fa fa-play-circle"></i> Find Out Now</a></li>
                                                <li><a href="#" class="btn btn-orange btn-cta"><i class="fa fa-bug"></i> Report Bugs</a></li>
                                                <li><a href="#" class="btn btn-red btn-cta"><i class="fa fa-exclamation-circle"></i> Submit Issues</a></li>
                                            </ul>
                                        </div>
                                    </div><!--//row-->
                                </div><!--//section-block-->
                            </section><!--//doc-section-->
                		</div>
                	</div><!--doc-content-->
                	<div class="doc-sidebar hidden-xs">
                        <nav id="doc-nav">
                            <ul id="doc-menu" class="nav doc-menu" data-spy="affix" data-offset="10">
                                <li><a class="scrollto" href="#download-section">Download</a></li>
                                <li>
                                    <a class="scrollto" href="#installation-section">Installation</a>
                                    <ul class="nav doc-sub-menu">
                                        <li><a class="scrollto" href="#step1">Step One</a></li>
                                        <li><a class="scrollto" href="#step2">Step Two</a></li>
                                        <li><a class="scrollto" href="#step3">Step Three</a></li>
                                    </ul><!--//nav-->
                                </li>
                                <li>
                                    <a class="scrollto" href="#code-section">Code</a>
                                    <ul class="nav doc-sub-menu">
                                        <li><a class="scrollto" href="#html">HTML</a></li>
                                    </ul><!--//nav-->
                                </li>
                                <li><a class="scrollto" href="#tables-section">Tables</a></li>
                                <li><a class="scrollto" href="#buttons-section">Buttons</a></li>
                            </ul><!--//doc-menu-->
                        </nav>
                    </div><!--//doc-sidebar-->
                </div>
			</div>

		</div>
	</div>
	

	<script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/prism/prism.js') }}"></script>    
    <script src="{{ asset('plugins/jquery-scrollTo/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-match-height/jquery.matchHeight-min.js') }}"></script>
    <script src="{{ asset('js/docs.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>