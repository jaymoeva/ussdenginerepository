@extends('frontend/layouts/template')

@push('custom-styles')
	<!-- Bootstrap Tour -->
    <link href="{{ asset('plugins/bootstrapTour/bootstrap-tour.min.css') }}" rel="stylesheet">
@endpush

@section('title')
    Signup Procedure
@endsection

@section('content')
	<!-- Slider with hero images -->
	<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
	    <div class="carousel-inner" role="listbox">
	        <div class="item active">
	            <div class="container">
	                <div class="carousel-caption blank">
	                    <h1>Step by step<br/> guide to sign up and log in</h1>
	                    <p>Take a tour of your signup and login journey</p>
	                    <p>
	                    	<a class="btn btn-lg btn-primary startTour" href="#"><i class="fa fa-play"></i> Start Tour</a>&nbsp;
	                    	<a class="btn btn-lg btn-primary" href="{{ url('login') }}">Login</a>
	                    </p>
	                </div>
	            </div>
	            <!-- Set background for slide in css -->
	            <div class="header-back one"></div>
	        </div>
	    </div>
	</div>
	<!-- // End Slider -->
	<div class="container-fluid" style="padding-top: 60px;">
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-success">
				  <div class="panel-heading">
				    <h3 class="panel-title">Step 01 - Visit our website</h3>
				  </div>
				  <div class="panel-body" id="step1">
				    Visit our site and click the login/register link.
				  </div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-success">
				  <div class="panel-heading">
				    <h3 class="panel-title">Step 02 - Create account</h3>
				  </div>
				  <div class="panel-body" id="step2">
				    Enter the relevant details to create an account.
				  </div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-success">
				  <div class="panel-heading">
				    <h3 class="panel-title">Step 03 - Verify email address</h3>
				  </div>
				  <div class="panel-body" id="step3">
				    Verify your email address
				  </div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-success">
				  <div class="panel-heading">
				    <h3 class="panel-title">Step 04 - Add company</h3>
				  </div>
				  <div class="panel-body" id="step4">
				    Register your company
				  </div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-success">
				  <div class="panel-heading">
				    <h3 class="panel-title">Step 05 - Log in and access your dashboard</h3>
				  </div>
				  <div class="panel-body" id="step5">
				    Enter your credentials and access the company dashboard.
				  </div>
				</div>
			</div>
		</div>
	</div>
	<!-- Steps -->
	
	
	<!--// End Steps //-->
@endsection

@push('scripts')
	<!-- Bootstrap Tour -->
    <script src="{{ asset('plugins/bootstrapTour/bootstrap-tour.min.js') }}"></script>
    <script src="{{ asset('js/tour.js') }}"></script>
@endpush