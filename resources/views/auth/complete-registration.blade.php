<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="USSD Engine, Engine, USSD">
    <meta name="author" content="Edgetech Consults Ltd">

	<title>{{ config('app.name') }} - Complete Registration</title>

	<!-- Core Css -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">

    <!-- Icon Font -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <!-- Theme Css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body class="gray-bg">
	
	<div class="loginColumns" style="margin-top: 130px;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
            	@include('frontend/alerts/_messages')
                <div class="ibox-content">
                   <form action="{{ route('reg.complete') }}" method="POST">
	                    {{ csrf_field() }}
	                    
	                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	                        <label>Name</label> 
	                        <input type="text" placeholder="Company name" id="name" name="name" class="form-control">
	                        <!-- If password has error -->
	                        @if ($errors->has('name'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('name') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
	                        <label>Address</label> 
	                        <input type="text" placeholder="Company address" id="address" name="address" class="form-control">
	                        <!-- If password has error -->
	                        @if ($errors->has('address'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('address') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                    <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
	                        <label>Telephone</label> 
	                        <input type="text" placeholder="Company telephone" id="telephone" name="telephone" class="form-control">
	                        <!-- If password has error -->
	                        @if ($errors->has('telephone'))
	                            <span class="help-block">
	                                <strong>{{ $errors->first('telephone') }}</strong>
	                            </span>
	                        @endif
	                    </div>
	                    <div>
	                        <button type="submit" class="btn btn-primary btn-block">Add</button>
	                    </div>
	                </form> 
                </div>
            </div>
        </div>
    </div>

    <!-- Core scripts -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

</body>
</html>