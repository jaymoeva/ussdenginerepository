@extends('frontend/layouts/template')

@section('title')
    Login
@endsection

@section('content')
    <div class="loginColumns">
        <!-- Auth Row -->
        <div class="row login-area">
            @include('frontend/alerts/_messages')
            <!-- Login column -->
            <div class="col-md-5">
               <h2 class="login-area-title">I'm Returning!</h2> 
               <!-- Form -->
                <form name= "login" class="m-t" role="form" action="{{ url('login') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->login->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form-control" placeholder="you@example.com" required="" name="email" value="{{ old('email') }}">

                        @if ($errors->login->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->login->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->login->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Password" required="" name="password">
                        @if ($errors->login->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->login->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                    <div class="i-checks">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>&nbsp; Remember Me
                    </div> 
                    <a href="{{ route('password.request') }}">
                        <small>Forgot password?</small>
                    </a>

                    <p class="text-muted text-center">
                        <small>Or sign up with your social networks</small>
                    </p>
                    <p class="text-center social-auth">
                        <a href="#"><img src="{{ asset('img/socials/facebook.png') }}" alt="Facebook"></a>
                        <a href="#"><img src="{{ asset('img/socials/twitter.png') }}" alt="Twitter"></a>
                        <a href="#"><img src="{{ asset('img/socials/github.png') }}" alt="Github"></a>
                        <a href="#"><img src="{{ asset('img/socials/gplus.png') }}" alt="Google Plus"></a>
                    </p>
                </form>
               <!-- // End Form // -->
            </div>
            <!-- // End Login Column // -->

            <!-- Separator -->
            <div class="col-md-2 has-separator">
                <div class="separator"></div>
                <h5 style="margin-left: 52px;">OR</h5>
                <div class="separator push-up"></div>
            </div>
            <!-- // End Separator // -->

            <!-- Register column -->
            <div class="col-md-5">
               <h2 class="login-area-title">Sign Up!</h2> 
               <!-- Form -->
                <form name="signup" class="m-t" role="form" action="{{ url('register') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="John Doe" name="name" value="{{ old('name') }}" required>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form-control" placeholder="you@example.com" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Password" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Register</button>
                </form>
               <!-- // End Form // -->
            </div>
            <!-- // End Register Column // -->
        </div>
       <!-- // End Auth Row // -->
    </div>
@endsection
