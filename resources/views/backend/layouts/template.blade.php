<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
 	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="">
        <meta name="keywords" content="USSD Engine, Engine, USSD">
        <meta name="author" content="Edgetech Consults Ltd">

        <title>{{ config('app.name') }} | @yield('title')</title>

        <!-- Core Css -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}">

        <!-- Icon Font -->
        <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/flaticon.css') }}">

        <!-- Icheck -->
        <link rel="stylesheet" href="{{ asset('plugins/iCheck/blue.css') }}">

        @stack('custom-styles')

        <!-- Theme Css -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

        <!-- Override Css -->
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        <style>
            .breathe {
                animation: beat .5s infinite alternate;
                transform-origin: center;
            }

            /* Heart beat animation */
            @keyframes beat{
                to { transform: scale(1.4); }
            }
        </style>

        @stack('styles')

        
    </head>
<body class="fixed-sidebar">
	<!-- Main Wrapper -->
    <div id="wrapper">
        
        <!-- Lef sidebar with mopdule links -->
        @include('backend/partials/_main-sidebar')
        <!-- // End left sidebar // -->

         <!-- Main Page Wrapper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Header -->
            <div class="row border-bottom">

                @include('backend/partials/_header')
                
            </div>

            <!-- Page Header -->
            @yield('page-header')
            
            <!-- Main Content -->
            <div class="wrapper wrapper-content">
                @include('backend/alerts/_messages')
                @yield('content')

            </div>

            <!-- Footer -->
            @include('backend/partials/_footer')
        </div>
        <!--// Main Page Wrapper -->

    </div>
    <!-- // End Main Wrapper // -->

	<!-- Core scripts -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/metis-menu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    @stack('graph-scripts')

    <!-- Plugin scripts for better user experience -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('plugins/pace/pace.min.js') }}"></script>

    @stack('demo')
    
    <script src="{{ asset('plugins/wow/wow.min.js') }}"></script>

    <!-- Custom scripts -->
    <script src="{{ asset('js/custom.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>

    <!-- Custom scripts -->
    <script src="{{ asset('plugins/steps/jquery.steps.min.js') }}"></script>

    <!-- Jquery Validate -->
    <script src="{{ asset('plugins/validate/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/ussdengine.js') }}"></script>


    @stack('scripts')
</body>
</html>