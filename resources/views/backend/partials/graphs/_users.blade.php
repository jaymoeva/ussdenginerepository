<div class="row">
    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>User Line Chart
                    <small>With custom colors.</small>
                </h5>
            </div>
            <div class="ibox-content">
                <div>
                    <canvas id="lineChart" height="140"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>User Bar Chart</h5>
            </div>
            <div class="ibox-content">
                <div>
                    <canvas id="barChart" height="140"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>