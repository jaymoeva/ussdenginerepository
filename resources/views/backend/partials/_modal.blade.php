<div class="modal inmodal" id="avatar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated flipInX">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <img src="{{ asset('img/avatars/' . $user->avatar) }}" alt="" class="modal-icon img-circle" style="width: 100px; height: 100px;">
                <h6 class="modal-title">Hey {{ $user->name }}</h6>
                <small class="font-bold">Above is your current avatar. However, you can change it below.</small>
            </div>
            <form role="form" action="{{ route('avatar.change') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                   <p><strong>Tip:</strong> Having a profile picture helps us know you better. It is recommended that you upload a clear picture of your face that will make it easy for us to differentiate you from the numerous users of our application. </p>
                        <div class="fileinput fileinput-new input-group form-group{{ $errors->has('avatar') ? ' has-error' : '' }}" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="avatar" required/>
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            <!-- If first name has error -->
                            @if ($errors->has('avatar'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="details" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated flipInX">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h6 class="modal-title">Hey {{ $user->name }}</h6>
                <small class="font-bold">Edit your personal details below.</small>
            </div>
            <form role="form" action="{{ route('profile.update') }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                   <div class="row">
                        <div class="col-md-6 form-group{{ $errors->has('names') ? ' has-error' : '' }}">
                            <input id="other_names" type="text" class="form-control" name="name" value="{{ $user->name }}" required>
                            <!-- If name has error -->
                            @if ($errors->has('other_names'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('other_names') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" placeholder="Email Address" value="{{ $user->email }}" name="email" required>
                        <!-- If email has error -->
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>
                        <!-- If password has error -->
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>