<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="{{ asset('img/avatars/' . Auth::user()->avatar) }}" style="width: 48px; height: 48px;" />
                         </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                         </span> <span class="text-muted text-xs block">{{ Auth::user()->email }} <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ route('user.profile') }}">Profile</a></li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div> 
            </li>
            <li class="{{ active('dashboard') }}">
                <a href="{{ route('dashboard') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Overview</span></a>
            </li>
            @role('admin')
                <li class="{{ active([
                                'companies.*',
                    ]) }}">
                    <a href="{{ route('companies.index') }}"><i class="fa fa-briefcase"></i> <span class="nav-label">Companies </span></a>
                </li>
            @endrole
            @role('executive')
                <li class="{{ active([
                        'company.*'
                    ]) }}">
                   <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label">My Company </span><span class="fa arrow"></span></a> 
                   <ul class="nav nav-second-level collapse">
                       <li class="{{ active('company.manage') }}"><a href="{{ route('company.manage') }}">Manage</a></li>
                   </ul>
                </li>
            @endrole
            <li class="{{ active([
                            'ussd.static',
                            'ussd.dynamic',
                            'sms.static',
                            'details.shortcode',
                            'sms.dynamic'
                ]) }}">                
                        <a href="#"><i class="fa fa-map"></i> <span class="nav-label">Services </span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ active([
                            'ussd.static',
                            'ussd.dynamic'
                        ]) }}">
                        <a href="#"><i class="fa fa-bars"></i>USSD <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level collapse">
                            <li class="{{ active('ussd.static') }}"><a href="{{ route('ussd.static') }}">Static App</a></li>
                            <li class="{{ active('ussd.dynamic') }}"><a href="{{ route('ussd.dynamic') }}">Dynamic App</a></li>
                        </ul>
                    </li>
                    <li class="{{ active([
                            'sms.static',
                            'details.shortcode',
                            'sms.dynamic'
                        ]) }}">
                        <a href="#"><i class="fa fa-envelope"></i> SMS <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level collapse">
                            <li class="{{ active('sms.static') }}"><a href="{{ route('sms.static') }}">Static App</a></li>
                            <li class="{{ active('sms.dynamic') }}"><a href="{{ route('sms.dynamic') }}">Dynamic App</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="{{ active([
                    'trivia',
                    'polls',
                    'lifestyle',
                    'lottery',
                    'viewtrivia',
                    'listtrivia',
                    'listpolls',
                    'poll-details',
                    'players',
                    'player-details',
                    'lifestyle.*',
                    'lifestylegames.*'
                ]) }}">
               <a href="#"><i class="fa fa-futbol-o"></i> <span class="nav-label">Games Management</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ active(['trivia', 'players', 'player-details']) }}"><a href="{{ url('trivia') }}">Trivia</a></li>
                    <li class="{{ active(['listpolls', 'polls', 'poll-details']) }}"><a href="{{ url('polls') }}">Election and Polls</a></li>
                    <li class="{{ active([
                        'lifestyle.*',
                        'lifestylegames.*'
                        ]) }}">
                        <a href="#"><span class="nav-label">Lifestyle </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level collapse">
                            <li class="{{ active('lifestyle.*') }}"><a href="/lifestyle-types">Lifestyle Types</a></li>
                            <li class="{{ active('lifestylegames.*') }}"><a href="{{ route('lifestylegames.index') }}">Games Present</a></li>
                            <li class="{{ active('lifestyle.add_participants') }}"><a href="{{ route('lifestyle.add_participants') }}">Add Participants</a></li>

                        </ul>
                    </li>
                </ul>
            </li>

            <li class="{{ active(['marketingemail.*', 'marketingsms.*']) }}">
                <a href="#"><i class="fa fa-pie-chart"></i> <span class="nav-label">Marketing</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ active('marketingemail.*') }}"><a href="{{ route('marketingemail.send') }}">Email</a></li>
                    <li class="{{ active('marketingsms.*') }}"><a href="{{ route('marketingsms.send') }}">SMS</a></li>
                    <li class=""><a href="">Facebook</a></li>
                    <li class=""><a href="">Twitter</a></li>
                </ul> 
            </li>
            <li class={{ active([
                    'analytics.graphs',
                    'analytics.reports'
                ]) }}>
               <a href="#"><i class="fa fa-bar-chart"></i> <span class="nav-label">Reports / Analytics</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ active('analytics.graphs') }}"><a href="{{ route('analytics.graphs') }}">Graphs</a></li>
                    <li class="{{ active('analytics.reports') }}"><a href="{{ route('analytics.reports') }}">Reports</a></li>
                </ul>
            </li>
            <li class={{ active([
                    'users.*',
                    'verified',
                    'unverified',
                    'active',
                    'inactive',
                    'change-role/',
                    'roles.*',
                    'permissions.',
                    'attach-permission/*'
                ]) }}>
                <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Settings </span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                   <li class={{ active([
                            'users.*',
                            'verified',
                            'unverified',
                            'active',
                            'inactive',
                            'change-role/'
                        ]) }}>
                        <a href="#"><i class="fa fa-user"></i> User Management <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level collapse">
                            <li class="{{ active('users.index') }}"><a href="{{ route('users.index') }}">All</a></li>
                            <li class="{{ active('verified') }}"><a href="{{ route('verified') }}">Verified</a></li>
                            <li class="{{ active('unverified') }}"><a href="{{ route('unverified') }}">Unverified</a></li>
                            <li class="{{ active('active') }}"><a href="{{ route('active') }}">Active</a></li>
                            <li class="{{ active('inactive') }}"><a href="{{ route('inactive') }}">Inactive</a></li>
                        </ul>
                    </li>
                    @role('admin')
                        <li class="{{ active([
                                'roles.*',
                                'permissions.*',
                                'attach-permission/*'
                            ]) }}">
                            <a href="#"><i class="fa fa-lock"></i> Access Control <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level collapse">
                                <li class="{{ active(['roles.*', 'attach-permission/*']) }}"><a href="{{ route('roles.index') }}">Roles</a></li>
                                <li class="{{ active(['permissions.*']) }}"><a href="{{ route('permissions.index') }}">Permissions</a></li>
                            </ul>
                        </li>
                    @endrole
                </ul>
            </li>
        </ul>
    </div>
</nav>