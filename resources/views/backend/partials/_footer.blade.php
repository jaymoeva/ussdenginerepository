<div class="footer">
	<div class="pull-right">
		Powered By <strong>Edgetech Consults</strong>
	</div>
	<div>
		<strong>Copyright</strong> {{ config('app.name') }}&copy; {{ date('Y') }}
	</div>
</div>