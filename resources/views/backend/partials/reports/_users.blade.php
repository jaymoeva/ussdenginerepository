<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Users report</h5>
                <div class="ibox-tools">
                    <a href="#" class="btn btn-danger btn-xs" style="color: white;">Pdf <i class="fa fa-file-pdf-o"></i></a>
                    <a href="#" class="btn btn-primary btn-xs" style="color: white;">Excel <i class="fa fa-file-excel-o"></i></a>
                </div>
			</div>
			<div class="ibox-content">
				<div class="table-responsive">
					<div class="results-wrapper">
						<table class="table table-striped table-bordered table-hover users-table">
							<thead>
						        <tr>
					                <th>Name</th>
					                <th>Role</th>
					                <th>Email</th>
					                <th>Verified</th>
					                <th>Active</th>
					                <th>Date of Joining</th>
						        </tr>
							</thead>
							<tbody>
								@if(count($users) > 0)
									@foreach($users as $user)
										<tr>
											<td>{{ $user->name }}</td>
											<td>
												@if(count($user->roles) < 1)
													<span>No role assigned</span>
												@else
													@foreach($user->roles as $role)
														<span class="label label-primary">{{ $role->name }}</span>
													@endforeach
												@endif
											</td>
											<td>{{ $user->email }}</td>
											<td>
												@if($user->verified)
													<span class="label label-primary">Yes</span>
												@else
													<span class="label label-danger">No</span>
												@endif
											</td>
											<td>
												@if($user->active)
													<span class="label label-primary">Yes</span>
												@else
													<span class="label label-danger">No</span>
												@endif
											</td>
											<td>{{ date('M j, Y', strtotime($user->created_at)) . ' at ' . date('H:i', strtotime($user->created_at)) }}</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td class="alert alert-warning text-center" colspan="6">No users are available</td>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
					@if(count($users) > 0)
                        <div class="pagination-wrapper">
                            <span style="line-height: 60px;">Showing {!! $users->firstItem() !!} to {!! $users->lastItem() !!} of {{ $users->total() }} users</span>
                            <span class="pull-right">{!! $users->links(); !!}</span>
                        </div>
                    @endif
				</div>
			</div>
		</div>
	</div>
</div>