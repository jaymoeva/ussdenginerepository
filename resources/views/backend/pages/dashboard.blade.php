@extends('backend/layouts/template')

@section('title')
	Dashboard
@endsection

@section('content')
	@role('admin')
        @include('backend/pages/partials/_admin')
    @endrole

    @role('developer')
		@include('backend/pages/partials/_developer')
    @endrole

    @role('executive')
		@include('backend/pages/partials/_executive')
    @endrole

    @role('clerk')
		@include('backend/pages/partials/_clerk')
    @endrole
@endsection

@push('scripts')
    <script src="{{ asset('plugins/chartJs/Chart.min.js') }}"></script>
    <script src="{{ asset('plugins/demo/chartjs-demo.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>
@endpush