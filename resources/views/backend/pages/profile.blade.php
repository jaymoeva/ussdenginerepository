@extends('backend/layouts/template')

@section('title')
	Profile
@endsection

@push('styles')
    <link href="{{ asset('plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>My Profile</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Profile</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Profile Detail</h5>
                </div>
                <div>
                    <div class="ibox-content border-left-right text-center">
                        <img alt="image" class="img-responsive" src="{{ asset('img/avatars/' . $user->avatar) }}" style="width: 70px; height: 70px; display: inline-block;">
                    </div>
                    <div class="ibox-content profile-content">
                        <h4><strong>{{ $user->name }}</strong></h4>
                        <p><i class="fa fa-envelope"></i> {{ $user->email }}</p>
                        <p><i class="fa fa-calendar"></i> You joined on {{ date('M j, Y', strtotime($user->created_at)) . ' at ' . date('H:i', strtotime($user->created_at)) }}</p>
                        <h5>
                            Roles and Permissions
                        </h5>
                        <p>
                            @if($user->roles()->count() > 0)
                                <p>
                                   @foreach($user->roles as $r)
                                     <span class="label label-primary">{{ $r->name }}</span>
                                    @endforeach 
                                </p>
                                <p>
                                    @foreach($user->roles as $role)
                                        @foreach($role->permissions as $userperm)
                                            <span class="label label-default" style="padding: 3px; display: inline-block;">{{ $userperm->name }}</span>
                                        @endforeach
                                    @endforeach
                                </p>
                            @else
                                <span class="label label-warning">No roles and permissions assigned</span>
                            @endif
                        </p>
                        <div class="row m-t-lg">
                            
                        </div>
                        <div class="user-button">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="#avatar" class="btn btn-primary btn-sm btn-block" data-toggle="modal"><i class="fa fa-user"></i>Change Avatar</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="#details" class="btn btn-default btn-sm btn-block" data-toggle="modal"><i class="fa fa-pencil"></i>Edit profile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Activity Feed</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <div class="feed-activity-list">
                            <div class="feed-element">
                                <span class="pull-left">
                                    <i class="fa fa-user fa-2x"></i>
                                </span>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">1m ago</small>
                                     Your account was approved <br>
                                    <small class="text-muted">Today 4:30 pm</small>
                                    <div class="actions">
                                        <a class="btn btn-xs btn-danger" href="#"><i class="fa fa-envelope"></i> Message admin</a>
                                    </div>
                                </div>
                            </div>
                            <div class="feed-element">
                                <span class="pull-left">
                                    <i class="fa fa-lock fa-2x"></i>
                                </span>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">2 days ago</small>
                                     Your roles details have changed <br>
                                    <small class="text-muted">Monday 1:21 pm</small>
                                    <div class="actions">
                                        <a class="btn btn-xs btn-primary" href="#"><i class="fa fa-book"></i> Read why</a>
                                    </div>
                                </div>
                            </div>
                            <div class="feed-element">
                                <span class="pull-left">
                                    <i class="fa fa-laptop fa-2x"></i>
                                </span>
                                <div class="media-body ">
                                    <small class="pull-right">2h ago</small>
                                    <strong>Session ID EUSE546Y</strong> has just expired <br>
                                    <small class="text-muted">Today 2:10 pm - 12.06.2014</small>
                                    <div class="well">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                        Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-xs btn-white"><i class="fa fa-laptop"></i> What does this mean?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block m"><i class="fa fa-arrow-down"></i> Show More</button>
                </div>
            </div>
        </div>
    </div>
    @include('backend/partials/_modal')
@endsection

@push('scripts')
    <!-- Jasny -->
    <script src="{{ asset('plugins/jasny/jasny-bootstrap.min.js') }}"></script>
@endpush