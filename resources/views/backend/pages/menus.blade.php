@extends('backend/layouts/template')

@section('title')
	Menus
@endsection

@push('custom-styles')
	<link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menus</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Menus</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
	<div class="row">
		<!-- Menu initiator -->
		<div class="col-lg-4">
            <div class="ibox">
            	<div class="ibox-title">
            		<h5>Create Project</h5>
            	</div>
            	<div class="ibox-content">
            		<form action="">
	                	<div class="form-group">
	                		<label for="">Project Name</label>
	                		<input type="text" class="form-control">
	                	</div>

	                	<div class="form-group">
	                		<label for="">Project Type</label>
                            <select class="form-control m-b" required name="account">
                                    <option value="">Please Select Type</option>
                                    <option value="1">USSD</option>
                                    <option value="2">SMS</option>
                                </select>

	                	</div>
                        <div class="form-group">
                            <label for="">Project Description</label>
                            <input type="text-area" class="form-control">
                        </div>
	                </form>
                	<a href="" class="btn btn-primary btn-sm btn-block">Add</a>
            	</div>
            </div>
        </div>
		<!--// End Menu Initiator //-->
		<!-- Wizard -->
		<div class="col-lg-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>CREATE USSD MENUS </h5>
				</div>
				<div class="ibox-content">
					<h2>USSD MENUS </h2>
                    <p>Create the Menus as by the <a>documentation</a></p>

					<form action="" method="POST" id="form" class="wizard-big">
						<h1>LEVEL 1</h1>
						<fieldset>
							<h5>Enter the main MENUS</h5>
							<div class="row">
                                <div class="col-lg-8">
                                    <input type="hidden" name="count" value="1" />
                                    <div id="menus" class="input-group"><input type="text" id="menus1"  required placeholder="Enter the menu" class="form-control required" >
                                        <button id="b1" class="btn add-more" type="button">+</button>
                                    </div>
                                    <br>
                                    <small>Press + to add another form field :)</small>
                                    <!--div-- id="newmenu"class="input-group"><input type="text" id="menu"  required placeholder="Enter the menu" class="form-control required" >
                                        <div class="input-group-btn">
                                            <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">Action <span class="caret"></span></button>
                                            <ul class="dropdown-menu pull-right">
                                                <li id ="addnew">Add Menu</li>
                                                <li id="removemenu">Remove Menu</li>
                                            </ul>
                                        </div>
                                    </div-->
                                    <div id="newmenu1">

                                    </div>
                                </div>
                            </div>
						</fieldset>
						<h1>Field 2</h1>
                        <fieldset>
                            <h2>Header Here</h2>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>First name *</label>
                                        <input id="name" name="name" type="text" class="form-control required">
                                    </div>
                                    <div class="form-group">
                                        <label>Last name *</label>
                                        <input id="surname" name="surname" type="text" class="form-control required">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <input id="email" name="email" type="text" class="form-control required email">
                                    </div>
                                    <div class="form-group">
                                        <label>Address *</label>
                                        <input id="address" name="address" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <h1>Field 3</h1>
                        <fieldset>
                            <h2>Header Here</h2>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>First name *</label>
                                        <input id="name" name="name" type="text" class="form-control required">
                                    </div>
                                    <div class="form-group">
                                        <label>Last name *</label>
                                        <input id="surname" name="surname" type="text" class="form-control required">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <input id="email" name="email" type="text" class="form-control required email">
                                    </div>
                                    <div class="form-group">
                                        <label>Address *</label>
                                        <input id="address" name="address" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <h1>Field 4</h1>
                        <fieldset>
                            <h2>Header Here</h2>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>First name *</label>
                                        <input id="name" name="name" type="text" class="form-control required">
                                    </div>
                                    <div class="form-group">
                                        <label>Last name *</label>
                                        <input id="surname" name="surname" type="text" class="form-control required">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <input id="email" name="email" type="text" class="form-control required email">
                                    </div>
                                    <div class="form-group">
                                        <label>Address *</label>
                                        <input id="address" name="address" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
					</form>
				</div>
			</div>
		</div>
		<!-- // End Wizard // -->
	</div>
@endsection