@if(!empty($myCompany))
    <div class="row">
    	<div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">Monthly</span>
                        <h5>SMS</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">0</h1>
                        <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                        <small>Total Sms</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Annual</span>
                        <h5>Users</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            @if(!empty($myCompany))
                                {{ count($users) }}
                            @else
                                0
                            @endif
                        </h1>
                        <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                        <small>Total Users</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Today</span>
                        <h5>Events</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">0</h1>
                        <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                        <small>Total Events</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">All Time</span>
                        <h5>Trivia</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">0</h1>
                        <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                        <small>Total Trivia</small>
                    </div>
                </div>
            </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>About your company</h5>
                            </div>
                            <div class="ibox-content">
                                <div>
                                    <div class="feed-activity-list">
                                        <div class="feed-element">
                                            <i class="fa fa-briefcase"></i> Full Name
                                            <span class="pull-right">{{ $myCompany->name }}</span>
                                        </div>
                                        <div class="feed-element">
                                            <i class="fa fa-calendar"></i> Added on
                                            <span class="pull-right">
                                                {{ date('M j, Y', strtotime($myCompany->created_at)) . ' at ' . date('H:i', strtotime($myCompany->created_at)) }}
                                            </span>
                                        </div>
                                        <div class="feed-element">
                                            <i class="fa fa-phone"></i> Contact details <br> <br>
                                            <i class="fa fa-mobile"></i><span> {{ $myCompany->telephone }}</span>
                                            <span class="pull-right">
                                                <i class="fa fa-globe"></i> 
                                                {{ $myCompany->address }}
                                            </span>
                                        </div>
                                        <div class="feed-element">
                                            <i class="fa fa-users"></i> Users <span class="pull-right">{{ count($users) }}</span> <br><br>
                                            <a href="{{ route('users.create') }}" class="btn btn-primary btn-block btn-xs">add new user</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                @if(count($latest_users) > 0)
                                    <a href="{{ route('users.index') }}" class="label label-primary pull-right">view all</a>
                                @endif
                                <h5>Latest users in this company</h5>
                            </div>
                            <div class="ibox-content">
                               <div class="table-responsive">
                                   <table class="table table-responsive table-hover table-bordered table-striped">
                                       <thead>
                                           <tr>
                                               <th>Name</th>
                                               <th>Email</th>
                                               <th>Active</th>
                                               <th>Action</th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                           @if(count($latest_users) > 0)
                                                @foreach($latest_users as $user)
                                                    <tr>
                                                        <td>{{ $user->name }}</td>
                                                        <td>{{ $user->email }}</td>
                                                        <td>
                                                            @if($user->active)
                                                                <span class="label label-primary">active</span>
                                                            @else
                                                                <span class="label label-danger">inacive</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('users.show', $user->id) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-xs btn-success" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                                            {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE', 'style' => 'display: inline-block']) !!}                                
                                                               {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                                                            {!! Form::close() !!} &nbsp;&nbsp;
                                                            @if($user->active)
                                                                {!! Form::open(['route' => ['user.deactivate', $user->id], 'method' => 'POST', 'style' => 'display: inline-block']) !!}                                
                                                                   {{Form::button('<i class="fa fa-ban"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-warning', 'title' => 'Disable'))}}
                                                                {!! Form::close() !!} &nbsp;&nbsp;
                                                            @else
                                                                {!! Form::open(['route' => ['user.activate', $user->id], 'method' => 'POST', 'style' => 'display: inline-block']) !!}                                
                                                                   {{Form::button('<i class="fa fa-check"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-primary', 'title' => 'Activate'))}}
                                                                {!! Form::close() !!} &nbsp;&nbsp;
                                                            @endif
                                                            <a href="{{ route('user.role', $user->id) }}" class="btn btn-xs btn-info" title="Change Role"><i class="fa fa-lock" aria-hidden="true"></i></a> &nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                @endforeach
                                           @else
                                                <tr>
                                                    <td colspan="4" class="alert alert-warning text-center">Your company has no users</td>
                                                </tr>
                                           @endif
                                       </tbody>
                                   </table>
                               </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <!-- Notify user that he or she needs to a company -->
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning text-center">
                <a href="{{ route('companies.create') }}" style="color: black;">Complete your account details by adding your company name and address</a>
            </div>
        </div>
    </div>
    <!-- // End Notify // -->

    <div class="row">
        <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">Monthly</span>
                        <h5>SMS</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">0</h1>
                        <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                        <small>Total Sms</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Annual</span>
                        <h5>Users</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            @if(!empty($myCompany))
                                {{ count($users) }}
                            @else
                                0
                            @endif
                        </h1>
                        <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                        <small>Total Users</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Today</span>
                        <h5>Events</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">0</h1>
                        <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                        <small>Total Events</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">All Time</span>
                        <h5>Trivia</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">0</h1>
                        <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                        <small>Total Trivia</small>
                    </div>
                </div>
            </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <a href="{{ route('companies.create') }}" class="label label-info pull-right">Add Company</a>
                                <h5>About your company</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="alert alert-warning text-center">Add company to view.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Latest users in this company</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="alert alert-warning text-center">You have no users. Add a company to start creating users</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif


