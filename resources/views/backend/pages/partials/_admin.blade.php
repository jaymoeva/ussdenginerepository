<!-- Info Boxes -->
<div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">Monthly</span>
                    <h5>SMS</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">1200</h1>
                    <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                    <small>Total Sms</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Annual</span>
                    <h5>Users</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{ count($users) }}</h1>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                    <small>Total Users</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">Today</span>
                    <h5>Events</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">120</h1>
                    <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                    <small>Total Events</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-danger pull-right">All Time</span>
                    <h5>Trivia</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">12</h1>
                    <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                    <small>Total Trivia</small>
                </div>
            </div>
        </div>
</div>
<!--// End Info Boxes //-->

<!-- Graphs and Daily Feed Section -->
<div class="row">
	<div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Sessions Summary</span>
                <h5>USSD Sessions</h5>
            </div>
            <div class="ibox-content">
               <div>
                    <canvas id="barChart" height="140"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">SMS Traffic Summary</span>
                <h5>SMS Traffic</h5>
            </div>
            <div class="ibox-content">
                <div>
                    <canvas id="lineChart" height="140"></canvas>
                </div>
            </div>
        </div>
    </div>

	
</div>
<!--// End Graphs and Daily Feed Section //-->

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <a href="{{ route('companies.index') }}" class="btn btn-xs btn-primary pull-right">View All</a>
                <h5>Latest companies</h5>
            </div>
            <div class="ibox-content">
               <div class="table-responsive">
                   <table class="table table-striped table-bordered table-hover companies-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Active</th>
                                <th>Owned by</th>
                                <th>No. of users</th>
                                <th>Created on</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($latest_companies) > 0)
                                @foreach($latest_companies as $company)
                                    <tr>
                                        <td>{{ $company->name }}</td>
                                        <td>
                                            @if($company->status)
                                                <span class="label label-primary">active</span>
                                            @else
                                                <span class="label label-danger">inactive</span>
                                            @endif
                                        </td>
                                        <td>{{ $owner->name }}</td>
                                        <td>{{ count($company_users) }}</td>
                                        <td>{{ date('M j, Y', strtotime($company->created_at)) . ' at ' . date('H:i', strtotime($company->created_at)) }}</td>
                                        <td>
                                            <a href="{{ route('companies.show', $company->id) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="alert alert-warning text-center">No companies are available</td>
                                </tr>
                            @endif
                        </tbody>
                        

                   </table>
               </div>
            </div>
        </div>
    </div>
    
</div>
