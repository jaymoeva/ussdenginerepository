@if (Session::has('update-user'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('update-user') }}
	</div>
@elseif (Session::has('change-role'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('change-role') }}
	</div>
@elseif (Session::has('create-user'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('create-user') }}
	</div>
@elseif (Session::has('delete-user'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('delete-user') }}
	</div>
@elseif (Session::has('deactivate-user'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('deactivate-user') }}
	</div>
@elseif (Session::has('activate-user'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('activate-user') }}
	</div>
@elseif (Session::has('new-role'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('new-role') }}
	</div>
@elseif (Session::has('update-role'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('update-role') }}
	</div>
@elseif (Session::has('delete-role'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('delete-role') }}
	</div>
@elseif (Session::has('new-permission'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('new-permission') }}
	</div>
@elseif (Session::has('update-permission'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('update-permission') }}
	</div>
@elseif (Session::has('delete-permission'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('delete-permission') }}
	</div>
@elseif (Session::has('role-permission'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('role-permission') }}
	</div>
@elseif (Session::has('avatar-change'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('avatar-change') }}
	</div>
@elseif (Session::has('profile-updated'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('profile-updated') }}
	</div>
@elseif (Session::has('company-added'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('company-added') }}
	</div>
@elseif (Session::has('update-company'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('update-company') }}
	</div>
@elseif (Session::has('company-activated'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('company-activated') }}
	</div>
@elseif (Session::has('company-deactivated'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('company-deactivated') }}
	</div>
@elseif (Session::has('not-permitted'))
	<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('not-permitted') }}
	</div>
@elseif (Session::has('unapproved'))
	<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('unapproved') }}
	</div>
@elseif (Session::has('company-first'))
	<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('company-first') }}
	</div>
@elseif (Session::has('company-exists'))
	<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('company-exists') }}
	</div>
@elseif (Session::has('registration-complete'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('registration-complete') }}
	</div>
@elseif (Session::has('complete-registration'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('complete-registration') }}
	</div>
@elseif (Session::has('success'))
	<div class="alert alert-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('success') }}
	</div>
@endif