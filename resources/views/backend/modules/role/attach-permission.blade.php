@extends('backend/layouts/template')

@section('title')
	Attach Permission
@endsection

@push('dashboard-styles')
    <link href="{{ asset('css/iCheck/custom.css') }}" rel="stylesheet">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Attach Permission(s)</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Home</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">Access Control</a>
                </li>
                <li>
                    <a href="{{ route('roles.index') }}">Roles</a>
                </li>
                <li class="active">
                    <strong>Attach new permission(s) to {{ $role->name }} role</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-float-e-margins">
                <div class="ibox-title">
                     <h5>{{ $role->name }} role permission details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12 b-r">
                            <form action="{{ route('permission.change', $role->id) }}" method="POST" >

                                {{ csrf_field() }}

                                @if(count($permissions) > 0) 
                                    <div class="form-group">
                                        <label for="roles">Permissions</label>
                                            @foreach($permissions->chunk(3) as $chunk)
                                                <div class="row checkbox i-checks">
                                                    @foreach($chunk as $permission)
                                                        <div class="col-xs-4">
                                                           <input type="checkbox" {{ in_array($permission->id,$role_permissions)?"checked":"" }} name="permissions[]" value="{{ $permission->id }}"> {{ $permission->name }} <br> 
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach    
                                    </div>
                                    <div>
                                        <!-- <button type="submit" class="btn btn-sm btn-block btn-primary pull-right">Update</button> -->
                                        <button type="submit" class="btn btn-primary">Attach</button>
                                    </div>
                                @else
                                    <div class="alert alert-warning text-center"> This operation can't be performed as there are no permissions  available.</div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <br>
@endsection

@push('dashboard-scripts')
    <!-- iCheck -->
    <script src="{{ asset('js/iCheck/icheck.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });
        });
    </script>
@endpush