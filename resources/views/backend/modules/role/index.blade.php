@extends('backend/layouts/template')

@section('title')
	All Roles
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>All Roles</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">Access Control</a>
                </li>
                <li class="active">
                    <strong>Roles</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-float e-margins">
                <div class="ibox-title">
                    <h5>All roles in {{ config('app.name') }}</h5>
                    <div class="ibox-tools"> 
                        <a href="{{ route('roles.create') }}" class="btn btn-primary btn-sm">New Role</a>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover users-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Users</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($roles) > 0)
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->display_name }}</td>
                                        <td>{{ substr(strip_tags($role->description), 0, 40) }}{{ strlen(strip_tags($role->description)) > 40 ? "..." : "" }}</td>
                                        <td>{{ count($role->users) }}</td>
                                        <td>
                                            {{ date('M j, Y', strtotime($role->created_at)) . ' at ' . date('H:i', strtotime($role->created_at)) }}
                                        </td>
                                        <td>
                                            <a href="{{ route('roles.show', $role->id) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                            <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-xs btn-success" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                            {!! Form::open(['route' => ['roles.destroy', $role->id], 'method' => 'DELETE', 'style' => 'display: inline-block']) !!}                                
                                               {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                                            {!! Form::close() !!} &nbsp;&nbsp;
                                            <a href="{{ route('role.permission', $role->id) }}" class="btn btn-xs btn-info" title="Attach Permission"><i class="fa fa-unlock-alt" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="alert alert-warning text-center">No roles are available at the moment</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    @if(count($roles) > 0)
                        <div>
                            <span style="line-height: 60px;">Showing {!! $roles->firstItem() !!} to {!! $roles->lastItem() !!} of {{ $roles->total() }} roles</span>
                            <span class="pull-right">{!! $roles->links(); !!}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection