@extends('backend/layouts/template')

@section('title')
	View Role
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>View Role</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">Access Control</a>
                </li>
                <li>
                    <a href="{{ route('roles.index') }}">Roles</a>
                </li>
                <li>
                    <strong>View {{ $role->name }}</strong>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>About {{ ucfirst($role->name) }} role</h5>
                </div>
                <div class="ibox-content profile-content">
                    <h5>
                        <i class="fa fa-lock" style="color: #1BB394"></i> Permissions
                    </h5>
                    <p>
                       @if(count($role->permissions) > 0)
							@foreach($role->permissions as $permission)
	                           <span class="label label-default" style="display: inline-block; margin-bottom: 3px;">{{ $permission->name }}</span>
	                       @endforeach
                       @else
							<span class="">No permissions assigned</span>
                       @endif
                    </p>
                    <div class="row m-t-lg">
                        <div class="col-md-4">
                            <i class="fa fa-user" style="color: #1BB394"></i>
                            <h5><strong>{{ count($role->users) }}</strong> users have this role</h5>
                        </div>
                        <div class="col-md-4">
                            <i class="fa fa-calendar" style="color: #1BB394"></i>
                            <h5>Created on <strong>{{ date('M j, Y', strtotime($role->created_at)) }}</strong></h5>
                        </div>
                        <div class="col-md-4">
                            <i class="fa fa-pencil" style="color: #1BB394"></i>
                            <h5>Last Updated <strong>{{ date('M j, Y', strtotime($role->updated_at)) }}</strong></h5>
                        </div>
                    </div>
                    <div class="user-button">
                        <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-pencil"></i> Edit Role</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Users with this role</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Email Verified</th>
                                    <th>Active</th>
                                    <th>View User</th>
                                </tr>
                            </thead>
                            @if(count($users) > 0)
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
												@if($user->verified)
													<span class="label label-primary">Yes</span>
												@else
													<span class="label label-danger">No</span>
												@endif
											</td>
											<td>
												@if($user->active)
													<span class="label label-primary">Active</span>
												@else
													<span class="label label-danger">Not Active</span>
												@endif
											</td>
                                            <td class="text-center"><a href="{{ route('users.show', $user->id) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            @else
                                <tbody>
                                    <tr>
                                        <td colspan="5" class="alert alert-warning text-center">No users assigned to this role</td>
                                    </tr>
                                </tbody>
                            @endif
                        </table>
                        @if(count($users) > 0)
                            <div>
                                <span style="line-height: 60px;">Showing {!! $users->firstItem() !!} to {!! $users->lastItem() !!} of {{ $users->total() }} users</span>
                                <span class="pull-right">{!! $users->links(); !!}</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>  
	</div>
@endsection