@extends('backend/layouts/template')

@section('title')
	Edit Role
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Edit Role</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">Access Control</a>
                </li>
                <li>
                    <a href="{{ route('roles.index') }}">Roles</a>
                </li>
                <li>
                    <strong>Edit {{ $role->name }} role</strong>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="ibox-float-e-margins">
                <div class="ibox-title">
                    <h5>{{ $role->name }} role details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12 b-r">
                            <form action="{{ route('roles.update', $role->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label> 
                                    <input type="text" placeholder="" id="name" name="name" class="form-control" value="{{ $role->name }}">
                                    <!-- If name has error -->
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                                    <label>Display Name</label> 
                                    <input type="text" placeholder="" id="display_name" name="display_name" class="form-control" value="{{ $role->display_name }}">
                                    <!-- If display name has error -->
                                    @if ($errors->has('display_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('display_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label>Description</label> 
                                    <input type="description" placeholder="" id="description" name="description" class="form-control" value="{{ $role->description }}">
                                    <!-- If description has error -->
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="roles">Permissions</label>
                                    <div class="row checkbox i-checks">
                                        @if(count($permissions) > 0)
                                            @foreach($permissions->chunk(3) as $chunk)
                                                @foreach($chunk as $permission)
                                                    <div class="col-xs-4">
                                                        <input type="checkbox" {{ in_array($permission->id,$role_permissions)?"checked":"" }} name="permissions[]" value="{{ $permission->id }}"> {{ $permission->name }}
                                                    </div>
                                                @endforeach
                                            @endforeach
                                        @else
                                            <div class="col-xs-12">
                                                <div class="alert alert-warning">
                                                    No permissions have been created yet.
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
@endsection