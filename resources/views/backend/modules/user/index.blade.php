@extends('backend/layouts/template')

@section('title')
	All Users
@endsection

@section('page-header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>All Users</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">User Management</a>
                </li>
                <li class="active">
                    <strong>All</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox-float e-margins">
				<div class="ibox-title">
                    <h5>All users in {{ config('app.name') }}</h5>
                    <div class="ibox-tools"> 
	                    <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm">New User</a>
	                </div>
                </div>
			</div>

			<div class="ibox-content">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover users-table">
						@include('backend/modules/user/partials/_thead')
						<tbody>
							@if(count($users) > 0)
								@foreach($users as $user)
									<tr>
										<td>{{ $user->name }}</td>
										<td>
											@if(count($user->roles) < 1)
												<span>No role assigned</span>
											@else
												@foreach($user->roles as $r)
													<span class="label label-info">{{ $r->name }}</span>
												@endforeach
											@endif
										</td>
										<td>{{ $user->email }}</td>
										<td>
											@if($user->verified)
												<span class="label label-primary">Yes</span>
											@else
												<span class="label label-danger">No</span>
											@endif
										</td>
										<td>
											@if($user->active)
												<span class="label label-primary">Active</span>
											@else
												<span class="label label-danger">Not Active</span>
											@endif
										</td>
										<td>
											{{ date('M j, Y', strtotime($user->created_at)) . ' at ' . date('H:i', strtotime($user->created_at)) }}
										</td>
										<td>
                                            <a href="{{ route('users.show', $user->id) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-xs btn-success" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                            {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE', 'style' => 'display: inline-block']) !!}                                
                                               {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                                            {!! Form::close() !!} &nbsp;&nbsp;
                                            @if($user->active)
	                                            {!! Form::open(['route' => ['user.deactivate', $user->id], 'method' => 'POST', 'style' => 'display: inline-block']) !!}                                
	                                               {{Form::button('<i class="fa fa-ban"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-warning', 'title' => 'Disable'))}}
	                                            {!! Form::close() !!} &nbsp;&nbsp;
	                                        @else
												{!! Form::open(['route' => ['user.activate', $user->id], 'method' => 'POST', 'style' => 'display: inline-block']) !!}                                
	                                               {{Form::button('<i class="fa fa-check"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-primary', 'title' => 'Activate'))}}
	                                            {!! Form::close() !!} &nbsp;&nbsp;
	                                        @endif
                                            <a href="{{ route('user.role', $user->id) }}" class="btn btn-xs btn-info" title="Change Role"><i class="fa fa-lock" aria-hidden="true"></i></a> &nbsp;&nbsp;
                                        </td>
									</tr>
								@endforeach
							@else
								<tr>
									<td colspan="7" class="alert alert-warning text-center">
										@if(Auth::user()->hasRole('admin'))
											No users are available at the moment
										@else
											Your company has no users
										@endif
									</td>
								</tr>
							@endif
						</tbody>
					</table>
					@if(count($users) > 0)
                        <div>
                            <span style="line-height: 60px;">Showing {!! $users->firstItem() !!} to {!! $users->lastItem() !!} of {{ $users->total() }} users</span>
                            <span class="pull-right">{!! $users->links(); !!}</span>
                        </div>
                    @endif
				</div>
			</div>
		</div>
	</div>
@endsection