@extends('backend/layouts/template')

@section('title')
	Change Role
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Change Role</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Home</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">User Management</a>
                </li>
                <li>
                    <a href="{{ route('users.index') }}">All</a>
                </li>
                <li class="active">
                    <strong>Change {{ ucfirst($user->name) }}'s Role</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox-float-e-margins">
                <div class="ibox-title">
                     <h5>{{ ucfirst($user->name) }}'s role details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12 b-r">
                            <form action="{{ route('role.change', $user->id) }}" method="POST" >

                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="roles">Roles</label>
                                        @foreach($roles->chunk(3) as $chunk)
                                            <div class="row checkbox i-checks">
                                                @foreach($chunk as $role)
                                                    <div class="col-xs-4">
                                                       <input type="checkbox" {{ in_array($role->id,$user_roles)?"checked":"" }} name="roles[]" value="{{ $role->id }}"> {{ $role->name }} <br> 
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach    
                                </div>
                                <div>
                                    <!-- <button type="submit" class="btn btn-sm btn-block btn-primary pull-right">Update</button> -->
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <br>
@endsection