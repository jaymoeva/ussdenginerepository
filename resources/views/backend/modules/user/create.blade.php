@extends('backend/layouts/template')

@section('title')
	Create User
@endsection

@push('styles')
    <style>
        .field-icon {
          float: right;
          margin-left: -25px;
          margin-top: -25px;
          position: relative;
          z-index: 2;
        }
    </style>
@endpush

@section('page-header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Create User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">User Management</a>
                </li>
                <li>
                    <a href="{{ route('users.index') }}">All</a>
                </li>
                <li class="active">
                    <strong>New User</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="ibox-float-e-margins">
                <div class="ibox-title">
                    <h5>Create new user</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12 b-r">
                            <form action="{{ route('users.store') }}" method="POST" name="createForm">
                                {{ csrf_field() }}
                                <input type="hidden" name="length" value="10">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label> 
                                    <input type="text" placeholder="" id="name" name="name" class="form-control" value="{{ old('name') }}">
                                    <!-- If password has error -->
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email</label> 
                                    <input type="email" placeholder="" id="email" name="email" class="form-control" value="{{ old('email') }}">
                                    <!-- If password has error -->
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label> <a href="javascript:generate();" class="pull-right">Generate</a> 
                                    <input type="password" placeholder="" id="password-field" name="password" class="form-control">
                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                    <!-- If password has error -->
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="roles">Roles</label>
                                    <div class="row checkbox i-checks">
                                        @foreach($roles->chunk(3) as $chunk)
                                            @foreach($chunk as $role)
                                                <div class="col-xs-4">
                                                    <input type="checkbox" name="roles[]" value="{{ $role->id }}"> {{ $role->name }}
                                                </div>
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
@endsection

@push('scripts')
    <script>
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            createForm.password.value = randomPassword(createForm.length.value);
        }
    </script>
@endpush