@extends('backend/layouts/template')

@section('title')
	Edit User
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Edit User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">User Management</a>
                </li>
                <li>
                    <a href="{{ url('users') }}">All</a>
                </li>
                <li>
                    <strong>Edit {{ $user->name }}</strong>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			<div class="ibox-float-e-margins">
				<div class="ibox-title">
					<h5>{{ $user->name }}'s account details</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12 b-r">
							<form action="{{ route('users.update', $user->id) }}" method="POST">
								{{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label> 
                                    <input type="text" placeholder="" id="name" name="name" class="form-control" value="{{ $user->name }}">
                                    <!-- If password has error -->
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email</label> 
                                    <input type="email" placeholder="" id="email" name="email" class="form-control" value="{{ $user->email }}">
                                    <!-- If password has error -->
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label> 
                                    <input type="password" placeholder="" id="password" name="password" class="form-control">
                                    <!-- If password has error -->
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                	<label for="roles">Roles</label>
                                	<div class="row checkbox i-checks">
                                        @foreach($roles->chunk(3) as $chunk)
                                        	@foreach($chunk as $role)
												<div class="col-xs-4">
													<input type="checkbox" {{ in_array($role->id,$user_roles)?"checked":"" }} name="roles[]" value="{{ $role->id }}"> {{ $role->name }}
												</div>
                                        	@endforeach
                                        @endforeach
                                    </div>
                                </div>
                                <div>
                                	<button type="submit" class="btn btn-primary">Update</button>
                                </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <br>
@endsection