@extends('backend/layouts/template')

@section('title')
	View User
@endsection

@section('page-header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>View User</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">User Management</a>
                </li>
                <li>
                    <a href="{{ url('users') }}">All</a>
                </li>
                <li>
                    <strong>View {{ $user->name }}</strong>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-6">
            <div class="profile-image">
                <img src="{{ asset('img/avatars/' . $user->avatar) }}" class="img-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                            {{ $user->name }}
                        </h2>
                        <small>
                            {{ $user->name }} is allowed to  
                            @foreach($user->roles as $role)
                                @foreach($role->permissions as $permission)
                                    <span class="label label-default">{{ $permission->name }}</span>
                                @endforeach
                            @endforeach
                        </small>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="ibox">
                <div class="ibox-content">
                    <h3>About {{ $user->name }}</h3>
                    <p class="small">
                        <i class="fa fa-lock"></i>  
                        @foreach($user->roles as $role)
                            <span class="label label-primary">{{ $role->name }}</span> role.
                        @endforeach
                    </p>
                    <p class="small">
                        <i class="fa fa-envelope"></i> {{ $user->email }}
                    </p>
                    <p class="small">
                        <i class="fa fa-calendar"></i> Joined on {{ date('M j, Y', strtotime($user->created_at)) }}
                    </p>
                    <p class="small">
                        <i class="fa fa-clock-o"></i> At {{ date('H:i', strtotime($user->created_at)) }}
                    </p>
                    <p class="small">
                        <i class="fa fa-sign-out"></i> Verified 
                        @if($user->verified == true)
                            <span class="label label-primary">Yes</span>
                        @else
                            <span class="label label-danger">No</span>
                        @endif
                    </p>
                    <br><br><br>
                </div>
            </div>
        </div>
    </div>
@endsection