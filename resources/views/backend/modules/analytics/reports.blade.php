@extends('backend/layouts/template')

@section('title')
	Report Analytics
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/graph-nav.css') }}">
@endpush

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
  <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@endpush

@section('page-header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Reports</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Reports and Analytics</a>
                </li>
                <li class="active">
                    <strong>Reports</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <!--  Resource Toggler -->
    <div class="resource-type">
        <nav class="navbar navbar-default">
            <div class="tab">
                <ul class="nav navbar-nav">
                    <li><a onclick="openResource(event, 'users')" class="tablinks"><i class="fa fa-users" id="defaultOpen"></i> Users</a></li>
                    <li><a onclick="openResource(event, 'sessions')" class="tablinks"><i class="fa fa-laptop"></i> USSD Sessions </a></li>
                    <li><a onclick="openResource(event, 'sms')" class="tablinks"><i class="fa fa-envelope"></i> SMS Traffic</a></li>
                    <li><a onclick="openResource(event, 'trivia')" class="tablinks"><i class="fa fa-book"></i> Trivia</a></li>
                    <li><a onclick="openResource(event, 'events')" class="tablinks"><i class="fa fa-calendar-o"></i> Events</a></li>
                </ul>
            </div><!-- /.container-fluid -->
        </nav>
    </div>
    <!-- // End Resource Toggler // -->

    <div class="chart-data">
        <div id="users" class="tabcontent">
            @include('backend/partials/reports/_users')
        </div>
        <div id="sessions" class="tabcontent">
            @include('backend/partials/reports/_sessions')
        </div>
        <div id="sms" class="tabcontent">
            @include('backend/partials/reports/_sms')
        </div>
        <div id="trivia" class="tabcontent">
            @include('backend/partials/reports/_trivia')
        </div>
        <div id="events" class="tabcontent">
            @include('backend/partials/reports/_events')
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function openResource(evt, resourceName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the button that opened the tab
            document.getElementById(resourceName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        
        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>
     <!-- Chosen -->
    <script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.chosen-select').chosen({width: "100%"});
        });
    </script>
        <!--
        <script>
            $(document).on('click', '.pagination-wrapper a', function(e){
                e.preventDefault();
                $('.results-wrapper').load($(this).attr('href') + ' .results-wrapper');
            });
        </script>
        -->
@endpush