@extends('backend/layouts/template')

@section('title')
	Manage Company
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Manage Company</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <a href="#">My Company</a>
                </li>
                <li>
                    <strong>Manage</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row m-b-xs m-t-xs">
        <div class="col-md-6">
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                            {{ $company->name }}
                        </h2>
                        <h4>Added on {{ date('M j, Y', strtotime($company->created_at)) . ' at ' . date('H:i', strtotime($company->created_at)) }}</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table class="table small m-b-xs">
                <tbody>
                <tr>
                    <td>
                        <strong>0</strong> Events
                    </td>
                    <td>
                        <strong>0</strong> Trivia
                    </td>

                </tr>
                <tr>
                    <td>
                        <strong>{{ count($users) }}</strong> Users
                    </td>
                    <td>
                        <strong>0</strong> Elections and Polls
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Your company details</h5>
                </div>
                <div class="ibox-content">
                    <table class="table table-stripped small m-t-lg">
                        <tbody>
                            <tr>
                                <td class="no-borders">
                                    <i class="fa fa-briefcase"></i> <strong>Name</strong>
                                </td>
                                <td  class="no-borders">
                                    {{ $company->name }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-user"></i> <strong>Users</strong>
                                </td>
                                <td>
                                    {{ count($users) }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-calendar"></i> <strong>Created On</strong>
                                </td>
                                <td>
                                   {{ date('M j, Y', strtotime($company->created_at)) . ' at ' . date('H:i', strtotime($company->created_at)) }} 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa fa-bookmark-o"></i> <strong>Status</strong>
                                </td>
                                <td>
                                    @if($company->status)
                                        <span class="label label-primary">Approved</span>
                                    @else
                                        <span class="label label-danger">Unapproved</span>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
           <div class="ibox">
               <div class="ibox-title">
                   <h5>Update your company</h5>
               </div>
               <div class="ibox-content">
                   <form action="{{ route('companies.update', $company->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>NAME</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="name" value="{{ $company->name }}" required />
                                        <span class="input-group-addon"><i class="fa fa-briefcase"></i></span>
                                    </div>
                                     @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->login->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                                    <label>TELEPHONE</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="telephone" placeholder="" value="{{ $company->telephone }}" required/>
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    </div>
                                     @if ($errors->login->has('telephone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->login->first('telephone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label>ADDRESS</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="address" value="{{ $company->address }}" required/>
                                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                    </div>
                                     @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-block" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
               </div>
           </div>
        </div>
    </div>
@endsection