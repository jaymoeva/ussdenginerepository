@extends('backend/layouts/template')

@section('title')
	View Company
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>View Company</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <strong>View {{ $company->name }}</strong>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>About {{ ucfirst($company->name) }}</h5>
                </div>
                <div class="ibox-content profile-content">
                    <div>
                        <div class="feed-activity-list">
                            <div class="feed-element">
                                <i class="fa fa-briefcase"></i> Full Name
                                <span class="pull-right">{{ $company->name }}</span>
                            </div>
                            <div class="feed-element">
                                <i class="fa fa-calendar"></i> Added on
                                <span class="pull-right">
                                    {{ date('M j, Y', strtotime($company->created_at)) . ' at ' . date('H:i', strtotime($company->created_at)) }}
                                </span>
                            </div>
                            <div class="feed-element">
                                <i class="fa fa-phone"></i> Contact details <br> <br>
                                <i class="fa fa-mobile"></i><span> {{ $company->telephone }}</span>
                                <span class="pull-right">
                                    <i class="fa fa-globe"></i> 
                                    {{ $company->address }}
                                </span>
                            </div>
                            <div class="feed-element">
                                <i class="fa fa-unlock-alt"></i> Company active ?
                                <span class="pull-right">
                                    @if($company->status)
                                        <span class="label label-primary">yes</span>
                                    @else
                                        <span class="label label-danger">no</span>
                                    @endif
                                </span>
                            </div>
                            <div class="user-button">
                                @if($company->status)
                                    <a href="{{ route('company.deactivate', $company->id) }}" class="btn btn-danger btn-sm btn-block">Deactivate</a>
                                @else
                                    <a href="{{ route('company.activate', $company->id) }}" class="btn btn-primary btn-sm btn-block">Activate</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Users belonging to this company</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Email Verified</th>
                                    <th>Active</th>
                                    <th>View User</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection