@extends('backend/layouts/template')

@section('title')
	All Companies
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>All Companies</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Companies</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover users-table">
                        @include('backend/modules/company/partials/_thead')
                        <tbody>
                            @if(count($companies) > 0)
                                @foreach($companies as $company)
                                    <tr>
                                        <td>{{ $company->name }}</td>
                                        <td>{{ $company->address }}</td>
                                        <td>{{ $company->telephone }}</td>
                                        <td>
                                            @if($company->status)
                                                <span class="label label-primary">Active</span>
                                            @else
                                                <span class="label label-danger">Inactive</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ date('M j, Y', strtotime($company->created_at)) . ' at ' . date('H:i', strtotime($company->created_at)) }}
                                        </td>
                                        <td>
                                            <a href="{{ route('companies.show', $company->id) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                            @if(Auth::user()->hasRole('admin'))
                                                @if($company->status)
                                                    {!! Form::open(['route' => ['company.deactivate', $company->id], 'method' => 'POST', 'style' => 'display: inline-block']) !!}                                
                                                       {{Form::button('<i class="fa fa-ban"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-warning', 'title' => 'Deactivate'))}}
                                                    {!! Form::close() !!} &nbsp;&nbsp;
                                                @else
                                                    {!! Form::open(['route' => ['company.activate', $company->id], 'method' => 'POST', 'style' => 'display: inline-block']) !!}                                
                                                       {{Form::button('<i class="fa fa-check"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-primary', 'title' => 'Activate'))}}
                                                    {!! Form::close() !!} &nbsp;&nbsp;
                                                @endif
                                            @else
                                                <a href="{{ route('companies.edit', $company->id) }}" class="btn btn-xs btn-success" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                                {!! Form::open(['route' => ['companies.destroy', $company->id], 'method' => 'DELETE', 'style' => 'display: inline-block']) !!}                                
                                                   {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                                                {!! Form::close() !!} &nbsp;&nbsp;
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="alert alert-warning text-center">
                                        @if(Auth::user()->hasRole('executive'))
                                            You have no companies
                                        @else
                                            There are no companies available
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    @if(count($companies) > 0)
                        <div>
                            <span style="line-height: 60px;">Showing {!! $companies->firstItem() !!} to {!! $companies->lastItem() !!} of {{ $companies->total() }} companies</span>
                            <span class="pull-right">{!! $companies->links(); !!}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection