@extends('backend/layouts/template')

@section('title')
	Edit Permission
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Edit Permission</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">Acces Control</a>
                </li>
                <li>
                    <a href="{{ route('permissions.index') }}">Permissions</a>
                </li>
                <li>
                    <strong>Edit {{ $permission->name }} permission</strong>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="ibox-float-e-margins">
                <div class="ibox-title">
                    <h5>{{ $permission->name }} details</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12 b-r">
                            <form action="{{ route('permissions.update', $permission->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label> 
                                    <input type="text" id="name" name="name" class="form-control" value="{{ $permission->name }}">
                                    <!-- If name has error -->
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                                    <label>Display Name</label> 
                                    <input type="text" id="display_name" name="display_name" class="form-control" value="{{ $permission->display_name }}">
                                    <!-- If display name has error -->
                                    @if ($errors->has('display_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('display_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label>Description</label> 
                                    <input type="description" id="description" name="description" class="form-control" value="{{ $permission->description }}">
                                    <!-- If description has error -->
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
@endsection