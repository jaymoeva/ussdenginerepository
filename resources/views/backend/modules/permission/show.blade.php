@extends('backend/layouts/template')

@section('title')
	View Permission
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>View Permission</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">Access Control</a>
                </li>
                <li>
                    <a href="{{ route('permissions.index') }}">Permissions</a>
                </li>
                <li>
                    <strong>View {{ $permission->name }} permission</strong>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>About {{ $permission->name }} permission</h5>
                </div>
                <div class="ibox-content profile-content">
                    <h5>Description</h5>
                    <p class="text-justify">{{ $permission->description }}</p>
                    <div class="row m-t-lg">
                        <div class="col-md-6">
                            <i class="fa fa-sticky-note" style="color: #1BB394"></i>
                            <h5>Full name <strong>{{ ucfirst($permission->display_name) }}</strong></h5>
                        </div>
                        <div class="col-md-6">
                            <i class="fa fa-calendar" style="color: #1BB394"></i>
                            <h5>Created on <strong>{{ date('M j, Y', strtotime($permission->created_at)) }}</strong></h5>
                        </div>
                    </div>
                    <div class="user-button">
                        <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-pencil"></i> Edit Permission</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Users with this permission</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Active</th>
                                    <th>View User</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @if(count($users) > 0)
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if($user->active)
                                                    <span class="label label-primary">Active</span>
                                                @else
                                                    <span class="label label-danger">Inactive</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('users.show', $user->id) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="alert alert-warning text-center" colspan="5">No users with this permission are available</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        @if(count($users) > 0)
                            <div>
                                <span style="line-height: 60px;">Showing {!! $users->firstItem() !!} to {!! $users->lastItem() !!} of {{ $users->total() }} users</span>
                                <span class="pull-right">{!! $users->links(); !!}</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>    
    </div>
@endsection