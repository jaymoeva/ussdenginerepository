@extends('backend/layouts/template')

@section('title')
	Create Permission
@endsection

@push('dashboard-styles')
    <link href="{{ asset('css/iCheck/custom.css') }}" rel="stylesheet">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Create Permission</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">Access Control</a>
                </li>
                <li>
                    <a href="{{ route('permissions.index') }}">Permissions</a>
                </li>
                <li>
                    <strong>Create permission</strong>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="ibox-float-e-margins">
                <div class="ibox-title">
                    <h5>Create new permission</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12 b-r">
                            <form action="{{ route('permissions.store') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label> 
                                    <input type="text" placeholder="eg create-users" id="name" name="name" class="form-control">
                                    <!-- If name has error -->
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                                    <label>Display Name</label> 
                                    <input type="text" placeholder="eg Create Users" id="display_name" name="display_name" class="form-control">
                                    <!-- If display name has error -->
                                    @if ($errors->has('display_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('display_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label>Description</label> 
                                    <input type="description" placeholder="eg Allows user to create other users" id="description" name="description" class="form-control">
                                    <!-- If description has error -->
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
@endsection