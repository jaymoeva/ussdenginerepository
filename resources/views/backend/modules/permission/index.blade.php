@extends('backend/layouts/template')

@section('title')
	All Permissions
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>All Permissions</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Settings</a>
                </li>
                <li>
                    <a href="#">Access Control</a>
                </li>
                <li class="active">
                    <strong>Permissions</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-float e-margins">
                <div class="ibox-title">
                    <h5>All permissions in {{ config('app.name') }}</h5>
                    <div class="ibox-tools"> 
                        <a href="{{ route('permissions.create') }}" class="btn btn-primary btn-sm">New Permission</a>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover users-table">
                        <thead>
                            <tr>
                                <th>Permission ID</th>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($permissions) > 0)
                                @foreach($permissions as $permission)
                                    <tr>
                                        <td>{{ $permission->id }}</td>
                                        <td>{{ $permission->name }}</td>
                                        <td>{{ $permission->display_name }}</td>
                                        <td>{{ $permission->description }}</td>
                                        <td>
                                            {{ date('M j, Y', strtotime($permission->created_at)) . ' at ' . date('H:i', strtotime($permission->created_at)) }}
                                        </td>
                                        <td>
                                            <a href="{{ route('permissions.show', $permission->id) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                            <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-xs btn-success" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                            {!! Form::open(['route' => ['permissions.destroy', $permission->id], 'method' => 'DELETE', 'style' => 'display: inline-block']) !!}                                
                                               {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                                            {!! Form::close() !!} &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="alert alert-warning text-center">No permissions are available at the moment</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    @if(count($permissions) > 0)
                        <div>
                            <span style="line-height: 60px;">Showing {!! $permissions->firstItem() !!} to {!! $permissions->lastItem() !!} of {{ $permissions->total() }} permissions</span>
                            <span class="pull-right">{!! $permissions->links(); !!}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection