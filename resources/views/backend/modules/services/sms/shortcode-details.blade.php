@extends('backend/layouts/template')

@section('title')
	Shortcode Details
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menus</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">SMS</a>
                </li>
                <li class="active">
                    <strong>Shortcode Details</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Application Detail</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        @foreach($projects as $project)

                        <p><strong>Application Name:</strong>{{$project->project_name}}</p>
                        @endforeach
                        <p><strong>Status:</strong>{{$project->name}}</p>
                        <p><strong>External App Url:</strong>{{$project->endpoint_external}}</p>
                            <p><strong>Type:</strong>{{$project->sms_typename}}</p>
                        <p><strong>Internal App Url:</strong>{{$project->endpoint_internal}}</p>
                        <p><strong>Created:</strong>{{$project->created_at}}</p>

                        
                        <div class="row m-t-lg text-center">
                           <div class="col-md-12">Create new sms app</div> 
                        </div><br>
                        <div class="user-button">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{ route('sms.static') }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-laptop"></i> Static</a>
                                </div>
                                <div class="col-md-6">  
                                    <a href="#" class="btn btn-default btn-sm btn-block"><i class="fa fa-bar-chart-o"></i> Dynamic</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Your app in depth</h5>
                </div>
                <div class="ibox-content">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#incoming"><i class="fa fa-download"></i> Incoming</a></li>
                        <li><a data-toggle="tab" onclick="getoutgoing({{$project->id}})" href="#outgoing"><i class="fa fa-upload"></i> Outgoing</a></li>
                        <li><a data-toggle="tab" onclick="getstatisticsms({{$project->id}})" href="#stats"><i class="fa fa-bar-chart-o"></i> Quick Stats</a></li>
						 <li><a data-toggle="tab"  href="#api"><i class="fa fa-bar-chart-o"></i> API DETAILS</a></li>
					</ul>
                    <div class="tab-content">
                        <div id="incoming" class="tab-pane active">
                            <br>
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="ibox-content">

                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Project Name</th>
                                                <th>Phone Number</th>
                                                <th>Message</th>
                                                <th>Unique ID</th>
                                                <th>Status</th>
                                                <th>Created_at</th>
                                            </tr>
                                            </thead>
                                            <tbody id="incomingtable">


                                            </tbody>
                                        </table>

                                </div>
                                </div>
                            </div>

                        </div>
                        <div id="outgoing" class="tab-pane">
                            <br>
                            <div class="timeline-item">
                                    <div class="row">
                                        <div class="ibox-content">

                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Project Name</th>
                                                    <th>Phone Number</th>
                                                    <th>Message</th>
                                                    <th>Unique ID</th>
                                                    <th>Status</th>
                                                    <th>Status Code</th>
                                                    <th>Created_at</th>
                                                </tr>
                                                </thead>
                                                <tbody id="outgoingsms">
                                                <tr>
                                                    <th>#</th><th>Project Name</th><th>Phone Number</th><th>Message</th><th>Unique ID</th><th>Status</th><th>status code</th></th><th>Created_at</th>
                                                </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                </div>
                            </div>

                        </div>
                        <div id="stats" class="tab-pane">
                            <br>
                            <div class="row">
                                <div>
                                    <canvas id="doughnutChart" height="140"></canvas>
                                </div>

                            </div>
                        </div>
						<div id="api" class="tab-pane">
                            <br>
                            <div class="row">
                                <div>
                                    @foreach($apidetails as $detail)

									<p><strong>API ENDPOINT:</strong></p><p>{{$detail->url_endpoint}}</p>
									
									<p><strong>API KEY:</strong></p><p>{{$detail->api_key}}</p>
											</div>
											@endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push("scripts")
    <script src="{{ asset('plugins/chartJs/Chart.min.js')}}"></script>
    <script src="{{ asset('js/smsstatic.js') }}"></script>
	
    @endpush
<script>function getoutgoing(id) {
    $.ajax({
        url: "/ussdengine/public/getoutgoing",
        type: 'GET',
        data: {id: id},
        dataType: 'json', // added data type
        success: function (res) {
            var markup = "";
            console.log(res);
            for (var i = 0; i <= res.length; i++) {
                console.log(res[i]);
                //   var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                //
                if (typeof res[i] === "undefined") {
                    console.log('here us undefined');
                } else {
                    var counter = i+1;
                    //         markup = markup + "<tr><td><input type='radio'  onclick='getdetails("+res[i]['project_id']+")' name='record' value="+ res[i]['project_id'] +"></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                    markup = markup + '<tr><td>'+ counter +'</td><td>'+res[i]['project_name']+'</td><td>'+res[i]['msisdn']+'</td><td>'+res[i]['message']+'</td><td>'+res[i]['uniqueid']+'</td><td>'+ res[i]['status'] +'</td><td>'+ res[i]['status_code'] +'</td><td>'+res[i]['created_at']+'</td></tr>';
                }
            }
            //  $("#sessions_table").empty().append(markup);
            // var active = finalresp[0]['project_id'];
            // getdetails(finalresp[0]['project_id']);
            $("#outgoingsms").empty().append(markup);
            //    alert(res);
            //  $("#applications").append(markup);

            // $("#app_name").empty().append(res[0]['project_name']);
            // $("#app_status").empty().append(res[0]['name']);
            //$("#app_created_at").empty().append(res[0]['created_at']);
            // $("#ext_url").empty().append(res[0]['endpoint_external']);
            // $("#int_url").empty().append(res[0]['endpoint_internal']);
        }
    });

}
function getstatisticsms(id){
    //
    console.log(id);
   // var current_id = active;
    $.ajax({
        url: "/ussdengine/public/getstatisticsms",
        type: 'GET',
        data: {id: id},
        dataType: 'json', // added data type
        success: function (stats) {
            console.log(stats);
            pie(stats);
            //    alert(res);
            //  $("#applications").append(markup);

//                $("#app_name").empty().append(res[0]['project_name']);
//                $("#app_status").empty().append(res[0]['name']);
//                $("#app_created_at").empty().append(res[0]['created_at']);
//                $("#ext_url").empty().append(res[0]['endpoint_external']);
//                $("#int_url").empty().append(res[0]['endpoint_internal']);
        }

    });
    //  alert('here');
}
function pie(stats){
    var doughnutData = {
        labels: ["Incoming","Outgoing" ],
        datasets: [{
            data: [stats['incoming'],stats['outgoing']],
            backgroundColor: ["#DCE126","#38E11D"]
        }]
    } ;


    var doughnutOptions = {
        responsive: true
    };


    var ctx4 = document.getElementById("doughnutChart").getContext("2d");
    new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
}
</script>
