@extends('backend/layouts/template')

@section('title')
	Static SMS
@endsection

@push('custom-styles')
	<link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menus</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">SMS</a>
                </li>
                <li class="active">
                    <strong>Static App</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <!-- New Staic App Form -->
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>New Application</h5>
                </div>
                <div class="ibox-content">
                    <form action="">
                        <div class="form-group">
                            <label for="">Appication Name</label>
                            <input type="text" placeholder="e.g. SMS Application" class="form-control" id="app-name">
                        </div>
                        <div class="form-group">
                            <label for="">External Application URL</label>
                            <input type="text" placeholder="e.g https://cloud.edgetech.co.ke" class="form-control" id="app-url">
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea id="app-description" class="form-control" placeholder="Short Project Description"></textarea>
                        </div>

                        <div class="form-group">
                            <select class="chosen-select" data-placeholder="SMS type" id="sms_type" name="sms_type" tabindex="2">
                                <option></option>
                                <option value="1">Bulk SMS</option>
                                <option value="2">M.O Shortcode</option>
                                <option value="3">Subscription ShortCode</option>
                            </select>
                        </div>
                    </form>
                    <a href="" class="btn btn-primary btn-sm btn-block addsms-application">Save</a>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Details</h5>
                </div>
                <div class="ibox-content">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#current"><i class="fa fa-clock-o"></i> Current Projects</a></li>
                        <!--li><a data-toggle="tab" href="#shortcode"><i class="fa fa-envelope"></i> Shortcode</a></li-->
                    </ul>
                    <div class="tab-content">
                        <div id="current" class="tab-pane active">
                            <div >
                                <ul id="current">

                                </ul>

                            </div>
                        </div>
                        <div id="shortcode" class="tab-pane">
                            <div>

                                <ul class="todo-list m-t small-list">
                                    <li>
                                        <a href="{{ route('details.shortcode') }}">
                                            <span class="m-l-xs">*12349</span>
                                        </a>
                                    </li>
                                    <li><a href="{{ route('details.shortcode') }}"><span class="m-l-xs">*544</span></a></li>
                                    <li>
                                        <a href="{{ route('details.shortcode') }}">
                                            <span class="m-l-xs">*188</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('details.shortcode') }}">
                                            <span class="m-l-xs">*79079</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// End New Static App Form //-->
    </div>
@endsection

@push('scripts')
    <!-- Chosen -->
    <script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.chosen-select').chosen({width: "100%"});

            // Dynamically add applications
            $(".addsms-application").click(function (e) {
                e.preventDefault();
                var appName = $("#app-name").val();
                var apptype = $("#sms_type").val();
                var appdesc = $("#app-description").val();
                var appUrl = $("#app-url").val();
                //   var appdesc = $("#app-description").val();
                var markup = '';
                console.log(appUrl);
                console.log(apptype);
                console.log(appName);
                $.ajax({
                    type: "POST",
					headers: {
			  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/ussdengine/public/createproject',
                    data: {appname: appName, appurl: appUrl, smstype: apptype, type: 2, appdesc: appdesc},
                    success: function (res) {
                        console.log(res);

                        for (var i = 0; i <= res.length; i++) {
                            console.log(res[i]);
                            //   if(res[i] !== "undefined") markup = markup + "<tr><td><input type='checkbox' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                            if (typeof res[i] === "undefined") {
                                console.log('here us undefined');
                            } else {
                                //    markup = markup + "<tr><td><input onclick='getdetails("+res[i]['project_id']+")' type='radio' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                                // markup = markup + "<li onclick=getdetails("+res[i]['project_id']+")> <span class='m-l-xs'>"+ res[i]['project_name'] +"</span></li>";
                                // markup = markup + '<li><a href=' onclick=getdetails("+res[i][\'project_id\']+")><span class="m-l-xs">'+res[i]["project_name"]+'</span></a></li>';
                                var project_id = "id="+res[i]['project_id'];
                                var project_iddetails = "{{route('details.shortcode','project')}}".replace("project", project_id);
                                console.log(project_iddetails);
                                markup = markup + '<li><a href=' + project_iddetails + '><span class="m-l-xs">' + res[i]["project_name"] + '</span></a></li>';
                            }
                        }
                        // console.log(markup);
                        $("#current").empty();
                        $("#current").append(markup);
                    }

                });
                console.log(markup);
                //var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + appName + "</td><td>" + appUrl + "</td></tr>";

            });
            function getoutgoing(id) {
                $.ajax({
                    url: "/ussdengine/public/getoutgoing",
                    type: 'GET',
                    data: {id: id},
                    dataType: 'json', // added data type
                    success: function (res) {
                        console.log(res);

                        //    alert(res);
                        //  $("#applications").append(markup);

                       // $("#app_name").empty().append(res[0]['project_name']);
                       // $("#app_status").empty().append(res[0]['name']);
                        //$("#app_created_at").empty().append(res[0]['created_at']);
                       // $("#ext_url").empty().append(res[0]['endpoint_external']);
                       // $("#int_url").empty().append(res[0]['endpoint_internal']);
                    }
                });

            }
            $.ajax({
                url: "/ussdengine/public/getprojects",
                type: 'GET',
                data: {type: 2},
                dataType: 'json', // added data type
                success: function (res) {
                    //  console.log(res);
                    //    alert(res);
                    var finalresp = res;
                    var markup = "";
                    for (var i = 0; i <= res.length; i++) {
                        console.log(res[i]);
                        //   var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                        //
                        if (typeof res[i] === "undefined") {
                            console.log('here us undefined');
                        } else {
                            var project_id = res[i]['project_id']; 
                            var project_iddetails = "{{route('details.shortcode','project')}}".replace("project", "id="+project_id);
                            markup = markup + '<li><a href=' + project_iddetails + '><span class="m-l-xs">' + res[i]["project_name"] + '</span></a></li>';
                        }
                    }
                    $("#current").empty();
                    $("#current").append(markup);
                    var active = finalresp[0]['project_id'];
                    //  getdetails(finalresp[0]['project_id']);
                    // $("#app_name").append("raymond here");
                    // $("#app_status").append("raymond here");
                    // $("#app_created_at").append("raymond here");
                    // $("#ext_url").append("raymond here");
                    // $("#int_url").append("raymond here");
                }
            });
        });

    </script>
@endpush