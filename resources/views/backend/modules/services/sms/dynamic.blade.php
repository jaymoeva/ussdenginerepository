@extends('backend/layouts/template')

@section('title')
	Dynamic App
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menus</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">SMS</a>
                </li>
                <li class="active">
                    <strong>Dynamic App</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="text-center">
                        <h4>Our dynamic app is found under games management. Click any of the links below.</h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <a href="/trivia" class="btn btn-primary">Trivia</a>
                        </div>
                        <div class="col-lg-3">
                            <a href="/polls" class="btn btn-primary">Elections and Polls</a>
                        </div>
                        <div class="col-lg-3">
                            <a href="/lifestyle" class="btn btn-primary">Lifestyle</a>
                        </div>
                        <div class="col-lg-3">
                            <a href="/lottery" class="btn btn-primary">Lotteries</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection