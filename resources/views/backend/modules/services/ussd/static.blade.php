@extends('backend/layouts/template')

@section('title')
	Static USSD
@endsection

@push('custom-styles')
	<link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menus</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">USSD</a>
                </li>
                <li class="active">
                    <strong>Static App</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
	<div class="row">
		<!-- Menu initiator -->
		<div class="col-lg-4">
            <div class="ibox">
            	<div class="ibox-title">
            		<h5>New Application</h5>
            	</div>
            	<div class="ibox-content">
            		<form action="">
	                	<div class="form-group">
	                		<label for="">Appication Name</label>
	                		<input type="text" placeholder="e.g. Voting USSD" class="form-control" id="app-name">
	                	</div>
                        <div class="form-group">
	                		<label for="">External Application URL</label>
	                		<input type="text" placeholder="e.g https://cloud.edgetech.co.ke" class="form-control" id="app-url">
	                	</div>
	                </form>
                	<a href="" class="btn btn-primary btn-sm btn-block add-application">Add</a>
            	</div>
                <div class="ibox-content i-checks">

                        <table id="testtable" class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Application</th>
                                <th>State</th>
                            </tr>
                            </thead>
                            <tbody id = "applications">
                            
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
		<!--// End Menu Initiator //-->
		<!-- Wizard -->
		<div class="col-lg-8">
			<div class="ibox">
				<div class="ibox-content">
					<ul class="nav nav-tabs">
                        <li class=""><a data-toggle="tab" onclick="ajaxcallsession()"  href="#tab-1"><i class="fa fa-laptop"></i> Session</a></li>
                        <li class=""><a data-toggle="tab" onclick="ajaxcallstats()"  href="#tab-2"><i class="fa fa-bar-chart-o"></i> Stats</a></li>
                        <li class="active"><a data-toggle="tab" href="#tab-3"><i class="fa fa-briefcase"></i> App Details</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane">
                            <div class="full-height-scroll">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover">

                                        <thead><tr><th>ussd</th><th> session</th><th>message</th> <th>response code</th><th>status</th><th>Created_at</th></tr></thead>
                                        <tbody id="sessions_table">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" value="" class="tab-pane">
                            <div class="row wrapper border-bottom white-bg page-heading">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>USSD SESSIONS STATISTICS </h5>

                                    </div>
                                    <div class="ibox-content">
                                        <div>
                                            <canvas id="doughnutChart" height="140"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane active">
                            <div class="ibox-content">
                                <div class="tab-content">
                                    <div id="contact-1" class="tab-pane active">
                                        <div class="client-detail">


                                                <strong>App Details</strong>

                                                <ul class="list-group clear-list">
                                                    <li class="list-group-item fist-item">
                                                        <span class="pull-right" id="app_name"></span>
                                                        Name
                                                    </li>
                                                    <li class="list-group-item">
                                                        <span class="pull-right" id="app_status"></span>
                                                        Status
                                                    </li>
                                                    <li class="list-group-item">
                                                        <span class="pull-right" id="app_created_at"> </span>
                                                        Created At
                                                    </li>
                                                    <li class="list-group-item">
                                                        <span class="pull-right" id="ext_url"></span>
                                                        External URL
                                                    </li>
                                                    <li class="list-group-item">
                                                        <span class="pull-right" id="int_url"></span>
                                                       App URL
                                                    </li>
                                                </ul>
                                                <strong>Further Description</strong>





                        </div>
                    </div>
                        </div>

				</div>
			</div>
		</div>
                </div>
            </div>
        </div>
    </div>
		<!-- // End Wizard // -->
@endsection

@push('graph-scripts')
    <!-- Flot -->
    <script src="{{ asset('plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('plugins/flot/jquery.flot.time.js') }}"></script>
@endpush

@push('demo')
    <!-- Flot demo data -->
    <script src="{{ asset('plugins/demo/flot-demo.js') }}"></script>
    <!-- ChartJS-->



    <script src="{{ asset('plugins/chartJs/Chart.min.js')}}"></script>
    <script src="{{ asset('plugins/demo/chartjs-demo.js')}}"></script>
@endpush
@push('scripts')
    <script src="{{ asset('js/ussdengine.js') }}"></script>
    @endpush
@push('scripts')
	<script>
    var active = '';
    function ajaxcallsession(){
        //
        //alert('here');
     //   active = id;
        $.ajax({
            url: "/ussdengine/public/getsessions",
            type: 'GET',
            data: {id: active,type:1},
            dataType: 'json', // added data type
            success: function (res) {
                //  console.log(res);
                //    alert(res);
                var finalresp = res;
                var markup = "";
                for (var i = 0; i <= res.length; i++) {
                    console.log(res[i]);
                    //   var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                    //
                    if (typeof res[i] === "undefined") {
                        console.log('here us undefined');
                    } else {
                       // markup = markup + "<tr><td><input type='radio'  onclick='getdetails("+res[i]['project_id']+")' name='record' value="+ res[i]['project_id'] +"></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                   markup = markup + "<tr><td><a data-toggle='tab' href='#contact-1' class='client-link'>"+ res[i]['project_name'] +"</a></td><td>"+ res[i]['session'] +"</td><td>"+ res[i]['text'] +"</td> <td>" + res[i]['status_code'] +"</td><td>"+ res[i]['status'] +"</td><td>"+ res[i]['created_at'] +"</td></tr>";
                    }
                }
                $("#sessions_table").empty().append(markup);
               // var active = finalresp[0]['project_id'];
               // getdetails(finalresp[0]['project_id']);
                // $("#app_name").append("raymond here");
                // $("#app_status").append("raymond here");
                // $("#app_created_at").append("raymond here");
                // $("#ext_url").append("raymond here");
                // $("#int_url").append("raymond here");
            }
        });

    }
    function ajaxcallstats(){
        //
        console.log(active);
        var current_id = active;
        $.ajax({
            url: "/ussdengine/public/getstatistics",
            type: 'GET',
            data: {id: current_id},
            dataType: 'json', // added data type
            success: function (stats) {
                console.log(stats);
                pie(stats);
                //    alert(res);
                //  $("#applications").append(markup);

//                $("#app_name").empty().append(res[0]['project_name']);
//                $("#app_status").empty().append(res[0]['name']);
//                $("#app_created_at").empty().append(res[0]['created_at']);
//                $("#ext_url").empty().append(res[0]['endpoint_external']);
//                $("#int_url").empty().append(res[0]['endpoint_internal']);
            }

        });
      //  alert('here');
    }
    function pie(stats){
        var doughnutData = {
            labels: ["Success","Failed" ],
            datasets: [{
                data: [stats['success'],stats['failed']],
                backgroundColor: ["#38E11D","#E10F09"]
            }]
        } ;


        var doughnutOptions = {
            responsive: true
        };


        var ctx4 = document.getElementById("doughnutChart").getContext("2d");
        new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
    }

    function getdetails(id) {
        active = id;
        $.ajax({
            url: "/ussdengine/public/getdetails",
            type: 'GET',
            data: {id: id},
            dataType: 'json', // added data type
            success: function (response) {
                console.log(response);
                //    alert(res);
                var res = response;
                //var res = response['project'];
                console.log(res);
                //  $("#applications").append(markup);

                $("#app_name").empty().append(res[0]['project_name']);
                $("#app_status").empty().append(res[0]['name']);
                $("#app_created_at").empty().append(res[0]['created_at']);
                $("#ext_url").empty().append(res[0]['endpoint_external']);
                $("#int_url").empty().append(res[0]['endpoint_internal']);
              //  pie(response['sessions']);
            }

        });

    }
	/* function getdetails(id) {
        $.ajax({
            url: "/ussdengine/public/getdetails",
            type: 'GET',
            data: {id: id},
            dataType: 'json', // added data type
            success: function (res) {
                 console.log(res);
                //    alert(res);
              //  $("#applications").append(markup);

                $("#app_name").empty().append(res[0]['project_name']);
                $("#app_status").empty().append(res[0]['name']);
                $("#app_created_at").empty().append(res[0]['created_at']);
                $("#ext_url").empty().append(res[0]['endpoint_external']);
                $("#int_url").empty().append(res[0]['endpoint_internal']);
            }
        });

    } */
	$.ajax({
        url: "/ussdengine/public/getprojects",
        type: 'GET',
        data: {id: active,type:1},
        dataType: 'json', // added data type
        success: function (res) {
            //  console.log(res);
            //    alert(res);
            var finalresp = res;
            var markup = "";
            for (var i = 0; i <= res.length; i++) {
                console.log(res[i]);
                //   var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                //
                if (typeof res[i] === "undefined") {
                    console.log('here us undefined');
                } else {
                    markup = markup + "<tr><td><input type='radio'  onclick='getdetails("+res[i]['project_id']+")' name='record' value="+ res[i]['project_id'] +"></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                }
            }
            $("#applications").empty().append(markup);
            var active = finalresp[0]['project_id'];
            getdetails(finalresp[0]['project_id']);
            // $("#app_name").append("raymond here");
            // $("#app_status").append("raymond here");
            // $("#app_created_at").append("raymond here");
            // $("#ext_url").append("raymond here");
            // $("#int_url").append("raymond here");
        }
    });

   

</script>
@endpush