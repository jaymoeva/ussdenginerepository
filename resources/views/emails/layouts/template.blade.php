<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="{{ asset('css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/email.css') }}" media="all" rel="stylesheet" type="text/css" />

</head>

<body>

	<table class="body-wrap">
	    <tr>
	        <td></td>
	        <td class="container" width="600">
	            <div class="content">
	                <table class="main" width="100%" cellpadding="0" cellspacing="0">
	                    <tr>
	                        <td class="content-wrap">
	                            <table  cellpadding="0" cellspacing="0">
	                                <tr>
	                                    <td>
	                                        <img class="img-responsive" src="{{ asset('img/banners/email-banner.png') }}"/>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td class="content-block">
	                                        <h3>@yield('salutation')</h3>
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td class="content-block">
	                                       @yield('intro-text')
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td class="content-block">
	                                       @yield('body')
	                                    </td>
	                                </tr>
	                                <tr>
	                                    <td class="content-block aligncenter">
	                                        @yield('button')
	                                    </td>
	                                </tr>
	                                <tr>
	                                	<td class="content-block">
	                                		<strong>
	                                			Thank You, <br />
	                                			{{ config('app.name') }}
	                                		</strong>
	                                	</td>
	                                </tr>
	                              </table>
	                        </td>
	                    </tr>
	                </table>
	                <div class="footer">
	                    <table width="100%">
	                        <tr>
	                            <td class="aligncenter content-block">&copy; Edgetech Consults Limited | All Rights Reserved</td>
	                        </tr>
	                    </table>
	                </div></div>
	        </td>
	        <td></td>
	    </tr>
	</table>

</body>