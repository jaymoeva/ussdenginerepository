@extends('emails.layouts.template')

@section('salutation')
    Dear {{ $user->name }},
@stop

@section('intro-text')
    We are pleased to notify you that your account has been activated.
@stop

@section('body')
    Log in below to get started.
@stop

@section('button')
    <a href="{{ url('login') }}" class="btn-primary">Login</a>
@stop