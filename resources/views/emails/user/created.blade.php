@extends('emails.layouts.template')

@section('salutation')
    Dear {{ $user->name }},
@stop

@section('intro-text')
    We would like to notify you that your account has been created.
@stop

@section('body')
    All the relevant details will be sent to you upon request.
@stop

@section('button')
    <a href="#" class="btn-primary">Request new details</a>
@stop