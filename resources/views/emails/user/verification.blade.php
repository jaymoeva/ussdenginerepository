@extends('emails.layouts.template')

@section('salutation')
    Dear {{ $user->name }},
@stop

@section('intro-text')
    Thanks for signing up. You are now registered with our app.
@stop

@section('body')
    We may need to send you critical information about our services and it is important that we have an accurate email address. Please note that your account is due activation. We will promptly notify you once that is done.
@stop

@section('button')
    <a href="{{ url('register/verify',$user->token) }}" class="btn-primary">Verify email address</a>
@stop