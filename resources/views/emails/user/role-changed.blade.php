@extends('emails.layouts.template')

@section('salutation')
    Dear {{ $user->name }},
@stop

@section('intro-text')
    We would like to notify you that your role has been changed.
@stop

@section('body')
    You have been assigned the @foreach($user->roles as $role) <strong>{{ $role->name }}</strong> @endforeach role. 
@endsection

@section('button')
    
@stop