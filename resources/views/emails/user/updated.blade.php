@extends('emails.layouts.template')

@section('salutation')
    Dear {{ $user->name }},
@stop

@section('intro-text')
    We would like to notify you that your details have been updated.
@stop

@section('body')
    Please read our <a href="#">terms &amp; conditions</a> to see why this might be the case.
@stop

@section('button')
    <a href="#" class="btn-primary">Request new details</a>
@stop