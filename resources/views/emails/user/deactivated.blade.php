@extends('emails.layouts.template')

@section('salutation')
    Dear {{ $user->name }},
@stop

@section('intro-text')
   Kindly note that your account has been deactivated.
@stop

@section('body')
    This means that you can no longer access some services in our application.
@stop