@extends('backend/layouts/template')

@section('title')
	Election and Polls
@endsection

@push('custom-styles')
	<link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
  <link href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Election and Polls</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Election and Polls</strong>
                </li>
                <li>
                  <a href="{{ url('/listpolls') }}">View All Polls</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
	<div class="row">
		<!-- Menu initiator -->
		<div class="col-lg-6 col-lg-offset-3">
        <div class="ibox">
        	<div class="ibox-title">
        		<h5>Election and Polls</h5>
        	</div>
        	<div class="ibox-content">
        		<form action="{{ url('create_polls') }}" method="POST">
				{{ csrf_field() }}
                <div class="row">
                   <div class="col-md-8">
                     <div class="form-group">
                       <label for="">Date Range</label>
                       <input class="form-control" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
                     </div>
                   </div>
                   <div class="col-md-4 clockpicker" data-autoclose="true">
                       <label for="">Time</label>
                       <input type="text" class="form-control" value="09:30">
                   </div>

                 </div>
				 <div class="form-group">
              		<label for="">Polls Name</label>
              		<input type="text" name="pollsname" class="form-control">
              	</div>
                </div>
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="name" class="form-control">
                </div>
              	<div class="form-group">
              		<label for="">Question</label>
              		<input type="text" name="triviaquestion" class="form-control">
              	</div>
                <div class="form-group">
                  <label for="">Expected Responses</label> <a href="" class="pull-right">Add Responses</a> 
                  <input type="text" name="expected_responses" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Question Description</label>
                    <textarea class="form-control m-b" required></textarea>
                 </div>

              
            	<button type="submit" class="btn btn-primary btn-sm btn-block">Add</button>
				</form> 

            <button type="submit" class="btn btn-primary btn-sm btn-block"></button>
            </form>
        	</div>
        </div>
    </div>
@endsection
<<<<<<< HEAD

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
  new Chart(document.getElementById("pie-chart"), {
    type: 'pie',
    data: {
      labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
        data: [2478,5267,734,784,433]
      }]
    },
    options: {
      title: {
        display: true,
        text: 'Predicted world population (millions) in 2050'
      }
    }
});
  });
</script>
=======
<!--
<script language="JavaScript" type="text/javascript" src="/js/jquery-1.2.6.min.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/jquery-ui-personalized-1.5.2.packed.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/sprinkle.js"></script> -->
@push('scripts')
    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="{{ asset('plugins/fullcalendar/moment.min.js') }}"></script>
    <!-- Date range picker -->
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Clock picker -->
    <script src="{{ asset('plugins/clockpicker/clockpicker.js') }}"></script>
    <script>
      $(document).ready(function () {
          $('input[name="daterange"]').daterangepicker();
          $('.clockpicker').clockpicker();
      });
    </script>
@endpush
>>>>>>> cac0a6fcf21d65b0901fb8193592336519393af5
