@extends('backend/layouts/template')

@section('title')
  Email Groups
@endsection

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
  <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@endpush

@push('styles')
    <link href="{{ asset('plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
@endpush

@section('page-header')
  <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Marketing</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Marketing</a>
                </li>
                <li>
                  <a href="{{ route('marketingsms.send') }}">Email</a>
                </li>
                <li>
                  <strong>SMS Groups</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Available SMS Groups</h5>
          <div class="ibox-tools">
            <a href="#newgroup" class="btn btn-primary btn-sm" data-toggle="modal">Add new group</a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Created on</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
               <tr>
                  <td>Edgetech Consults</td>
                  <td>Lorem ipsum</td>
                  <td>{{ date("Y/m/d") }}</td>
                  <td>
                    <a href="{{ route('marketingsms.showgroup') }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                    <a href="{{ route('marketingsms.editgroup') }}" class="btn btn-xs btn-success" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp; &nbsp;
                    <a href="#contacts" class="btn btn-xs btn-info" title="Import Contacts" data-toggle="modal"><i class="fa fa-user" aria-hidden="true"></i></a>&nbsp; &nbsp;
                    {!! Form::open(['method' => 'DELETE', 'style' => 'display: inline-block']) !!}   
                       {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                    {!! Form::close() !!} &nbsp;&nbsp;
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

   <div class="modal inmodal" id="newgroup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated flipInX">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h6 class="modal-title">Add a new group</h6>
                <small class="font-bold">Create an sms group and insert contacts</small>
            </div>
            <form role="form" action="" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">                   
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Name</label>
                        <input id="name" type="text" class="form-control" name="name" placeholder="eg Urgent List" required>
                        <!-- If name has error -->
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label>Description</label>
                        <textarea id="description" class="form-control" placeholder="List containing urgent mail recipients" name="description" required></textarea>
                        <!-- If email has error -->
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
  </div>


   <!-- Import CSV Modal -->
  <div class="modal inmodal" id="contacts" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content animated flipInX">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h6 class="modal-title">Import CSV</h6>
                  <small class="font-bold">Import a csv file to add multiple contacts to this group</small>
              </div>
              <form role="form" action="" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="modal-body">
                      <div class="fileinput fileinput-new input-group form-group{{ $errors->has('contacts') ? ' has-error' : '' }}" data-provides="fileinput">
                          <div class="form-control" data-trigger="fileinput">
                              <i class="fa fa-file fileinput-exists"></i>
                          <span class="fileinput-filename"></span>
                          </div>
                          <span class="input-group-addon btn btn-default btn-file">
                              <span class="fileinput-new">Select file</span>
                              <span class="fileinput-exists">Change</span>
                              <input type="file" name="avatar" required/>
                          </span>
                          <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                          <!-- If first name has error -->
                          @if ($errors->has('contacts'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('contacts') }}</strong>
                              </span>
                          @endif
                      </div> 
                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary btn-block btn-sm">Import</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <!--// End CSV Modal //-->
@endsection