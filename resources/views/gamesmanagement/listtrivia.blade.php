@extends('backend/layouts/template')

@section('title')
  Menus
@endsection

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
@endpush

@section('page-header')
  <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Trivia</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="#">Games Management</a>
                </li>
                <li class="active">
                    <strong>Trivia</strong>
                </li> 
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')

 <div class="wrapper wrapper-content animated fadeInRight">

     
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Trivias List</h5>

                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                         <!--    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                                   placeholder="Search in table"> -->

                            <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Trivia Name</th>
                                    <th data-hide="phone,tablet">Description</th>
                                    <!--th data-hide="phone,tablet">Questions</th>
                                    <th data-hide="phone,tablet">Participants</th-->
                                    <th>View</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $pos=1 ?>
                                @foreach($trivia_details as $trivia_detail)
                                <tr class="gradeX">
                                    <td>{{$pos}}</td>
                                    <td>{{$trivia_detail->trivia_name}}
                                    </td>
                                    <td>{{$trivia_detail->description}}</td>
                                    <!--td class="center">0</td>
                                    <td class="center">0</td-->
                                    <td><a href="viewtrivia?id={{$trivia_detail->id}}" class="btn btn-success btn-sm col-lg-12">View</a></td>
                                    <td><a href="deletetrivia?id={{$trivia_detail->id}}" class="btn btn-danger btn-sm col-lg-12">Delete</a></td>
                                </tr>
                                <?php $pos++ ?>
                                @endforeach

                                
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection