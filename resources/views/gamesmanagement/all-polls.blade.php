@extends('backend/layouts/template')

@section('title')
	All Polls
@endsection

@section('page-header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>All Polls</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Games Management</a>
                </li>
                <li>
                    <a href="{{ route('polls') }}">Elections and Polls</a>
                </li>
                <li>
                    <strong>View All Polls</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>All polls</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Created On</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            @if(count($polls) > 0)
                            <tbody>
								<?php $i = 0 ?>
                                @foreach($polls as $poll)
								<?php $i++ ?>
                                <tr>

                                    <td>{{$i}}</td>
                                    <td>{{$poll->name}}</td>
                                    <td>{{$poll->starts}}</td>
                                    <td>{{$poll->ends}}</td>
                                    <td>active</td> 
                                    <td>{{$poll->created_at}}</td>
                                    <td> 
                                        <a href="{{ route('polls.details',['id'=>$poll->id]) }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                       <a href="{{ url('edit-poll') }}" class="btn btn-xs btn-success" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                        {!! Form::open(['method' => 'DELETE', 'style' => 'display: inline-block']) !!}   
                                           {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                                        {!! Form::close() !!} &nbsp;&nbsp;
                                    </td>
                                </tr>
                                
								
                                @endforeach
                            </tbody>
							@endif
                        </table>
						{{ $polls->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection