@extends('backend/layouts/template')

@section('title')
	Player Details
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Player Details</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Games Management</a>
                </li>
                <li>
                    <a href="/trivia">Trivia</a>
                </li>
                <li>
                    <a href="/players">Players</a>
                </li>
                <li class="active">
                    <strong>Player Details</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>About the player</h5>
                </div>
                <div>
                    <div class="ibox-content profile-content">
                        <h4><strong>Anthony Mutinda</strong></h4>
                        <p><i class="fa fa-envelope"></i> toneymutinda@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Player Activity Detail</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <div class="feed-activity-list">
                            <div class="feed-element">
                                <span class="pull-left">
                                    <i class="fa fa-user fa-2x"></i>
                                </span>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">1m ago</small>
                                     Your account was approved <br>
                                    <small class="text-muted">Today 4:30 pm</small>
                                    <div class="actions">
                                        <a class="btn btn-xs btn-danger" href="#"><i class="fa fa-envelope"></i> Message admin</a>
                                    </div>
                                </div>
                            </div>
                            <div class="feed-element">
                                <span class="pull-left">
                                    <i class="fa fa-lock fa-2x"></i>
                                </span>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">2 days ago</small>
                                     Your roles details have changed <br>
                                    <small class="text-muted">Monday 1:21 pm</small>
                                    <div class="actions">
                                        <a class="btn btn-xs btn-primary" href="#"><i class="fa fa-book"></i> Read why</a>
                                    </div>
                                </div>
                            </div>
                            <div class="feed-element">
                                <span class="pull-left">
                                    <i class="fa fa-laptop fa-2x"></i>
                                </span>
                                <div class="media-body ">
                                    <small class="pull-right">2h ago</small>
                                    <strong>Session ID EUSE546Y</strong> has just expired <br>
                                    <small class="text-muted">Today 2:10 pm - 12.06.2014</small>
                                    <div class="well">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                        Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-xs btn-white"><i class="fa fa-laptop"></i> What does this mean?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block m"><i class="fa fa-arrow-down"></i> Show More</button>
                </div>
            </div>
        </div>
    </div>
@endsection