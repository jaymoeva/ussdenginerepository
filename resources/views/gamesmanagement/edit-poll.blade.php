@extends('backend/layouts/template')

@section('title')
	Edit Poll
@endsection

@push('custom-styles')
	<link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
  <link href="{{ asset('plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Election and Polls</h2>
            <ol class="breadcrumb"> 
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('polls') }}">Election and Polls</a>
                </li>
                <li>
                  <a href="{{ url('/listpolls') }}">View All Polls</a>
                </li>
                <li class="active">
                    <strong>Edit Poll</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit Poll</h5>
                </div>
                <div class="ibox-content">
                    <form action="">
                        <div class="row">
                           <div class="col-md-8">
                             <div class="form-group">
                               <label for="">Date Range</label>
                               <input class="form-control" type="text" name="daterange" value="01/01/2015 - 01/31/2015" />
                             </div>
                           </div>
                           <div class="col-md-4 clockpicker" data-autoclose="true">
                               <label for="">Time</label>
                               <input type="text" class="form-control" value="09:30">
                           </div>
                        </div>
                        <div class="form-group">
                          <label>Name</label>
                          <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Question</label>
                            <input type="text" name="trivianame" class="form-control">
                        </div>
                        <div class="form-group" id="list">
                            <div class="form-group">
                                <label>Expected Responses</label> <a href="#" class="pull-right list_add">Add field</a>
                                <div class="list_var">
                                    <div class="row">
                                        <div class="col-md-10">
                                           <input type="text" size="40" name="list_0" id="list_0" class="form-control"> 
                                        </div>
                                        <div class="col-md-2">
                                           <button class="btn btn-danger btn-sm list_del">Delete</button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Question Description</label>
                            <textarea class="form-control m-b" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm btn-block">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="{{ asset('plugins/fullcalendar/moment.min.js') }}"></script>
    <!-- Date range picker -->
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- Clock picker -->
    <script src="{{ asset('plugins/clockpicker/clockpicker.js') }}"></script>
    <script src="{{ asset('plugins/jquery-addInput-area/jquery.add-input-area.js') }}"></script>
    <script>
      $(document).ready(function () {
          $('input[name="daterange"]').daterangepicker();
          $('.clockpicker').clockpicker();
          $('#list').addInputArea();
      });
    </script>
@endpush