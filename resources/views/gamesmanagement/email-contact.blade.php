@extends('backend/layouts/template')

@section('title')
  View Email Contact
@endsection

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
  <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@endpush

@push('styles')
    <link href="{{ asset('plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
@endpush

@section('page-header')
  <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Marketing</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Marketing</a>
                </li>
                <li>
                  <a href="{{ route('marketingemail.send') }}">Email</a>
                </li>
                <li>
                  <a href="{{ route('marketingemail.groups') }}">Email groups</a>
                </li>
                <li>
                  <strong>View email contact</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-4">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Contact Information</h5>
        </div>
        <div class="ibox-content">
          <div class="feed-activity-list">
            <div class="feed-element">
              <i class="fa fa-envelope"></i> Email
              <span class="pull-right">johndoe@example.com</span>
            </div>
            <div class="feed-element">
              <i class="fa fa-envelope-open"></i> Emails sent
              <span class="pull-right">2</span>
            </div>
            <div class="feed-element">
              <i class="fa fa-rss"></i> Subscribed
              <span class="pull-right">Yes</span>
            </div>
            <div class="feed-element">
              <i class="fa fa-pencil"></i> Edit contact <br>
              <form action="">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email" id="newEmail" placeholder="johndoe@example.com" {{ old('email') }}>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <button type="submit" class="btn btn-block btn-primary btn-sm">Update</button>
              </form>
            </div>
            <div class="feed-element">
              <div class="row">
                <div class="col-lg-12">
                  <a href="" class="btn btn-danger btn-block btn-sm">Unsubscribe</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Email sent to this contact</h5>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-responsive table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>Subject</th>
                  <th>Message</th>
                  <th>Sent on</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Bulk Activation</td>
                  <td>Activate your bulk accounts to continue receiving the latest news in softwa...</td>
                  <td>{{ date("Y/m/d") }}</td>
                  <td>
                    <a href="" class="btn btn-xs btn-primary" title="View Email"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection