@extends('backend/layouts/template')

@section('title')
  SMS Marketing
@endsection

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
  <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@endpush

@push('styles')
    <link href="{{ asset('plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
@endpush

@section('page-header')
  <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Marketing</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Marketing</a>
                </li>
                <li>
                  <strong>SMS Marketing</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-4">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Groups Summary</h5>
        </div>
        <div class="ibox-content">
          <div class="feed-activity-list">
            <div class="feed-element">
              <i class="fa fa-user"></i> SMS groups
              <span class="pull-right">0</span>
            </div>
            <div class="feed-element">
              <div class="row">
                <div class="col-md-6">
                  <a href="#newgroup" class="btn btn-block btn-sm btn-primary" data-toggle="modal">Add new group</a>
                </div>
                <div class="col-md-6">
                  <a href="{{ route('marketingsms.groups') }}" class="btn btn-block btn-sm btn-primary">View groups</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Send SMS</h5>
        </div>
        <div class="ibox-content">
          <form action="#" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
              <label>SMS message</label>
              <textarea class="form-control" name="message"></textarea>
            </div>
            <div class="form-group">
              <label>Select sms group</label>
              <div class="form-group">
                  <select class="chosen-select" data-placeholder="Email Group" id="group" name="group" tabindex="2">
                      <option></option>
                      <option value="">Edgetech</option>
                      <option value="">Stonewood</option>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-block btn-sm btn-primary">Send</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal inmodal" id="newgroup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated flipInX">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h6 class="modal-title">Add a new group</h6>
                <small class="font-bold">Create an sms group and insert contacts</small>
            </div>
            <form role="form" action="" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">                   
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Name</label>
                        <input id="name" type="text" class="form-control" name="name" placeholder="eg Urgent List" required>
                        <!-- If name has error -->
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label>Description</label>
                        <textarea id="description" class="form-control" placeholder="List containing urgent sms recipients" name="description" required></textarea>
                        <!-- If email has error -->
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
  </div>
@endsection

@push('scripts')
    <!-- Jasny -->
    <script src="{{ asset('plugins/jasny/jasny-bootstrap.min.js') }}"></script>
@endpush

@push('graph-scripts')
     <!-- Chosen -->
    <script src="{{ asset('plugins/chosen/chosen.jquery.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.chosen-select').chosen({width: "100%"});
        });
    </script>
@endpush
