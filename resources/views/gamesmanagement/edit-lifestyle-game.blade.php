@extends('backend/layouts/template')

@section('title')
	Edit Lifestyle Game
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Edit Lifestyle Game</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                  <a href="#">Games Management</a>
                </li>
                <li>
                  <a href="#">Lifestyle</a>
                </li>
                <li>
                  <a href="{{ route('lifestylegames.index') }}">Games Present</a>
                </li>
                <li>
                    <strong>Edit Lifestyle Game</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit lifestyle game</h5>
                </div>
                <div class="ibox-content">
                    <form action="" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="lifestylegame" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                          <textarea name="lifestyledesc" class="form-control m-b" required></textarea>
                        </div>
                        <div> <button type="submit" class="btn btn-primary btn-block">Add</button></div>
                    </form>
                </div>
            </div> 
        </div>
    </div>
@endsection


