@extends('backend/layouts/template')

@section('title')
  Edit SMS Group
@endsection

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
  <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@endpush

@push('styles')
    <link href="{{ asset('plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
@endpush

@section('page-header')
  <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Marketing</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Marketing</a>
                </li>
                <li>
                  <a href="{{ route('marketingsms.send') }}">SMS</a>
                </li>
                <li>
                  <a href="{{ route('marketingsms.groups') }}">SMS Groups</a></strong>
                </li>
                <li>
                  <strong>Edit Group</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Edit this SMS group</h5>
        </div>
        <div class="ibox-content">
          <form action="" method="POST">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              <label>Name</label>
              <input type="text" name="name" class="form-control">
              @if($errors->has('name'))
                <span class="help-block">
                  {{ $errors->first('name') }}
                </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              <label>Description</label>
              <textarea name="description" class="form-control"></textarea>
              @if($errors->has('description'))
                <span class="help-block">
                  {{ $errors->first('description') }}
                </span>
              @endif
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-block btn-primary btn-sm">Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection