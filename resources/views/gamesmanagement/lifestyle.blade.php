@extends('backend/layouts/template')

@section('title')
  Menus
@endsection

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
@endpush

@section('page-header')
  <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Lifestyle</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Lifestyle</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
  <div class="row">
    <!-- Menu initiator -->
    <div class="col-lg-6">
            <div class="ibox">
              <div class="ibox-title">
                <h5>Add Participants</h5>
              </div>
              <div class="ibox-content">
                <form action="">
                    <div class="form-group">
                      <label for="">Select Lifestyle</label>
                      <select class="form-control">
                        <option value="">Select Lifestyle</option>
                        <option class="">Modelling</option>
                        <option class="">Dancing</option>
                        <option class="">Music</option>
                      </select>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="">First Name </label>
                          <input type="text" name="trivianame" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="">Last Name </label>
                          <input type="text" name="trivianame" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="">Keyword (Code) <a href="" class="pull-right">Generate</a></label>
                          <input type="text" name="trivianame" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="">Phone </label>
                          <input type="text" name="trivianame" class="form-control">
                        </div>
                      </div>
                    </div>
                      <div class="form-group">
                      <label for="">Address </label>
                      <input type="text" name="trivianame" class="form-control">
                    </div>
                  </form>
                  <a href="" class="btn btn-primary btn-sm btn-block">Add</a>
              </div>
            </div>
    </div>
    <!--// End Menu Initiator //-->
    <!-- Wizard -->
    <div class="col-lg-6">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Lifestyle  Participants</h5>
        </div>
        <div class="ibox-content">
          
<div class="row">
                <div class="col-lg-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1"> Participants</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2">Statistics</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3">Analysis</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                <table class="table table-stripped">
                                  <th>Name</th>
                                  <th>Code</th>
                                  <th>Phone</th>
                                  <th>Address</th>
                                  <th>Votes</th>
                                  <tr>
                                    <td>jawiwy chegg</td>
                                    <td>ja12</td>
                                    <td>0715254854</td>
                                    <td>Rongai</td>
                                    <td>15</td>
                                  </tr>
                                </table> 
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <strong>Voting Participation</strong>


                                </div>
                            </div>

                             <div id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                    <strong>Responses Summarry</strong>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>

        </div>
      </div>
    </div>
    <!-- // End Wizard // -->
  </div>
@endsection
