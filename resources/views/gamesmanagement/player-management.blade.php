@extends('backend/layouts/template')

@section('title')
	All Players
@endsection

@section('page-header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>All Players</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Games Management</a>
                </li>
                <li>
                    <a href="/polls">Trivia</a>
                </li>
                <li>
                    <strong>Player Management</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>All polls</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            {{-- @if(count($polls) > 0) this will go here --}}
                            <tbody>
                                {{-- foreach loop will go here --}}
                                <tr>
                                    <td>1</td>
                                    <td>Anthony Mutinda</td>
                                    <td>toneymutinda@gmail.com</td>
                                    <td>
                                        <a href="{{ url('player-details') }}" class="btn btn-xs btn-primary" title="View Player"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Raymond Sigei</td>
                                    <td>rsigei@edgetech.co.ke</td>
                                    <td>
                                        <a href="{{ url('player-details') }}" class="btn btn-xs btn-primary" title="View Player"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                        {{-- Pagination links --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection