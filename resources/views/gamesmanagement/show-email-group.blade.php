@extends('backend/layouts/template')

@section('title')
  View Email Group
@endsection

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
  <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('plugins/chosen/bootstrap-chosen.css') }}" rel="stylesheet">
@endpush

@push('styles')
    <link href="{{ asset('plugins/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet">
@endpush

@section('page-header')
  <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Marketing</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Marketing</a>
                </li>
                <li>
                  <a href="{{ route('marketingemail.send') }}">Email</a>
                </li>
                <li>
                  <a href="{{ route('marketingemail.groups') }}">Email groups</a>
                </li>
                <li>
                  <strong>View email group</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-info">
        Want to add more than one email? <a href="#contacts" data-toggle="modal">Import a list</a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-4">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Groups Summary</h5>
        </div>
        <div class="ibox-content">
          <div class="feed-activity-list">
            <div class="feed-element">
              <i class="fa fa-bookmark"></i> Name
              <span class="pull-right">{{ $group->name }}</span>
            </div>
            <div class="feed-element">
              <i class="fa fa-sticky-note"></i> Description <br>
              <p>{{ $group->description }}</p>
            </div>
            <div class="feed-element">
              <i class="fa fa-user"></i> Contacts
              <span class="pull-right">{{ count($contacts) }}</span>
            </div>
            <div class="feed-element">
              <i class="fa fa-user-plus"></i> Add contact <br>
              <form action="{{ route('emailcontact.add', $group->id) }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email" placeholder="johndoe@example.com" value="{{ old('email') }}" id="newEmail">
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <button type="submit" class="btn btn-block btn-success btn-success">Add</button>
              </form>
            </div>
            <div class="feed-element">
              <div class="row">
                <div class="col-md-6">
                  <a href="#newgroup" class="btn btn-block btn-sm btn-primary" data-toggle="modal">Edit group</a>
                </div>
                <div class="col-md-6">
                  <a href="{{ route('marketingemail.groups') }}" class="btn btn-block btn-sm btn-primary">Back to all groups</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Send email to this group</h5>
        </div>
        <div class="ibox-content">
          <form action="{{ route('marketingemail.bulksend', $group->id) }}" method="POST">
              {{ csrf_field() }}
              <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                <label>Subject</label>
                <input type="text" name="subject" class="form-control" value="{{ old('subject') }}">
                @if($errors->has('subject'))
                  <span class="help-block">
                    {{ $errors->has('subject') }}
                  </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                <label>Email message</label>
                <textarea class="form-control" name="message"></textarea>
                @if($errors->has('message'))
                  <span class="help-block">
                    {{ $errors->has('message') }}
                  </span>
                @endif
              </div>
              <div class="form-group">
                <label>Email group</label>
                <div class="form-group{{ $errors->has('group') ? ' has-error' : '' }}">
                    <input type="text" value="{{ $group->name }}" name="group" class="form-control" readonly>
                    @if($errors->has('name'))
                      <span class="help-block">
                        {{ $errors->has('name') }}
                      </span>
                    @endif
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-block btn-sm btn-primary">Send</button>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Contacts in this group</h5>
          <div class="ibox-tools">
            <button class="btn btn-primary btn-sm" id="addContact">Add Contact</button> &nbsp;
            <a href="" class="btn btn-primary btn-sm">Export List</a>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                  <tr>
                      <th>Email</th>
                      <th>Added On</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                @foreach($contacts as $contact)
                  <tr>
                    <td>{{ $contact->email }}</td>
                    <td>{{ $contact->created_at }}</td>
                    <td>
                      <a href="{{ route('marketingemail.emailcontact', $contact->id) }}" class="btn btn-xs btn-primary" title="View Contact"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                      {!! Form::open(['method' => 'DELETE', 'style' => 'display: inline-block']) !!}   
                         {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                      {!! Form::close() !!} &nbsp;&nbsp;
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Import CSV Modal -->

  <div class="modal inmodal" id="contacts" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content animated flipInX">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h6 class="modal-title">Import CSV</h6>
                  <small class="font-bold">Import a csv file to add multiple contacts to this group</small>
              </div>
              <form role="form" action="" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="modal-body">
                      <div class="fileinput fileinput-new input-group form-group{{ $errors->has('contacts') ? ' has-error' : '' }}" data-provides="fileinput">
                          <div class="form-control" data-trigger="fileinput">
                              <i class="fa fa-file fileinput-exists"></i>
                          <span class="fileinput-filename"></span>
                          </div>
                          <span class="input-group-addon btn btn-default btn-file">
                              <span class="fileinput-new">Select file</span>
                              <span class="fileinput-exists">Change</span>
                              <input type="file" name="avatar" required/>
                          </span>
                          <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                          <!-- If first name has error -->
                          @if ($errors->has('contacts'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('contacts') }}</strong>
                              </span>
                          @endif
                      </div> 
                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary btn-block btn-sm">Import</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <!--// End CSV Modal //-->
@endsection

@push('scripts')
  <script>
    $(document).ready(function () {
      $('#addContact').click(function(){
          $('#newEmail').focus();
      });
    });
  </script>
  <!-- Jasny -->
  <script src="{{ asset('plugins/jasny/jasny-bootstrap.min.js') }}"></script>
@endpush