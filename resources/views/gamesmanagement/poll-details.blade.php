@extends('backend/layouts/template')

@section('title')
	Poll Details
@endsection

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Menus</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li>
                    <a href="#">Games Management</a>
                </li>
                <li>
                    <a href="/polls">Election and Polls</a>
                </li>
                <li>
                    <a href="/listpolls">All Polls</a>
                </li>
                <li>
                    <strong>Poll Details</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Poll Details</h5>
                </div>
                <div class="ibox-content">
                   <div>
                        <div class="feed-activity-list">
						<div class="feed-element">
                                <i class="fa fa-send"></i>Polls Name
                                <span class="pull-right">
								{{$poll_details->name}}
                                </span>
                            </div>
							<div class="feed-element">
                                <i class="fa fa-send"></i>Polls Question
                                <span class="pull-right">
								{{$poll_details->question}}
                                </span>
                            </div>
							<div class="feed-element">
                                <i class="fa fa-send"></i>Polls Desc
                                <span class="pull-right">
								{{$poll_details->desc}}
                                </span>
                            </div>
							<div class="feed-element">
                                <i class="fa fa-send"></i>Polls Response
								@foreach($responses as $response)
								<li> <span class="pull-centre">
								{{$response}}
                                </span></li>
								@endforeach
                               
                            </div>
							<div class="feed-element">
                                <i class="fa fa-user"></i> Participants
                                <span class="pull-right">
								{{$participants}}
                                </span>
                            </div>
							<div class="feed-element">
                                <i class="fa fa-send"></i> Poll Type
                                <span class="pull-right">
                                    Multiple
                                </span>
                            </div>
                            <div class="feed-element">
                                <i class="fa fa-calendar"></i> Created On
                                <span class="pull-right">{{$poll_details->created_at}}</span>
                            </div>
							<div class="feed-element">
                                <i class="fa fa-calendar"></i> Starts At
                                <span class="pull-right">{{$poll_details->starts}}</span>
                            </div>
							<div class="feed-element">
                                <i class="fa fa-calendar"></i> Ends On
                                <span class="pull-right">{{$poll_details->ends}}</span>
                            </div>
                            
                            <div class="feed-element"> 
                                <i class="fa fa-send"></i>Polls Endpoint
                                <span >
								{{$poll_details->endpoint}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="ibox">
                <div class="ibox-content">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#participants"><i class="fa fa-user"></i> Participants</a></li>
                        <li><a data-toggle="tab" href="#stats"><i class="fa fa-bar-chart-o"></i> Poll Statistics</a></li>
                        <li><a data-toggle="tab" href="#responses"><i class="fa fa-sticky-note"></i> Responses</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="participants" class="tab-pane active">
                            <br>
                            <div class="table-responsive">
                                <a href="#" class="btn btn-primary btn-sm">Export as spreadsheet</a>
                                
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Phone Number</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody id="particpants_list">
									@foreach($particpantslist as $particpantslists)
                                        <tr>
                                            <td>{{$particpantslists->msisdn}}</td>
                                            <td>{{$particpantslists->created_at}}</td>
                                        </tr>
                                     @endforeach   
                                    </tbody>
                                </table>
								@if($participants > 1)
								<div class="pull-right">
                                    <button   id="btn-more" data-id="{{$particpantslists->id}}"  class="btn btn-primary btn-sm">Next</button >
                                    <button   id="btn-prev" data-id="{{$particpantslists->id}}"  class="btn btn-primary btn-sm">Previous</button>
                                </div>
								@endif
                            </div>
                        </div>
                        <div id="stats" class="tab-pane">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <span class="label label-info pull-right">Sessions Summary</span>
                                            <h5>USSD Sessions</h5>
                                        </div>
                                        <div class="ibox-content">
                                           <div>
                                                <canvas id="barChart" height="140"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="responses" class="tab-pane">
                            <br>
                            <div class="table-responsive">
                                <div class="pull-right">
                                    <a href="#" class="btn btn-primary btn-sm">Next</a>
                                    <a href="#" class="btn btn-primary btn-sm">Previous</a>
                                </div>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>By</th>
                                            <th>Response Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Anthony Mutinda</td>
                                            <td>Yes</td>
                                        </tr>
                                        <tr>
                                            <td>Raymond Sigei</td>
                                            <td>No</td>
                                        </tr>
                                        <tr>
                                            <td>George Mburu</td>
                                            <td>Yes</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('plugins/chartJs/Chart.min.js') }}"></script>
    <script src="{{ asset('plugins/demo/chartjs-demo.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>

<script>
$(document).ready(function(){
   $(document).on('click','#btn-more',function(){
       var id = $(this).data('id');
	   alert(id);
       $("#btn-more").html("Loading....");
       $.ajax({
           url : '{{ url("poll-participants") }}',
           method : "GET",
           data : {id:id, _token:"{{csrf_token()}}"},
           dataType : "text",
           success : function (data)
           {
              if(data != '') 
              {
				  var markup = "";
                for (var i = 0; i <= data.data; i++) {
                    console.log(data[i]);
                    //   var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
                    //
                    if (typeof data[i] === "undefined") {
                        console.log('here us undefined');
                    } else {
                       // markup = markup + "<tr><td><input type='radio'  onclick='getdetails("+res[i]['project_id']+")' name='record' value="+ res[i]['project_id'] +"></td><td>" + res[i]['project_name'] + "</td><td>" + res[i]['name'] + "</td></tr>";
					markup = markup + "<tr><td>"+data[i]['msisdn']+"</td><td>"+data[i]['created_at']+"</td></tr>";
                    } 
                }
                  $('#remove-row').remove();
                  $('#particpants_list').empty().append(markup);
              }
              else
              {
                  $('#btn-more').html("No Data");
              }
           }
       });
   });  
}); 
</script>
@endpush