@extends('backend/layouts/template')

@section('title')
	Lifestyle Types
@endsection

@section('page-header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>All Lifestyle Types</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Games Management</a>
                </li>
                <li>
                    <a href="#">Lifestyle</a>
                </li>
                <li>
                    <strong>Lifestyle Types</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
     <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                     <h5>All lifestyle types</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('lifestyle.create') }}" class="btn btn-primary btn-sm">Add New</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Created On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            {{-- @if(count($polls) > 0) this will go here --}}
                            <tbody>
                                {{-- foreach loop will go here --}}
                                <tr>
                                    <td>1</td>
                                    <td>Modelling</td>
                                    <td>Lorem ipsum is a latin text phrase</td>
                                    <td>{{ date("Y/m/d") }}</td>
                                    <td>
                                        <a href="{{ url('poll-details') }}" class="btn btn-xs btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                        <a href="{{ route('lifestyle.edit') }}" class="btn btn-xs btn-success" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp; &nbsp;
                                        {!! Form::open(['method' => 'DELETE', 'style' => 'display: inline-block']) !!}   
                                           {{Form::button('<i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger', 'title' => 'Delete'))}}
                                        {!! Form::close() !!} &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                        {{-- Pagination links --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection