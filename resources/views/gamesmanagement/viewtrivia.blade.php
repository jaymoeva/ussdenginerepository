@extends('backend/layouts/template')

@section('title')
  Menus
@endsection

@push('custom-styles')
  <link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
@endpush

@section('page-header')
  <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Trivia</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Games Management</a>
                </li>
                <li class="active">
                    <strong>Trivia</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-4">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>Trivia Details</h5>
        </div>
        <div class="ibox-content">
          <div>
            <div class="feed-activity-list">
              @foreach($trivia_details as $trivia_detail)
                <div class="feed-element">
                  <span><strong>Name</strong></span> <br>
                  <p>{{$trivia_detail->trivia_name}}</p>
                </div>
                <div class="feed-element">
                  <span><strong>Type</strong></span> <br>
                  <p>{{$trivia_detail->trivia_type}}</p>
                </div>
                <div class="feed-element">
                  <span><strong>End-point</strong></span> <br>
                  <p>{{$trivia_detail->url_endpoint}}</p>
                </div>
                <div class="feed-element">
                  <span><strong>Description</strong></span> <br>
                  <p>{{$trivia_detail->description}}</p>
                </div>
                <div class="feed-element">
                  <span><strong>Created Dated</strong></span> <br>
                  <p>{{$trivia_detail->created_at}}</p>
                </div>
              @endforeach
              <div class="feed-element">
                <div class="row">
                  <div class="col-md-6"><a href="{{ url('listtrivia') }}" class="btn btn-primary btn-sm btn-block">View All</a></div>
                  <div class="col-md-6"><a href="{{ url('trivia') }}" class="btn btn-primary btn-sm btn-block">Add New</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="ibox">
        <div class="ibox-title">
          <h5>Trivia Details</h5>
        </div>
        <div class="ibox-content">
          <br>
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab-0"> Add Questions</a></li>
              <li class=""><a data-toggle="tab" href="#tab-1"> Questions</a></li>
              <li class=""><a data-toggle="tab" href="#tab-2"> Statistics</a></li>
              <li class=""><a data-toggle="tab" href="#tab-3"> Participation</a></li>
              <li class=""><a data-toggle="tab" href="#tab-4"> Participants</a></li>
          </ul>
          <div class="tab-content">
            <div id="tab-0" class="tab-pane active">
                <div class="panel-body">
                    <form action="{{ route('trivia.addquestions') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Question</label>
                            <input type="text" name="question" class="form-control col-md-8">
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group col-md-8">
                                <label for="">Choice: A</label>
                                <input type="text" name="answer_a" class="form-control col-md-8">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group col-md-8">
                                <label for="">Choice: B</label>
                                <input type="text" name="answer_b" class="form-control col-md-8">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group col-md-8">
                                <label for="">Choice: C</label>
                                <input type="text" name="answer_c" class="form-control col-md-8">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group col-md-3">
                                <label for="">Answer</label>
                                <input type="text" name="answer"  class="form-control blue-bg">
                            </div>
                        </div>
                        <input type="text" name="trivia_id" value="{{$id}}" hidden >

                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-success btn-lg">Submit Question</button>
                        </div>

                    </form>
                </div>
            </div>
            <div id="tab-1" class="tab-pane">
                <div class="panel-body">
                  <div id="accordion">
                      <?php $pos=1 ?>
                      @foreach($questions as $key=>$question)

                      <div class="card">
                        <div class="card-header" id="headingOne">
                          <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$pos}}" aria-expanded="true" aria-controls="collapse{{$pos}}">
                              Question {{$pos}}. {{$question->question}}
                            </button>
                          </h5>
                        </div>

                        <div id="collapse{{$pos}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                          <div class="card-body ">
                        <!--    <h4><u>Choices & Answer</u></h4> -->
                           <ul style="list-style-type: none;">
                            <li> A. {{$question->choice_a}}  </li>
                            <li> B. {{$question->choice_b}}</li>
                            <li> C. {{$question->choice_c}} </li>
                            <li><b>Answer: {{$question->answer}}</b></li>
                          </ul>
                          </div>
                        </div>
                      </div>
                              <?php $pos++ ?>
                      @endforeach
                  </div>
                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <strong>Trivia Statistics</strong>
                </div>
            </div>
            <div id="tab-3" class="tab-pane">
                <div class="panel-body">
                    <strong>Trivia Participation Analytics</strong>
                </div>
            </div>
            <div id="tab-4" class="tab-pane">
              <br>
              <div class="table-responsive">
                <table class="table table-responsive table-hover table-striped table-hover table-bordered">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Phone Number</th>
                      <th>Email</th>
                      <th>Participated On</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>John Doe</td>
                      <td>0712345678</td>
                      <td>johndoe@example.com</td>
                      <td>{{ date("Y/m/d") }}</td>
                      <td>
                        <a href="" class="btn btn-primary btn-xs" title="View Participants"><i class="fa fa-eye"></i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection