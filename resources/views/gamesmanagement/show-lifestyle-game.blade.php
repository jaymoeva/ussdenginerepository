@extends('backend/layouts/template')

@section('title')
	Show Lifestyle Game
@endsection

@section('page-header')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Show Lifestyle Game</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('dashboard') }}">Dashboard</a>
                </li>
                <li>
                    <a href="#">Games Management</a>
                </li>
                <li>
                    <a href="#">Lifestyle</a>
                </li>
                <li>
                    <a href="{{ route('lifestylegames.index') }}">Games Present</a>
                </li>
                <li>
                    <strong>Show Lifestyle Game</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Game Details</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <div class="feed-activity-list">
                            <div class="feed-element">
                              <span><strong>Name</strong></span> <br>
                              <p>Sample Name</p>
                            </div>
                            <div class="feed-element">
                              <span><strong>Description</strong></span> <br>
                              <p>Sample Description</p>
                            </div>
                            <div class="feed-element">
                              <span><strong>Created Date</strong></span> <br>
                              <p>{{ date("Y/m/d") }}</p>
                            </div>
                            <div class="feed-element">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="{{ url('games-present') }}" class="btn btn-block btn-primary btn-sm">View all</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{ url('edit-lifestyle-game') }}" class="btn btn-block btn-primary btn-sm">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    Game Statistics and Information
                </div>
                <div class="ibox-content">
                      <br>
                      <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#tab-0">Participants</a></li>
                          <li><a href="#tab-1" data-toggle="tab">Quick Stats</a></li>
                          <li><a href="#tab-2" data-toggle="tab">Results</a></li>
                      </ul>
                      <div class="tab-content">
                          <div class="tab-pane active" id="tab-0">
                              
                          </div>
                          <div class="tab-pane" id="tab-1">
                              
                          </div>
                          <div class="tab-pane" id="tab-2">
                              
                          </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
@endsection