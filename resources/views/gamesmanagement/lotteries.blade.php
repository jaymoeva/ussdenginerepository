@extends('backend/layouts/template')

@section('title')
	Menus
@endsection

@push('custom-styles')
	<link rel="stylesheet" href="{{ asset('plugins/steps/jquery.steps.css') }}">
@endpush

@section('page-header')
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Trivia</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Trivia</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')
	<div class="row">
		<!-- Menu initiator -->
		<div class="col-lg-4">
            <div class="ibox">
            	<div class="ibox-title">
            		<h5>Create Trivia</h5>
            	</div>
            	<div class="ibox-content">
            		<form action="">
	                	<div class="form-group">
	                		<label for="">Trivia Name</label>
	                		<input type="text" name="trivianame" class="form-control">
	                	</div>

	                	<div class="form-group">
	                		<label for="">Trivia Type</label>
                            <select class="form-control m-b" required name="triviatype">
                                    <option value="">Please Select Type</option>
                                    <option value="1">Bible</option>
                                    <option value="2">Life</option>
                                </select>

	                	</div>
                        <div class="form-group">
                            <label for="">Trivia Description</label>
                          <textarea class="form-control m-b" required></textarea>
                        </div>
	                </form>
                	<a href="" class="btn btn-primary btn-sm btn-block">Add</a>
            	</div>
            </div>
        </div>
		<!--// End Menu Initiator //-->
		<!-- Wizard -->
		<div class="col-lg-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>TRIVIA QUESTIONS </h5>
				</div>
				<div class="ibox-content">
					
<div class="row">
                <div class="col-lg-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1"> Add Question</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2">View Questions</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                            <form action="">
                            <div class="form-group">
                            <label for="">Question</label>
                            <input type="text" name="question" class="form-control col-md-8">
                        </div>
                        <div class="col-lg-12">
                        <div class="form-group col-md-8">
                            <label for="">Choice: A</label>
                            <input type="text" name="trivianame" class="form-control col-md-8">
                        </div>
                        </div>
                        <div class="col-lg-12">
                         <div class="form-group col-md-8">
                            <label for="">Choice: B</label>
                            <input type="text" name="trivianame" class="form-control col-md-8">
                        </div>    
                        </div>
                        <div class="col-lg-12">                   
                        <div class="form-group col-md-8">
                            <label for="">Choice: C</label>
                            <input type="text" name="trivianame" class="form-control col-md-8">
                        </div>
                    </div>
                    <div class="col-lg-12">
                         <div class="form-group col-md-3">
                            <label for="">Answer</label>
                            <input type="text" name="trivianame"  class="form-control blue-bg">
                        </div>    
                     </div>         

                     <div class="form-group">
                         <button class="btn btn-sm btn-success btn-lg">Submit Question</button>
                     </div>     
                       
                    </form>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <strong>Trivia Questions and </strong>

                                  <div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Question 1. What is a bible
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body ">
    <!--    <h4><u>Choices & Answer</u></h4> -->
       <ul style="list-style-type: none;">
        <li> A. Bible is Bible </li>
        <li> B. Bible is True </li>
        <li> C. bible is real </li>
        <li><b>Answer: C</b></li>
      </ul>   
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Question 2. What is a bible
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <ul style="list-style-type: none;">
        <li> A. Bible is Bible </li>
        <li> B. Bible is True </li>
        <li> C. bible is real </li>
        <li><b>Answer: C</b></li>
      </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Question 3. What is a bible
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
         <ul style="list-style-type: none;">
        <li> A. Bible is Bible </li>
        <li> B. Bible is True </li>
        <li> C. bible is real </li>
        <li><b>Answer: C</b></li>
      </ul>
      </div>
    </div>
  </div>
</div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

				</div>
			</div>
		</div>
		<!-- // End Wizard // -->
	</div>
@endsection