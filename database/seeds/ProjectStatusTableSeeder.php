<?php

use Illuminate\Database\Seeder;

class ProjectStatusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('project_status')->delete();
        
        \DB::table('project_status')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_status' => 1,
                'name' => 'Sandbox',
                'created_at' => '2018-03-09 19:01:02',
            ),
            1 => 
            array (
                'id' => 2,
                'id_status' => 2,
                'name' => 'Production',
                'created_at' => '2018-03-09 19:01:02',
            ),
            2 => 
            array (
                'id' => 3,
                'id_status' => 0,
                'name' => 'unknown',
                'created_at' => '2018-03-09 19:01:15',
            ),
        ));
        
        
    }
}