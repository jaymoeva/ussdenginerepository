<?php

use Illuminate\Database\Seeder;

class SmsTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sms_type')->delete();
        
        \DB::table('sms_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Bulk SMS',
                'status' => 1,
                'created_at' => '2018-04-19 15:27:46',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'M.O Shortcode',
                'status' => 1,
                'created_at' => '2018-04-19 15:27:46',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Subscription ShortCode',
                'status' => 1,
                'created_at' => '2018-04-19 15:28:34',
            ),
        ));
        
        
    }
}