<?php

use Illuminate\Database\Seeder;

class TriviaQuestionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('trivia_questions')->delete();
        
        \DB::table('trivia_questions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'trivia_id' => 4,
                'question' => 'What is my name',
                'choice_a' => 'was',
                'choice_b' => 'were',
                'choice_c' => 'ada',
                'answer' => 'c',
                'created_at' => '2018-04-25 17:43:19',
            ),
            1 => 
            array (
                'id' => 2,
                'trivia_id' => 4,
                'question' => 'who is the name of Uganda\'s president',
                'choice_a' => 'Museveni',
                'choice_b' => 'Moi',
                'choice_c' => 'None',
                'answer' => 'A',
                'created_at' => '2018-04-25 17:44:28',
            ),
            2 => 
            array (
                'id' => 3,
                'trivia_id' => 4,
                'question' => 'What is the name of your company ?',
                'choice_a' => 'Copa Kenya Limited',
                'choice_b' => 'Edgetech Consults Limited',
                'choice_c' => 'Drums For Africa',
                'answer' => 'C',
                'created_at' => '2018-04-25 18:14:05',
            ),
            3 => 
            array (
                'id' => 4,
                'trivia_id' => 5,
                'question' => 'What did the crucification of Jesus Christ mean to us as believers',
                'choice_a' => 'Love',
                'choice_b' => 'Faith',
                'choice_c' => 'Hope',
                'answer' => 'A',
                'created_at' => '2018-04-25 18:43:52',
            ),
        ));
        
        
    }
}