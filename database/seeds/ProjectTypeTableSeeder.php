<?php

use Illuminate\Database\Seeder;

class ProjectTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('project_type')->delete();
        
        \DB::table('project_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'USSD',
                'status' => 0,
                'created_at' => '2018-03-09 18:12:24',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'SMS',
                'status' => 0,
                'created_at' => '2018-03-09 18:12:24',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'TRIVIA',
                'status' => 0,
                'created_at' => '2018-03-09 18:12:32',
            ),
        ));
        
        
    }
}