<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //$this->call(LaratrustSeeder::class);

        $this->call(CompaniesTableSeeder::class);
        $this->call(MigrationsTableSeeder::class);
        $this->call(OauthAccessTokensTableSeeder::class);
        $this->call(OauthAuthCodesTableSeeder::class);
        $this->call(OauthClientsTableSeeder::class);
        $this->call(OauthPersonalAccessClientsTableSeeder::class);
        $this->call(OauthRefreshTokensTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(PermissionUserTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(ProjectTableSeeder::class);
        $this->call(ProjectEndpointsTableSeeder::class);
        $this->call(ProjectStatusTableSeeder::class);
        $this->call(ProjectTypeTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(SmsIncomingTableSeeder::class);
        $this->call(SmsOutgoingTableSeeder::class);
        $this->call(SmsTriviaincomingTableSeeder::class);
        $this->call(SmsTriviaoutgoingTableSeeder::class);
        $this->call(SmsTypeTableSeeder::class);
        $this->call(TasksTableSeeder::class);
        $this->call(TriviaTableSeeder::class);
        $this->call(TriviaParticipantsTableSeeder::class);
        $this->call(TriviaQuestionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UssdCodesTableSeeder::class);
        $this->call(UssdCodesTypeTableSeeder::class);
        $this->call(UssdSessionTableSeeder::class);
        $this->call(UssdTypeTableSeeder::class);
    }
}
