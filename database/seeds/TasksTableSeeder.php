<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tasks')->delete();
        
        \DB::table('tasks')->insert(array (
            0 => 
            array (
                'id' => 3,
                'name' => 'test',
                'user_id' => 1,
                'description' => 'task descriptionjhjhk',
                'created_at' => '2018-03-02 16:19:59',
                'updated_at' => '2018-03-02 16:22:52',
            ),
            1 => 
            array (
                'id' => 5,
                'name' => 'Welcome to edgetech',
                'user_id' => 1,
                'description' => 'home',
                'created_at' => '2018-03-02 16:47:20',
                'updated_at' => '2018-03-02 16:47:20',
            ),
        ));
        
        
    }
}