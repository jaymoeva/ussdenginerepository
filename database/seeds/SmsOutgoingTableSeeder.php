<?php

use Illuminate\Database\Seeder;

class SmsOutgoingTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sms_outgoing')->delete();
        
        \DB::table('sms_outgoing')->insert(array (
            0 => 
            array (
                'id' => 1,
                'project_id' => 6,
                'msisdn' => 254724619830,
                'message' => 'responded well',
                'uniqueid' => 34445555,
                'status' => 1,
                'status_code' => 200,
                'delivery_report' => 'delivered sucessfully',
                'processed' => 1,
                'created_at' => '2018-04-21 15:49:55',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'project_id' => 6,
                'msisdn' => 254724619822,
                'message' => 'responded well',
                'uniqueid' => 34445555,
                'status' => 1,
                'status_code' => 200,
                'delivery_report' => 'delivered sucessfully',
                'processed' => 1,
                'created_at' => '2018-04-21 15:49:55',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'project_id' => 6,
                'msisdn' => 254724619812,
                'message' => 'responded well',
                'uniqueid' => 34445555,
                'status' => 1,
                'status_code' => 200,
                'delivery_report' => 'delivered sucessfully',
                'processed' => 1,
                'created_at' => '2018-04-21 15:49:55',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}