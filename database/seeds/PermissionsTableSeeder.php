<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'create-user',
                'display_name' => 'Create User',
                'description' => 'Allows one to create users',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'edit-user',
                'display_name' => 'Edit User',
                'description' => 'Allows one to edit and update users',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'view-user',
                'display_name' => 'View User',
                'description' => 'Allows one to view other users',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'crud-role',
                'display_name' => 'Manage Role',
                'description' => 'Allows one to create, read, update and delete roles',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'crud-permission',
                'display_name' => 'Manage Permission',
                'description' => 'Allows one to create, read, update and delete permissions',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
        ));
        
        
    }
}