<?php

use Illuminate\Database\Seeder;

class MigrationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migrations')->delete();
        
        \DB::table('migrations')->insert(array (
            0 => 
            array (
                'id' => 3,
                'migration' => '2018_02_28_212423_create_tasks_table',
                'batch' => 1,
            ),
            1 => 
            array (
                'id' => 50,
                'migration' => '2014_10_12_000000_create_users_table',
                'batch' => 2,
            ),
            2 => 
            array (
                'id' => 51,
                'migration' => '2014_10_12_100000_create_password_resets_table',
                'batch' => 2,
            ),
            3 => 
            array (
                'id' => 52,
                'migration' => '2016_06_01_000001_create_oauth_auth_codes_table',
                'batch' => 2,
            ),
            4 => 
            array (
                'id' => 53,
                'migration' => '2016_06_01_000002_create_oauth_access_tokens_table',
                'batch' => 2,
            ),
            5 => 
            array (
                'id' => 54,
                'migration' => '2016_06_01_000003_create_oauth_refresh_tokens_table',
                'batch' => 2,
            ),
            6 => 
            array (
                'id' => 55,
                'migration' => '2016_06_01_000004_create_oauth_clients_table',
                'batch' => 2,
            ),
            7 => 
            array (
                'id' => 56,
                'migration' => '2016_06_01_000005_create_oauth_personal_access_clients_table',
                'batch' => 2,
            ),
            8 => 
            array (
                'id' => 57,
                'migration' => '2018_04_14_101252_laratrust_setup_tables',
                'batch' => 2,
            ),
            9 => 
            array (
                'id' => 58,
                'migration' => '2018_04_19_154535_create_companies_table',
                'batch' => 2,
            ),
            10 => 
            array (
                'id' => 59,
                'migration' => '2018_04_25_103131_create_companies_table',
                'batch' => 0,
            ),
            11 => 
            array (
                'id' => 60,
                'migration' => '2018_04_25_103131_create_oauth_access_tokens_table',
                'batch' => 0,
            ),
            12 => 
            array (
                'id' => 61,
                'migration' => '2018_04_25_103131_create_oauth_auth_codes_table',
                'batch' => 0,
            ),
            13 => 
            array (
                'id' => 62,
                'migration' => '2018_04_25_103131_create_oauth_clients_table',
                'batch' => 0,
            ),
            14 => 
            array (
                'id' => 63,
                'migration' => '2018_04_25_103131_create_oauth_personal_access_clients_table',
                'batch' => 0,
            ),
            15 => 
            array (
                'id' => 64,
                'migration' => '2018_04_25_103131_create_oauth_refresh_tokens_table',
                'batch' => 0,
            ),
            16 => 
            array (
                'id' => 65,
                'migration' => '2018_04_25_103131_create_password_resets_table',
                'batch' => 0,
            ),
            17 => 
            array (
                'id' => 66,
                'migration' => '2018_04_25_103131_create_permission_role_table',
                'batch' => 0,
            ),
            18 => 
            array (
                'id' => 67,
                'migration' => '2018_04_25_103131_create_permission_user_table',
                'batch' => 0,
            ),
            19 => 
            array (
                'id' => 68,
                'migration' => '2018_04_25_103131_create_permissions_table',
                'batch' => 0,
            ),
            20 => 
            array (
                'id' => 69,
                'migration' => '2018_04_25_103131_create_project_table',
                'batch' => 0,
            ),
            21 => 
            array (
                'id' => 70,
                'migration' => '2018_04_25_103131_create_project_endpoints_table',
                'batch' => 0,
            ),
            22 => 
            array (
                'id' => 71,
                'migration' => '2018_04_25_103131_create_project_status_table',
                'batch' => 0,
            ),
            23 => 
            array (
                'id' => 72,
                'migration' => '2018_04_25_103131_create_project_type_table',
                'batch' => 0,
            ),
            24 => 
            array (
                'id' => 73,
                'migration' => '2018_04_25_103131_create_role_user_table',
                'batch' => 0,
            ),
            25 => 
            array (
                'id' => 74,
                'migration' => '2018_04_25_103131_create_roles_table',
                'batch' => 0,
            ),
            26 => 
            array (
                'id' => 75,
                'migration' => '2018_04_25_103131_create_sms_incoming_table',
                'batch' => 0,
            ),
            27 => 
            array (
                'id' => 76,
                'migration' => '2018_04_25_103131_create_sms_outgoing_table',
                'batch' => 0,
            ),
            28 => 
            array (
                'id' => 77,
                'migration' => '2018_04_25_103131_create_sms_triviaincoming_table',
                'batch' => 0,
            ),
            29 => 
            array (
                'id' => 78,
                'migration' => '2018_04_25_103131_create_sms_triviaoutgoing_table',
                'batch' => 0,
            ),
            30 => 
            array (
                'id' => 79,
                'migration' => '2018_04_25_103131_create_sms_type_table',
                'batch' => 0,
            ),
            31 => 
            array (
                'id' => 80,
                'migration' => '2018_04_25_103131_create_tasks_table',
                'batch' => 0,
            ),
            32 => 
            array (
                'id' => 81,
                'migration' => '2018_04_25_103131_create_trivia_table',
                'batch' => 0,
            ),
            33 => 
            array (
                'id' => 82,
                'migration' => '2018_04_25_103131_create_trivia_participants_table',
                'batch' => 0,
            ),
            34 => 
            array (
                'id' => 83,
                'migration' => '2018_04_25_103131_create_trivia_questions_table',
                'batch' => 0,
            ),
            35 => 
            array (
                'id' => 84,
                'migration' => '2018_04_25_103131_create_users_table',
                'batch' => 0,
            ),
            36 => 
            array (
                'id' => 85,
                'migration' => '2018_04_25_103131_create_ussd_codes_table',
                'batch' => 0,
            ),
            37 => 
            array (
                'id' => 86,
                'migration' => '2018_04_25_103131_create_ussd_codes_type_table',
                'batch' => 0,
            ),
            38 => 
            array (
                'id' => 87,
                'migration' => '2018_04_25_103131_create_ussd_session_table',
                'batch' => 0,
            ),
            39 => 
            array (
                'id' => 88,
                'migration' => '2018_04_25_103131_create_ussd_type_table',
                'batch' => 0,
            ),
            40 => 
            array (
                'id' => 89,
                'migration' => '2018_04_26_113057_create_companies_table',
                'batch' => 0,
            ),
            41 => 
            array (
                'id' => 90,
                'migration' => '2018_04_26_113057_create_oauth_access_tokens_table',
                'batch' => 0,
            ),
            42 => 
            array (
                'id' => 91,
                'migration' => '2018_04_26_113057_create_oauth_auth_codes_table',
                'batch' => 0,
            ),
            43 => 
            array (
                'id' => 92,
                'migration' => '2018_04_26_113057_create_oauth_clients_table',
                'batch' => 0,
            ),
            44 => 
            array (
                'id' => 93,
                'migration' => '2018_04_26_113057_create_oauth_personal_access_clients_table',
                'batch' => 0,
            ),
            45 => 
            array (
                'id' => 94,
                'migration' => '2018_04_26_113057_create_oauth_refresh_tokens_table',
                'batch' => 0,
            ),
            46 => 
            array (
                'id' => 95,
                'migration' => '2018_04_26_113057_create_password_resets_table',
                'batch' => 0,
            ),
            47 => 
            array (
                'id' => 96,
                'migration' => '2018_04_26_113057_create_permission_role_table',
                'batch' => 0,
            ),
            48 => 
            array (
                'id' => 97,
                'migration' => '2018_04_26_113057_create_permission_user_table',
                'batch' => 0,
            ),
            49 => 
            array (
                'id' => 98,
                'migration' => '2018_04_26_113057_create_permissions_table',
                'batch' => 0,
            ),
            50 => 
            array (
                'id' => 99,
                'migration' => '2018_04_26_113057_create_project_table',
                'batch' => 0,
            ),
            51 => 
            array (
                'id' => 100,
                'migration' => '2018_04_26_113057_create_project_endpoints_table',
                'batch' => 0,
            ),
            52 => 
            array (
                'id' => 101,
                'migration' => '2018_04_26_113057_create_project_status_table',
                'batch' => 0,
            ),
            53 => 
            array (
                'id' => 102,
                'migration' => '2018_04_26_113057_create_project_type_table',
                'batch' => 0,
            ),
            54 => 
            array (
                'id' => 103,
                'migration' => '2018_04_26_113057_create_role_user_table',
                'batch' => 0,
            ),
            55 => 
            array (
                'id' => 104,
                'migration' => '2018_04_26_113057_create_roles_table',
                'batch' => 0,
            ),
            56 => 
            array (
                'id' => 105,
                'migration' => '2018_04_26_113057_create_routes_table',
                'batch' => 0,
            ),
            57 => 
            array (
                'id' => 106,
                'migration' => '2018_04_26_113057_create_sms_incoming_table',
                'batch' => 0,
            ),
            58 => 
            array (
                'id' => 107,
                'migration' => '2018_04_26_113057_create_sms_outgoing_table',
                'batch' => 0,
            ),
            59 => 
            array (
                'id' => 108,
                'migration' => '2018_04_26_113057_create_sms_triviaincoming_table',
                'batch' => 0,
            ),
            60 => 
            array (
                'id' => 109,
                'migration' => '2018_04_26_113057_create_sms_triviaoutgoing_table',
                'batch' => 0,
            ),
            61 => 
            array (
                'id' => 110,
                'migration' => '2018_04_26_113057_create_sms_type_table',
                'batch' => 0,
            ),
            62 => 
            array (
                'id' => 111,
                'migration' => '2018_04_26_113057_create_tasks_table',
                'batch' => 0,
            ),
            63 => 
            array (
                'id' => 112,
                'migration' => '2018_04_26_113057_create_trivia_table',
                'batch' => 0,
            ),
            64 => 
            array (
                'id' => 113,
                'migration' => '2018_04_26_113057_create_trivia_participants_table',
                'batch' => 0,
            ),
            65 => 
            array (
                'id' => 114,
                'migration' => '2018_04_26_113057_create_trivia_questions_table',
                'batch' => 0,
            ),
            66 => 
            array (
                'id' => 115,
                'migration' => '2018_04_26_113057_create_users_table',
                'batch' => 0,
            ),
            67 => 
            array (
                'id' => 116,
                'migration' => '2018_04_26_113057_create_ussd_codes_table',
                'batch' => 0,
            ),
            68 => 
            array (
                'id' => 117,
                'migration' => '2018_04_26_113057_create_ussd_codes_type_table',
                'batch' => 0,
            ),
            69 => 
            array (
                'id' => 118,
                'migration' => '2018_04_26_113057_create_ussd_hops_table',
                'batch' => 0,
            ),
            70 => 
            array (
                'id' => 119,
                'migration' => '2018_04_26_113057_create_ussd_session_table',
                'batch' => 0,
            ),
            71 => 
            array (
                'id' => 120,
                'migration' => '2018_04_26_113057_create_ussd_type_table',
                'batch' => 0,
            ),
        ));
        
        
    }
}