<?php

use Illuminate\Database\Seeder;

class SmsTriviaincomingTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sms_triviaincoming')->delete();
        
        \DB::table('sms_triviaincoming')->insert(array (
            0 => 
            array (
                'id' => 1,
                'project_id' => 2,
                'msisdn' => 254724619830,
                'message' => 'Start',
                'link_id' => '2333',
                'service_id' => 0,
                'sender_id' => 0,
                'network' => 0,
                'uniqueid' => 0,
                'shortcode' => 0,
                'correlator' => 0,
                'status' => 0,
                'created_at' => '2018-04-26 11:43:26',
            ),
            1 => 
            array (
                'id' => 2,
                'project_id' => 2,
                'msisdn' => 254724619830,
                'message' => 'A',
                'link_id' => '0',
                'service_id' => 0,
                'sender_id' => 0,
                'network' => 0,
                'uniqueid' => 0,
                'shortcode' => 0,
                'correlator' => 0,
                'status' => 0,
                'created_at' => '2018-04-26 11:43:26',
            ),
        ));
        
        
    }
}