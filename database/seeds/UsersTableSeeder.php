<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Support',
                'email' => 'support@edgetech.co.ke',
                'added_by' => NULL,
                'avatar' => 'default.png',
                'password' => '$2y$10$IovnbU0wVNeqp.ExAhCKyepF0xX7QNHHXdAoVvtKC.jf87jaBMtHm',
                'company_id' => 1,
                'verified' => 1,
                'active' => 1,
                'token' => 'MNV1capJf26Y6qOOqZkS7ySWucQfUFcLuLP91Txd',
                'remember_token' => 'Tt9RtGxnuyhUnanlyc3aHm3vnFCFQZ5kaymaXU34kQJTzx0Ak85Z6IXT2wBB',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Anthony',
                'email' => 'toneymutinda@gmail.com',
                'added_by' => NULL,
                'avatar' => 'default.png',
                'password' => '$2y$10$C4C/a1WY.983xD2x5w8gQ.wBf7Y.6v6bmPziyvcexD5BjrmHq8fL.',
                'company_id' => 2,
                'verified' => 1,
                'active' => 1,
                'token' => 'z11UfHkonuEhHJhkFTkyO713Tpo912zwYiD666oD',
                'remember_token' => 'x9JYVQEyRY',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Edgetech Consults Limited',
                'email' => 'support2@edgetech.co.ke',
                'added_by' => NULL,
                'avatar' => 'default.png',
                'password' => '$2y$10$stxFHK6wJf2GQySCmPej2.skZGpnzViQT8g8MPqhhRiwLjsdyRBJy',
                'company_id' => 3,
                'verified' => 1,
                'active' => 1,
                'token' => 'v01YYTGRgAyMeGCcQEcIMZk7j4ypOZXKEJcgUlKx',
                'remember_token' => NULL,
                'created_at' => '2018-04-25 09:44:51',
                'updated_at' => '2018-04-25 09:44:51',
            ),
        ));
        
        
    }
}