<?php

use Illuminate\Database\Seeder;

class UssdSessionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ussd_session')->delete();
        
        \DB::table('ussd_session')->insert(array (
            0 => 
            array (
                'id' => 1,
                'project_id' => 1,
                'code' => 0,
                'session' => '233442',
                'hops_count' => 0,
                'text' => 'tests',
                'response' => 'welcome',
                'status_code' => 200,
                'status' => 1,
                'created_at' => '2018-04-18 18:01:22',
                'updated_at' => '2018-04-18 18:03:16',
            ),
            1 => 
            array (
                'id' => 2,
                'project_id' => 1,
                'code' => 0,
                'session' => '233443',
                'hops_count' => 0,
                'text' => 'tests2',
                'response' => 'welcome2',
                'status_code' => 200,
                'status' => 1,
                'created_at' => '2018-04-18 18:01:22',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'project_id' => 1,
                'code' => 0,
                'session' => '233444',
                'hops_count' => 0,
                'text' => 'tests3',
                'response' => 'welcome3',
                'status_code' => 500,
                'status' => 0,
                'created_at' => '2018-04-18 18:01:22',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'project_id' => 1,
                'code' => 0,
                'session' => '233446',
                'hops_count' => 0,
                'text' => 'tests4',
                'response' => 'welcome4',
                'status_code' => 500,
                'status' => 0,
                'created_at' => '2018-04-18 18:01:22',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'project_id' => 1,
                'code' => 0,
                'session' => '233445',
                'hops_count' => 0,
                'text' => 'tests5',
                'response' => 'welcome5',
                'status_code' => 200,
                'status' => 1,
                'created_at' => '2018-04-18 18:01:22',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}