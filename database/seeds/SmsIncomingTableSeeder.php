<?php

use Illuminate\Database\Seeder;

class SmsIncomingTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sms_incoming')->delete();
        
        \DB::table('sms_incoming')->insert(array (
            0 => 
            array (
                'id' => 1,
                'project_id' => 6,
                'msisdn' => 724619830,
                'message' => 'system tests',
                'link_id' => '23455677755543433',
                'service_id' => 3334,
                'sender_id' => 23456,
                'network' => 2,
                'uniqueid' => 3456789,
                'shortcode' => 23456,
                'correlator' => 3,
                'status' => 0,
                'created_at' => '2018-04-21 04:10:44',
            ),
            1 => 
            array (
                'id' => 2,
                'project_id' => 6,
                'msisdn' => 724619831,
                'message' => 'system tests',
                'link_id' => '23455677755543432',
                'service_id' => 3334,
                'sender_id' => 23456,
                'network' => 2,
                'uniqueid' => 3456783,
                'shortcode' => 23456,
                'correlator' => 3,
                'status' => 1,
                'created_at' => '2018-04-21 04:10:44',
            ),
        ));
        
        
    }
}