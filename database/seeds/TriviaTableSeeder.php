<?php

use Illuminate\Database\Seeder;

class TriviaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('trivia')->delete();
        
        \DB::table('trivia')->insert(array (
            0 => 
            array (
                'id' => 2,
                'company_id' => 3,
                'trivia_name' => 'Museveni',
                'trivia_type' => 'tests',
                'description' => 'tests',
                'url_endpoint' => 'http://127.0.0.1:8000/project/Museveni',
                'status' => 0,
                'created_at' => '2018-04-25 16:24:11',
            ),
            1 => 
            array (
                'id' => 3,
                'company_id' => 3,
                'trivia_name' => 'Museveni',
                'trivia_type' => 'tests',
                'description' => 'tests',
                'url_endpoint' => 'http://127.0.0.1:8000/project/Museveni',
                'status' => 0,
                'created_at' => '2018-04-25 16:34:06',
            ),
            2 => 
            array (
                'id' => 4,
                'company_id' => 3,
                'trivia_name' => 'Museveni',
                'trivia_type' => 'tests',
                'description' => 'tests',
                'url_endpoint' => 'http://127.0.0.1:8000/project/Museveni',
                'status' => 0,
                'created_at' => '2018-04-25 16:36:23',
            ),
            3 => 
            array (
                'id' => 5,
                'company_id' => 3,
                'trivia_name' => 'WEDNESDAY TASK',
                'trivia_type' => 'Inspiration',
                'description' => 'Reminding ourselves about the love of christ',
                'url_endpoint' => 'http://127.0.0.1:8000/project/WEDNESDAYTASK',
                'status' => 0,
                'created_at' => '2018-04-25 18:41:56',
            ),
        ));
        
        
    }
}