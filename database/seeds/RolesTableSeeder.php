<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'display_name' => 'System Administrator',
                'description' => 'User who can add other users and perform custom system configurations',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'developer',
                'display_name' => 'Developer',
                'description' => 'User who can build custom USSD menu applications',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'executive',
                'display_name' => 'Executive',
                'description' => 'User who can own a company in the system',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'clerk',
                'display_name' => 'Clerk',
                'description' => 'User who is responsible for data entry into the system',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'guest',
                'display_name' => 'Guest',
                'description' => 'User who is yet to be approved',
                'created_at' => '2018-04-25 09:41:51',
                'updated_at' => '2018-04-25 09:41:51',
            ),
        ));
        
        
    }
}