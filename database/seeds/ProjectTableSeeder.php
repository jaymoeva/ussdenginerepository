<?php

use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('project')->delete();
        
        \DB::table('project')->insert(array (
            0 => 
            array (
                'id' => 1,
                'company_id' => 1,
                'name' => 'ray app',
                'type' => 1,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-17 17:01:36',
            ),
            1 => 
            array (
                'id' => 2,
                'company_id' => 1,
                'name' => 'vetting',
                'type' => 1,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-17 18:16:35',
            ),
            2 => 
            array (
                'id' => 3,
                'company_id' => 1,
                'name' => 'yuerwu',
                'type' => 1,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-18 09:18:36',
            ),
            3 => 
            array (
                'id' => 4,
                'company_id' => 1,
                'name' => 'ray USSD',
                'type' => 1,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-18 11:16:58',
            ),
            4 => 
            array (
                'id' => 5,
                'company_id' => 1,
                'name' => 'ray_test',
                'type' => 1,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-19 18:00:12',
            ),
            5 => 
            array (
                'id' => 6,
                'company_id' => 1,
                'name' => 'Games SMS',
                'type' => 2,
                'sms_type' => 1,
                'status' => 1,
                'created_at' => '2018-04-20 12:25:12',
            ),
            6 => 
            array (
                'id' => 7,
                'company_id' => 1,
                'name' => 'SMS APP',
                'type' => 2,
                'sms_type' => 2,
                'status' => 1,
                'created_at' => '2018-04-20 18:22:25',
            ),
            7 => 
            array (
                'id' => 8,
                'company_id' => 1,
                'name' => 'SMS APP',
                'type' => 2,
                'sms_type' => 3,
                'status' => 1,
                'created_at' => '2018-04-20 18:23:02',
            ),
            8 => 
            array (
                'id' => 9,
                'company_id' => 1,
                'name' => 'test',
                'type' => 2,
                'sms_type' => 2,
                'status' => 1,
                'created_at' => '2018-04-20 18:23:22',
            ),
            9 => 
            array (
                'id' => 10,
                'company_id' => 1,
                'name' => 'last',
                'type' => 2,
                'sms_type' => 3,
                'status' => 1,
                'created_at' => '2018-04-20 18:25:23',
            ),
            10 => 
            array (
                'id' => 11,
                'company_id' => 1,
                'name' => '1',
                'type' => 2,
                'sms_type' => 1,
                'status' => 1,
                'created_at' => '2018-04-20 18:32:43',
            ),
            11 => 
            array (
                'id' => 12,
                'company_id' => 1,
                'name' => 'qwerty',
                'type' => 2,
                'sms_type' => 1,
                'status' => 1,
                'created_at' => '2018-04-20 18:33:35',
            ),
            12 => 
            array (
                'id' => 13,
                'company_id' => 1,
                'name' => 'asas',
                'type' => 2,
                'sms_type' => 2,
                'status' => 1,
                'created_at' => '2018-04-20 19:01:05',
            ),
            13 => 
            array (
                'id' => 14,
                'company_id' => 1,
                'name' => 'dasa',
                'type' => 2,
                'sms_type' => 2,
                'status' => 1,
                'created_at' => '2018-04-20 19:02:21',
            ),
            14 => 
            array (
                'id' => 15,
                'company_id' => 1,
                'name' => 'dasa',
                'type' => 2,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-20 19:06:44',
            ),
            15 => 
            array (
                'id' => 16,
                'company_id' => 1,
                'name' => 'qwe',
                'type' => 2,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-20 19:37:09',
            ),
            16 => 
            array (
                'id' => 17,
                'company_id' => 1,
                'name' => 'ww',
                'type' => 2,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-20 22:59:10',
            ),
            17 => 
            array (
                'id' => 18,
                'company_id' => 1,
                'name' => 'we',
                'type' => 2,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-21 00:18:35',
            ),
            18 => 
            array (
                'id' => 19,
                'company_id' => 1,
                'name' => 'q',
                'type' => 2,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-21 00:21:53',
            ),
            19 => 
            array (
                'id' => 20,
                'company_id' => 1,
                'name' => 'q',
                'type' => 2,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-21 00:22:26',
            ),
            20 => 
            array (
                'id' => 21,
                'company_id' => 1,
                'name' => 'sms',
                'type' => 2,
                'sms_type' => 0,
                'status' => 1,
                'created_at' => '2018-04-21 00:26:36',
            ),
        ));
        
        
    }
}