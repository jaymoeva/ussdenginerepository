<?php

use Illuminate\Database\Seeder;

class ProjectEndpointsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('project_endpoints')->delete();
        
        \DB::table('project_endpoints')->insert(array (
            0 => 
            array (
                'id' => 1,
                'company_id' => 1,
                'project_id' => 1,
                'endpoint_external' => 'http://127.0.0.1:8000/menus',
                'endpoint_internal' => '',
                'status' => 0,
                'created_at' => '2018-04-17 17:01:36',
            ),
            1 => 
            array (
                'id' => 2,
                'company_id' => 1,
                'project_id' => 2,
                'endpoint_external' => 'http://127.0.0.1:8000/menus',
                'endpoint_internal' => '',
                'status' => 0,
                'created_at' => '2018-04-17 18:16:35',
            ),
            2 => 
            array (
                'id' => 3,
                'company_id' => 1,
                'project_id' => 3,
                'endpoint_external' => 'ueue',
                'endpoint_internal' => '',
                'status' => 0,
                'created_at' => '2018-04-18 09:18:36',
            ),
            3 => 
            array (
                'id' => 4,
                'company_id' => 1,
                'project_id' => 4,
                'endpoint_external' => 'cloud.edgetech.co.ke/app',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/ray USSD',
                'status' => 0,
                'created_at' => '2018-04-18 11:16:58',
            ),
            4 => 
            array (
                'id' => 5,
                'company_id' => 1,
                'project_id' => 5,
                'endpoint_external' => 'http://127.0.0.1:8000/ussd-static',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/ray_test',
                'status' => 0,
                'created_at' => '2018-04-19 18:00:12',
            ),
            5 => 
            array (
                'id' => 6,
                'company_id' => 1,
                'project_id' => 6,
                'endpoint_external' => 'http://127.0.0.1:8000/sms-static#',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/GamesSMS',
                'status' => 0,
                'created_at' => '2018-04-20 12:25:12',
            ),
            6 => 
            array (
                'id' => 7,
                'company_id' => 1,
                'project_id' => 7,
                'endpoint_external' => 'http://127.0.0.1:8000/sms-static',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/SMSAPP',
                'status' => 0,
                'created_at' => '2018-04-20 18:22:25',
            ),
            7 => 
            array (
                'id' => 8,
                'company_id' => 1,
                'project_id' => 8,
                'endpoint_external' => 'http://127.0.0.1:8000/sms-static',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/SMSAPP',
                'status' => 0,
                'created_at' => '2018-04-20 18:23:02',
            ),
            8 => 
            array (
                'id' => 9,
                'company_id' => 1,
                'project_id' => 9,
                'endpoint_external' => 'current',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/test',
                'status' => 0,
                'created_at' => '2018-04-20 18:23:22',
            ),
            9 => 
            array (
                'id' => 10,
                'company_id' => 1,
                'project_id' => 10,
                'endpoint_external' => 'test',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/last',
                'status' => 0,
                'created_at' => '2018-04-20 18:25:23',
            ),
            10 => 
            array (
                'id' => 11,
                'company_id' => 1,
                'project_id' => 11,
                'endpoint_external' => '2',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/1',
                'status' => 0,
                'created_at' => '2018-04-20 18:32:43',
            ),
            11 => 
            array (
                'id' => 12,
                'company_id' => 1,
                'project_id' => 12,
                'endpoint_external' => 'ruri',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/qwerty',
                'status' => 0,
                'created_at' => '2018-04-20 18:33:35',
            ),
            12 => 
            array (
                'id' => 13,
                'company_id' => 1,
                'project_id' => 13,
                'endpoint_external' => 'ssddd',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/asas',
                'status' => 0,
                'created_at' => '2018-04-20 19:01:05',
            ),
            13 => 
            array (
                'id' => 14,
                'company_id' => 1,
                'project_id' => 14,
                'endpoint_external' => 'frgdsds',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/dasa',
                'status' => 0,
                'created_at' => '2018-04-20 19:02:21',
            ),
            14 => 
            array (
                'id' => 15,
                'company_id' => 1,
                'project_id' => 15,
                'endpoint_external' => 'frgdsds',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/dasa',
                'status' => 0,
                'created_at' => '2018-04-20 19:06:44',
            ),
            15 => 
            array (
                'id' => 16,
                'company_id' => 1,
                'project_id' => 16,
                'endpoint_external' => 'wwe',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/qwe',
                'status' => 0,
                'created_at' => '2018-04-20 19:37:10',
            ),
            16 => 
            array (
                'id' => 17,
                'company_id' => 1,
                'project_id' => 17,
                'endpoint_external' => 'eee',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/ww',
                'status' => 0,
                'created_at' => '2018-04-20 22:59:10',
            ),
            17 => 
            array (
                'id' => 18,
                'company_id' => 1,
                'project_id' => 18,
                'endpoint_external' => 'ee',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/we',
                'status' => 0,
                'created_at' => '2018-04-21 00:18:35',
            ),
            18 => 
            array (
                'id' => 19,
                'company_id' => 1,
                'project_id' => 19,
                'endpoint_external' => 'ww',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/q',
                'status' => 0,
                'created_at' => '2018-04-21 00:21:53',
            ),
            19 => 
            array (
                'id' => 20,
                'company_id' => 1,
                'project_id' => 20,
                'endpoint_external' => 'ww',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/q',
                'status' => 0,
                'created_at' => '2018-04-21 00:22:26',
            ),
            20 => 
            array (
                'id' => 21,
                'company_id' => 1,
                'project_id' => 21,
                'endpoint_external' => 'sjsk',
                'endpoint_internal' => 'http://127.0.0.1:8000/project/sms',
                'status' => 0,
                'created_at' => '2018-04-21 00:26:36',
            ),
        ));
        
        
    }
}