<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTriviaParticipantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trivia_participants', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('msisdn');
			$table->integer('trivia_id');
			$table->integer('question_id');
			$table->string('answer', 110)->nullable();
			$table->integer('answer_id');
			$table->integer('status')->default(0);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trivia_participants');
	}

}
