<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUssdSessionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ussd_session', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id');
			$table->integer('code');
			$table->string('session', 200);
			$table->integer('hops_count');
			$table->string('text', 200)->nullable();
			$table->string('response', 200)->nullable();
			$table->integer('status_code')->nullable();
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ussd_session');
	}

}
