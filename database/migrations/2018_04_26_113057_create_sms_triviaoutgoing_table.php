<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSmsTriviaoutgoingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sms_triviaoutgoing', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id');
			$table->bigInteger('msisdn');
			$table->string('message', 700)->nullable();
			$table->bigInteger('uniqueid')->default(0);
			$table->integer('status')->default(0);
			$table->integer('status_code')->default(0);
			$table->string('delivery_report', 100)->nullable();
			$table->integer('processed')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sms_triviaoutgoing');
	}

}
