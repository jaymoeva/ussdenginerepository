<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSmsIncomingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sms_incoming', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('project_id');
			$table->bigInteger('msisdn');
			$table->string('message', 400);
			$table->string('link_id', 100)->default('0');
			$table->bigInteger('service_id')->default(0);
			$table->bigInteger('sender_id')->default(0);
			$table->integer('network')->default(0);
			$table->bigInteger('uniqueid')->default(0);
			$table->bigInteger('shortcode')->default(0);
			$table->bigInteger('correlator')->default(0);
			$table->integer('status')->default(0);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sms_incoming');
	}

}
