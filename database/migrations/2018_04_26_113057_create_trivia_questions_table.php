<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTriviaQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trivia_questions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('trivia_id');
			$table->string('question', 100);
			$table->string('choice_a', 100);
			$table->string('choice_b', 100);
			$table->string('choice_c', 100);
			$table->string('answer', 100);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trivia_questions');
	}

}
