<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUssdHopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ussd_hops', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('session_id');
			$table->string('text', 200)->nullable();
			$table->string('message', 200)->nullable();
			$table->integer('status')->default(0);
			$table->bigInteger('response_code')->default(0);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ussd_hops');
	}

}
