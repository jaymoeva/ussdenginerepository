<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectEndpointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_endpoints', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('company_id')->default(0);
			$table->integer('project_id');
			$table->string('endpoint_external', 200)->nullable();
			$table->string('endpoint_internal', 200)->nullable();
			$table->integer('status')->default(0);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_endpoints');
	}

}
