<?php

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
|
| These routes point to the public pages of the application. No 
| middleware is assigned to them as a result
|
*/

Route::namespace('Frontend')->group(function () {
	Route::get('/', 'PageController@getWelcome');
	Route::get('/documentation', 'PageController@getDocs');
	Route::get('complete-registration', 'PageController@getCompleteRegistration')->name('registration.complete');
	Route::get('login-and-signup-procedure', 'PageController@getHowTo')->name('howto');
});

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
|
| These routes point to the private pages of the application. Some 
| middleware is assigned to them as a result.
|
*/

Route::namespace('Backend')->group(function () {
	Route::get('/dashboard', 'PageController@getDashboard')->name('dashboard');
	Route::get('ussd-static', 'PageController@getStaticUSSD')->name('ussd.static');
	Route::get('ussd-dynamic', 'PageController@getDynamicUSSD')->name('ussd.dynamic');
	Route::get('sms-static', 'PageController@getStaticSMS')->name('sms.static');
	Route::get('sms-dynamic', 'PageController@getDynamicSMS')->name('sms.dynamic');
	Route::get('shortcode-details', 'PageController@getShortcodeDetails')->name('details.shortcode');
	Route::resource('users', 'UserController');
	Route::get('verified', 'UserController@verified')->name('verified');
	Route::get('unverified', 'UserController@unverified')->name('unverified');
	Route::get('active', 'UserController@active')->name('active');
	Route::get('inactive', 'UserController@inactive')->name('inactive');
	Route::get('change-role/{id}', 'UserController@getChangeRole')->name('user.role');
	Route::post('change-role/{id}', 'UserController@postChangeRole')->name('role.change');
	Route::post('deactivate-user/{id}', 'UserController@deactivate')->name('user.deactivate');
	Route::post('activate-user/{id}', 'UserController@activate')->name('user.activate');
	Route::get('profile', 'PageController@getProfile')->name('user.profile');
	Route::post('change-avatar', 'PageController@postChangeAvatar')->name('avatar.change');
	Route::post('edit-details', 'PageController@postChangeDetails')->name('profile.update');
	Route::resource('roles', 'RoleController');
	Route::get('attach-permission/{id}', 'RoleController@getChangePermission')->name('role.permission');
	Route::post('attach-permission/{id}', 'RoleController@postChangePermission')->name('permission.change');
	Route::resource('permissions', 'PermissionController');
	Route::get('analytics-graphs', 'ReportController@getGraphs')->name('analytics.graphs');
	Route::get('analytics-reports', 'ReportController@getReports')->name('analytics.reports');
	Route::resource('companies', 'CompanyController');
	Route::post('activate-company/{id}', 'CompanyController@activateCompany')->name('company.activate');
	Route::post('deactivate-company/{id}', 'CompanyController@deactivateCompany')->name('company.deactivate');
	//Route::get('company-users', 'CompanyController@users')->name('company.users');
	Route::get('manage-company', 'CompanyController@manageCompany')->name('company.manage');
    Route::post('/createtrivia','GamesController@addtrivia')->name('trivia.create');
    Route::post('/addquestions','GamesController@addtriviaquestions')->name('trivia.addquestions');
    Route::get('/deletetrivia','GamesController@deletetrivia')->name('trivia.delete');
    Route::get('/marketing/email','MarketingController@emailMarketing')->name('marketingemail.send');
    Route::post('/marketing/email', 'MarketingController@postSendEmail')->name('marketingemail.bulksend');
	Route::get('marketing/email-groups', 'MarketingController@emailGroups')->name('marketingemail.groups');
	Route::get('marketing/view-email-group/{id}', 'MarketingController@showEmailGroup')->name('marketingemail.showgroup');
	Route::get('marketing/edit-email-group/{id}', 'MarketingController@editEmailGroup')->name('marketingemail.editgroup');
	Route::post('marketing/update-email-group/{id}', 'MarketingController@updateEmailGroup')->name('marketingemail.updategroup');
	Route::get('/marketing/view-email-contact/{id}','MarketingController@showEmailContact')->name('marketingemail.emailcontact');
	Route::post('add-email-group', 'MarketingController@addEmailGroup')->name('emailgroup.add');
	Route::post('add-email-contact/{id}', 'MarketingController@addEmailContact')->name('emailcontact.add');
	Route::get('marketing/sms', 'MarketingController@smsMarketing')->name('marketingsms.send');
	Route::get('marketing/sms-groups', 'MarketingController@smsGroups')->name('marketingsms.groups');
	Route::get('marketing/view-sms-group', 'MarketingController@showSMSGroup')->name('marketingsms.showgroup');
	Route::get('marketing/edit-sms-group', 'MarketingController@editSMSGroup')->name('marketingsms.editgroup');
	Route::get('/marketing/view-sms-contact','MarketingController@showSMSContact')->name('marketingsms.smscontact');
});

Route::get('/trivia','GamesManagementController@trivia');

Route::get('/listtrivia','GamesManagementController@listtrivia');
Route::get('/viewtrivia','GamesManagementController@viewtrivia');
Route::get('players','GamesManagementController@getPlayers');
Route::get('player-details', 'GamesManagementController@getPlayerDetails');
Route::get('/polls','GamesManagementController@polls')->name('polls');
Route::post('/create_polls','Backend\ElectionsController@create');
Route::get('lifestyle','GamesManagementController@lifestyle')->name('lifestyle.add_participants');
Route::get('/polls','GamesManagementController@polls');
Route::get('edit-poll', 'GamesManagementController@editPoll');
Route::get('lifestyle','GamesManagementController@lifestyle')->name('lifestyle.add_participants');
Route::get('lifestyle-types', 'GamesManagementController@getLifestyleTypes')->name('lifestyle.types');
Route::get('edit-lifestyle', 'GamesManagementController@editLifestyleType')->name('lifestyle.edit');
Route::get('create-lifestyle', 'GamesManagementController@createLifestyle')->name('lifestyle.create');
Route::get('games-present', 'GamesManagementController@lifestyleGamesPresent')->name('lifestylegames.index');
Route::get('create-lifestyle-game', 'GamesManagementController@createLifestyleGame')->name('lifestylegames.create');
Route::get('edit-lifestyle-game', 'GamesManagementController@editLifestyleGame')->name('lifestylegames.edit');
Route::get('view-lifestyle-game', 'GamesManagementController@showLifestyleGame')->name('lifestylegames.show');
 
//Route::get('/lifestyle','GamesManagementController@lifestyle');
Route::get('/lottery','GamesManagementController@lottery');
Route::get('/marketting','GamesManagementController@marketting');
Route::get('listpolls', 'Backend\ElectionsController@view');
Route::get('poll-details', 'Backend\ElectionsController@getinvidualpoll')->name("polls.details");
Route::get('poll-participants', 'Backend\ElectionsController@getparticipants')->name("polls.participants");
Route::get('listpolls', 'GamesManagementController@getAllPolls');
Route::get('poll-details', 'GamesManagementController@getPollDetails');
Route::post('/postemails','API\MarketingController@postemails');
Route::post('/postsms','API\MarketingController@postsms');
Route::post('createproject', 'API\ProjectsController@createproject');
Route::get('getprojects', 'API\ProjectsController@getprojects');
Route::get('getdetails', 'API\ProjectsController@getprojectsdetails');
Route::get('getstatistics', 'API\UssdController@getstatistics');
Route::get('getsessions', 'API\UssdController@getsessions');
Route::get('getsmstype', 'API\ProjectsController@getsmstype');
Route::get('getstatisticsms', 'API\SmsController@getstatistics');
Route::get('getoutgoing', 'API\SmsController@getoutgoing');
Route::get('getincomingsms', 'API\SmsController@getincomingsms');


/*
|--------------------------------------------------------------------------
| Authentication Routes
|--------------------------------------------------------------------------
|
| These routes deal with all authentication logic of the app. 
|
*/

Auth::routes();
Route::namespace('Auth')->group(function () {
	Route::get('register/verify/{token}', 'RegisterController@verifyEmail');
	Route::post('complete-registration', 'RegisterController@completeRegistration')->name('reg.complete');
});


Route::get('/home', 'HomeController@index')->name('home');
