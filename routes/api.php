<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('createproject', 'API\ProjectsController@createproject');
Route::get('getprojects', 'API\ProjectsController@getprojects');
Route::get('getdetails', 'API\ProjectsController@getprojectsdetails');
Route::get('getstatistics', 'API\UssdController@getstatistics');
Route::get('getsessions', 'API\UssdController@getsessions');
Route::get('getsmstype', 'API\ProjectsController@getsmstype');
Route::get('getstatisticsms', 'API\SmsController@getstatistics');
Route::get('getoutgoing', 'API\SmsController@getoutgoing');
Route::get('getincomingsms', 'API\SmsController@getincomingsms');
Route::post('service/{id}/{type}/{name}', 'API\UssdController@ussdlink');
Route::post('smsservice/{id}/{type}/{name}', 'API\SmsController@smsservice');
Route::get('processtrivia', 'API\GamemanagementController@processTrivia'); 
//Route::group(['middleware' => 'auth:api'], function(){
//
//    Route::get('details', 'API\UserController@details');
//    Route::get('createproject', 'API\ProjectsController@createproject');
//
//});
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
